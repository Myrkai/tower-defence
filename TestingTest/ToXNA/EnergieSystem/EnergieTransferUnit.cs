﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.ToXNA;
using TestingTest.Component;
using System.Collections.ObjectModel;
using SharpDX;

namespace TestingTest.ToXNA.EnergieSystem {
	/// <summary>
	/// Automatische Verteilung des Stroms an Tower und Brücken.
	/// Der Strom kann nur von Klassen angenommen werden die das Interface IEnergieReceiveUnit ableiten.
	/// Zusätzlich ist einer Timer mit integriert.
	/// </summary>
	public class EnergieTransferUnit {
		private List<IEnergieReceiveUnit> CurrentConnections { get; set; } //Die verbundenen Brücken... hier sind die Ziele wohin der Strom geht
		public ReadOnlyCollection<IEnergieReceiveUnit> Current_Connections { get { return new ReadOnlyCollection<IEnergieReceiveUnit>(CurrentConnections); } }

		public bool CanConnetBridges { get; private set; }
		public bool CanConnetTowers { get; private set; }

		public float TransferRate { get; private set; } //Die Menge an Energie die auf einmal verschoben werden kann
		public float Range { get; private set; } //wie weit können die Stromleitungen gezogen werden
		public int MaxConnections { get; private set; } //wieviele Verbindungen können aufgebaut werden

		private float Timer { get; set; }
		public event EventHandler TimerTick;
		public event EventHandler<EnergieTransferUnitEventArgs> EnergieTransfered;
		private Vector2 CenterLocation { get; set; }

		public EnergieTransferUnit(int maxConnections, float transferRate, float range, bool canConnetBridges, bool canConnetTowers) { 
			Timer = 0f;
			TransferRate = transferRate;
			Range = range;
			MaxConnections = maxConnections;

			CanConnetTowers = canConnetTowers;
			CanConnetBridges = canConnetBridges;

			CurrentConnections = new List<IEnergieReceiveUnit>();
		}

		public void AddUnit(IEnergieReceiveUnit unit) {
			if (unit.GetType() == typeof(Tower) && CanConnetTowers || unit.GetType() == typeof(Bridge) && CanConnetBridges) {
				if (CurrentConnections.Count + 1 <= MaxConnections) {
					CurrentConnections.Add(unit);
				}
			}
		}

		public void RemoveUnit(IEnergieReceiveUnit unit) {
			CurrentConnections.Remove(unit);
		}

		public void RemoveAll() {
			CurrentConnections.Clear();
		}

		public virtual void Update(GameTime time) {
			Timer += (float)time.TimeDiff.TotalSeconds;

			if (Timer > 1f) {
				Timer -= 1f;
				if (TimerTick != null) {
					TimerTick(this, null);
				}
			}
		}

		public virtual void Draw(Spritebatch spriteBatch) {
			if (CenterLocation != null) {
				foreach (IEnergieReceiveUnit unit in Current_Connections) {
					spriteBatch.DrawLine(CenterLocation, unit.GetCenterLocation(), 2f, Color.Red);
				}
			}
		}

		public void TransferEnergie(float amount) {
			float rateAmount = Math.Min(amount, TransferRate);
			float openRateAmount = 0f;
			int countConnections = CurrentConnections.Count();

			float energieNeeded = 0f;

			foreach (IEnergieReceiveUnit unit in CurrentConnections.OrderBy(x => x.GetUnusedFreeEnergie())) {
				//benötige mehr Energie als zugeteilt wird
				if (unit.GetUnusedFreeEnergie() > rateAmount / countConnections) {
					float used = rateAmount / countConnections + openRateAmount;
					used = Math.Min(used, unit.GetUnusedFreeEnergie());
					unit.AddEnergie(used);
					energieNeeded += used;
					openRateAmount -= used - rateAmount / countConnections;
				} else { //benötigt weniger energie als zugeteilt wird
					float open = rateAmount / countConnections - unit.GetUnusedFreeEnergie();
					energieNeeded += unit.GetUnusedFreeEnergie();
					unit.AddEnergie(unit.GetUnusedFreeEnergie());
					openRateAmount += open;
				}
			}

			if (EnergieTransfered != null) {
				EnergieTransfered(this, new EnergieTransferUnitEventArgs(energieNeeded));
			}
		}

		/// <summary>
		/// Do Not Use. Its for Konstruktor Only
		/// </summary>
		internal void SetCenterLocationForDrawing(Vector2 centerLocation) {
			CenterLocation = centerLocation;
		}

		/// <summary>
		/// Clont ohne Connections
		/// </summary>
		internal EnergieTransferUnit Clone() {
			EnergieTransferUnit tmp = new EnergieTransferUnit(this.MaxConnections, this.TransferRate, this.Range, this.CanConnetBridges, this.CanConnetTowers);
			return tmp;
		}
	}

	public class EnergieTransferUnitEventArgs : EventArgs {
		public float EnergieUsed { get; private set; }

		public EnergieTransferUnitEventArgs(float f) {
			EnergieUsed = f;
		}
	}
}
