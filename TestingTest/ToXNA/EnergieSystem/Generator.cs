﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct3D11;
using TestingTest.Component;
using Texture2D = TestingTest.Component.Texture2D;
using SideProjekt.Komponents;
using SharpDX;

namespace TestingTest.ToXNA.EnergieSystem {
	/// <summary>
	/// Stellt kontinurierlich Strom her. Dieser kann nur an Brücken abgegeben werden.
	/// Abso Generator -> Brücke -> Tower.
	/// Die Verteilung des Stroms passiert über EnergieTransferUnit.
	/// Der Strom kann nur von Klassen angenommen werden die das Interface IEnergieReceiveUnit ableiten.
	/// </summary>
	public class Generator : EnergieTransferUnit {
		public string ThumbnailAsset { get; private set; } //Vorschaubild
		public Texture2D Thumbnail { get; private set; }

		public string GraphicAsset { get; private set; } //Aussehen des Generator
		public Texture2D Graphic { get; private set; }

		private Generator UpgradeableGenerator { get; set; } //Nächst besseren Generator
		public int GoldCost { get; private set; } //Aktuellen Kosten für diesen Generator

		public float MaxEnergie { get; private set; } //Jeder Tower braucht Energie und hier wird die Maximale Menge davon Gespeichert die dieser Generator aufnehmen kann
		public float CurrentEnergie { get; private set; } //die Menge an Energie die derzeit hier Gespeichert ist
		public float ProducePerSecond { get; private set; } //Strommenge die pro Sekunde produziert wird

		public Vector2 Location { get; private set; } //Wo steht das Gebäude eigentlich
		public Vector2 CenterLocation { get { return new Vector2(Location.X + Graphic.Width / 2f, Location.Y + Graphic.Height / 2f); } }

		public Generator() : base(2, 80, 100f, true, false) {
			UpgradeableGenerator = null;
			GoldCost = 0;//250;

			MaxEnergie = 800f;
			CurrentEnergie = 0f;
			ProducePerSecond = 40f;

			Graphic = Content.Load<Texture2D>(@"Graphics\Generator\Generator1");
			Thumbnail = Content.Load<Texture2D>(@"Graphics\Generator\ThumbGenerator1");
			GraphicAsset = "Generator1";
			ThumbnailAsset = "ThumbGenerator1";

			Location = new Vector2(320, 32);
			base.SetCenterLocationForDrawing(CenterLocation);

			base.TimerTick += new EventHandler(Generator_TimerTick);
			base.EnergieTransfered += new EventHandler<EnergieTransferUnitEventArgs>(Generator_EnergieTransfered);
		}

		void Generator_EnergieTransfered(object sender, EnergieTransferUnitEventArgs e) {
			CurrentEnergie -= e.EnergieUsed;
		}

		//jeder Sekunde wird ein Tick ausgelöst, in diesem Tick soll dann die energie verschoben werden
		void Generator_TimerTick(object sender, EventArgs e) {
			base.TransferEnergie(Math.Min(CurrentEnergie, TransferRate));
		}

		public override void Draw(Spritebatch spriteBatch) {
			base.Draw(spriteBatch);

			spriteBatch.Draw(Graphic, Location, Color.White);
			spriteBatch.DrawText(Math.Round(CurrentEnergie, 0).ToString() + "/" + Math.Round(MaxEnergie, 0).ToString(), SpriteFont.GetFont(10f), new Vector2(Location.X, Location.Y - 10f));
		}

		public override void Update(GameTime time) {
			base.Update(time);
			CurrentEnergie += Math.Min(ProducePerSecond * (float)time.TimeDiff.TotalSeconds, MaxEnergie - CurrentEnergie);
		}

		internal Generator Clone() {
			Generator tmp = new Generator();
			
			return tmp;
		}
	}
}
/*
 * Concept:
 * Der Generator produziert Strom und verteilt es über die Transferline zu den Tower.
 * Die Bridges sind Stromspeicher und können die TransferLines verbinden und trennen.
 * 
 * Es geht also nur Generator -> Bridge -> Tower
 * 
 * Bridges können mehr Strom aufnehmen und an mehr Ziele verteilen.
 * Dafür können nur Generatoren Strom erzeugen.
 * Generatoren können keine Tower versorgen
*/