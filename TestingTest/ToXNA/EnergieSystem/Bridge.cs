﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct3D11;
using SideProjekt.ToXNA;
using Texture2D = TestingTest.Component.Texture2D;
using SideProjekt.Komponents;
using Color = SharpDX.Color;
using SharpDX;
using TestingTest.Component;

namespace TestingTest.ToXNA.EnergieSystem {
	/// <summary>
	/// Stellt den Energiespeicher und den transport zu Türmen da. Der Generator produziert Strom der nur über Brücken zu Türmen verteilt werden kann.
	/// Die Verteilung des Stroms passiert über EnergieTransferUnit.
	/// Der Strom kann nur von Klassen angenommen werden die das Interface IEnergieReceiveUnit ableiten.
	/// </summary>
	public class Bridge : EnergieTransferUnit, IEnergieReceiveUnit {
		public float MaxEnergie { get; private set; } //Jeder Tower braucht Energie und hier wird die Maximale Menge davon Gespeichert die diese Brücke aufnehmen kann
		public float CurrentEnergie { get; private set; } //die Menge an Energie die derzeit hier Gespeichert ist
		private Bridge UpgradeableBridge { get; set; } //Nächst bessere Brücke
		public int GoldCost { get; private set; } //Aktuellen Kosten für diese Brücke

		public string ThumbnailAsset { get; private set; } //Vorschaubild
		public Texture2D Thumbnail { get; private set; }

		public string GraphicAsset { get; private set; } //Aussehen der Brücke
		public Texture2D Graphic { get; private set; }

		public Vector2 Location { get; private set; } //Wo steht das Gebäude eigentlich
		public Vector2 CenterLocation { get { return new Vector2(Location.X + Graphic.Width / 2f, Location.Y + Graphic.Height / 2f); } }
		public Vector2 GetCenterLocation() {return CenterLocation;}

		public Bridge() : base(8, 250f, 250f, true, true) {
			MaxEnergie = 2500f;
			CurrentEnergie = 0f;
			UpgradeableBridge = null;
			GoldCost = 0;//125;

			Graphic = Content.Load<Texture2D>(@"Graphics\Bridge\Bridge1");
			Thumbnail = Content.Load<Texture2D>(@"Graphics\Bridge\ThumbBridge1");
			GraphicAsset = "Bridge1";
			ThumbnailAsset = "ThumbBridge1";

			Location = new Vector2(640, 320);
			base.SetCenterLocationForDrawing(CenterLocation);

			base.TimerTick += new EventHandler(Bridge_TimerTick);
			base.EnergieTransfered += new EventHandler<EnergieTransferUnitEventArgs>(Bridge_EnergieTransfered);
		}

		void Bridge_EnergieTransfered(object sender, EnergieTransferUnitEventArgs e) {
			CurrentEnergie -= e.EnergieUsed;
		}

		void Bridge_TimerTick(object sender, EventArgs e) {
			base.TransferEnergie(Math.Min(CurrentEnergie, TransferRate));
		}

		public void AddEnergie(float amount) {
			CurrentEnergie += Math.Min(amount, MaxEnergie - CurrentEnergie);
		}

		public override void Draw(Spritebatch spriteBatch) {
			base.Draw(spriteBatch);

			foreach (IEnergieReceiveUnit unit in Current_Connections) {
				spriteBatch.DrawLine(CenterLocation, unit.GetCenterLocation(), 2f, Color.Red);
			}

			spriteBatch.Draw(Graphic, Location, Color.White);
			spriteBatch.DrawText(Math.Round(CurrentEnergie, 0).ToString() + "/" + Math.Round(MaxEnergie, 0).ToString(), SpriteFont.GetFont(10f), new Vector2(Location.X, Location.Y - 10f));
		}

		public override void Update(GameTime time) {
			base.Update(time);
		}

		public float GetMaxEnergie() { return MaxEnergie; }
		public float GetCurrentEnergie() { return CurrentEnergie; }
		public float GetUnusedFreeEnergie() { return MaxEnergie - CurrentEnergie; }

		internal Bridge Clone() {
			Bridge tmp = new Bridge();

			return tmp;
		}
	}
}
