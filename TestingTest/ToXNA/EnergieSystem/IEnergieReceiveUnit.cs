﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace TestingTest.ToXNA.EnergieSystem {
	/// <summary>
	/// Zentrale Schnittstelle und einzge Möglichkeit um Strom/Energie anzunehmen.
	/// </summary>
	public interface IEnergieReceiveUnit {
		void AddEnergie(float amount);
		Type GetType();
		float GetMaxEnergie();
		float GetCurrentEnergie();
		float GetUnusedFreeEnergie();
		Vector2 GetCenterLocation();
	}
}
