﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using System.Drawing.Drawing2D;
using SideProjekt.Interfaces;
using System.Collections.Concurrent;
using System.Collections;
using SideProjekt.ToXNA.Battle;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;
using TestingTest.Component;
using TestingTest.ToXNA;
using TestingTest.ToXNA.EnergieSystem;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Tower. Greift Monster an in dem es Kugeln(Bullets) verschießt.
	/// Nur Bullets übertragen den Schaden und Effekte auf die Monster.
	/// Tower können dabei auflevel (der schaden und reichweite verbessern sich)
	/// Tower können komplett zu anderen Tower geändert werden (der level ist danach wieder 1).
	/// </summary>
	public class Tower : IClickable, IEnergieReceiveUnit {
		#region variablen
		private static int ID = 0;
		public int currentID;

		public string ThumbnailAsset { get; private set; } //Vorschaubild
		public Texture2D Thumbnail { get; private set; }

		public string GraphicAsset { get; private set; } //Toweraussehen
		public Texture2D Graphic { get; private set; }

		public string TowerName { get; private set; } //wie heißt der Tower. Like Arrowtower, Poisontower, Firetower

		public int Hitpoints { get; private set; } //Anzahl der Lebenspunkte
		private int MaxHitpoints { get; set; } //Anzahl der MaximalenLebenspunkte

		public Vector2 Location { get; set; } //wo befindet sich der Tower
		public Vector2 CenterLocation { get { return new Vector2(Location.X + Graphic.Width / 2, Location.Y + Graphic.Height / 2); } }

		public bool IsPlaced { get; set; } //wurde der Tower gebaut oder sucht sich der spieler noch einen Platz für den Tower aus
		public bool IsSelected { get; set; } //ist der Tower gerad ausgewählt, dann zeigen radius an
		public int DmgCircleWidth { get; private set; } //wie weit kann der Tower Monster angreifen

		internal Wave currentWave { get; private set; } //ein Tower muss Wissen welche Wave derzeit Läuft.

		private float SellScale { get; set; }
		public event EventHandler TowerIsSelected;
		public event EventHandler TowerIsUnSelected;
		public event EventHandler<TowerEventArgs> TowerWasBuild;
		public event EventHandler<TowerSellEventArgs> TowerWasSelled;
		public event EventHandler<TowerEventArgs> TowerWasDestroyed;

		//Tower Kosten
		public int GoldCost { get; private set; } //jeder Turm kostet Gold
		public int EnergieCost { get; private set; } //elektische Türme verbrauchen Storm pro Schuß

		//forAttack
		#region Tower Attack
		public float AttackSpeed { get; private set; } //geschwindigkeit für den Interval wann geschossen wird
		private float AttackSpeedTimer { get; set; } //timer um den Interval wie wann geschossen wird zu berechnen
		private int maxTargets { get; set; } //wieviele Ziele kann der Tower gleichzeitig anwiesieren
		public Bullet BaseBullet { get; private set; } //welchen Schußtyp hat er eigentlich (Aussehen, Dmg Type, Dmg, Geschwindigkeit usw.)
		private List<Bullet> BulletsShooted { get; set; } //aktuell abgefeuerten Geschosse
		private List<Bullet> BulletsToRemove { get; set; } //aktuell abgefeuerten Geschosse
		private bool WaitingForShot { get; set; } //kann der tower sofort einen schuß abgeben? (ignoriert den AtkSppedTimer)
		public Player Owner { get; private set; }
		public float MountForEffekt { get; private set; }
		public bool DamageIsAmount { get; private set; }
		#endregion

		//tower damage
		#region Tower Damage
		public string DamageString { get { return ThrowCount == 0 || ThrowDamage == 0 ? Damage.ToString() : string.Concat((Damage + ThrowCount * 1), " bis ", (Damage + ThrowCount * ThrowDamage)); } }
		public int CurrentDamage { get { return Damage + Dice.GetManyRandomNumber(ThrowDamage, ThrowCount); } } //dmg + ((BETWEEN 1 AND ThrowDamage) * ThrowCount)
		public int Damage { get; private set; }
		public int ThrowDamage { get; private set; } //mit was für ein würfel wird geworfen
		public int ThrowCount { get; private set; } //wieviele würfe stehen zur Verfügung
		public Element Element { get; private set; } //todo set element and attack
		public AttackType Attack { get; private set; }
		public float DPS {
			get {
				if (BaseBullet.EffektType != "PoisonEffekt") {
					return (float)(Math.Round((((Damage + ThrowCount * 1) + (Damage + ThrowCount * ThrowDamage)) / 2f) * (1f / AttackSpeed), 2) * maxTargets);
				} else {
					return (float)(Math.Round(((((Damage + ThrowCount * 1) + (Damage + ThrowCount * ThrowDamage)) / 2f) / BaseBullet.Time) * (1f / AttackSpeed), 2) * maxTargets);
				}
			}
		}
		#endregion

		//Upgrade System
		#region UpgradeSystem
		//todo implement towerUpgrade system (funktioniert schon mit Test Daten, es fehlt noch noch das laden der Tower aus dem Level und das bestimmte Tower dabei nur Upgrade Tower sind.)
		public event EventHandler<TowerEventArgs> TowerWasUpgraded;
		private List<Tower> UpgradeTowers; //speichert sämtliche Towers zu dennen er Tower Upgegradet werden kann

		public Tower GetUpgradedTower(int index) {
			if (index < 0 || index > UpgradeTowers.Count() - 1) {
				return null;
			}
			Tower t = UpgradeTowers[index].Clone(Owner);
			//recalc tower cost (Tower wird um den aktuellen Tower billiger, sodass die Tower auch wiederverwendet werden können)
			t.SetTowerCosts(Math.Max(t.GoldCost - this.GoldCost, 0), Math.Max(t.EnergieCost - this.EnergieCost, 0));
			return t;
		}

		public Tower GetUpgradedTower(string name) {
			Tower t = UpgradeTowers.Where(x => x.TowerName == name).FirstOrDefault();
			if (t == null) {
				return null;
			} else {
				t = t.Clone(Owner);
				//recalc tower cost (Tower wird um den aktuellen Tower billiger, sodass die Tower auch wiederverwendet werden können)
				t.SetTowerCosts(Math.Max(t.GoldCost - this.GoldCost, 0), Math.Max(t.EnergieCost - this.EnergieCost, 0));
				return t;
			}
		}

		public int UpgraedTowerCount { get { return UpgradeTowers.Count; } }

		//warum hier index verwendet wird, ich möchte den originalen tower haben, nicht den mit den reduzierten kosten
		public bool Upgrade(int index) {
			if (index < 0 || index > UpgradeTowers.Count() - 1) {
				return false;
			} else {
				Tower t = UpgradeTowers[index].Clone(Owner);
				DoUpgrade(t);

				if (TowerWasUpgraded != null) {
					TowerWasUpgraded(this, new TowerEventArgs(Owner, this));
				}
				return true;
			}
		}

		//warum hier name verwendet wird, ich möchte den originalen tower haben, nicht den mit den reduzierten kosten
		public bool Upgrade(string name) {
			Tower t = UpgradeTowers.Where(x => x.TowerName == name).FirstOrDefault();
			if (t == null) {
				return false;
			} else {
				t = t.Clone(Owner);
				DoUpgrade(t);

				if (TowerWasUpgraded != null) {
					TowerWasUpgraded(this, new TowerEventArgs(Owner, this));
				}
				return true;
			}
		}

		public void AddUpgradeableTower(Tower t) {
			UpgradeTowers.Add(t);
		}

		private void DoUpgrade(Tower t) {
			this.AttackSpeed = t.AttackSpeed;
			this.BaseBullet = t.BaseBullet;
			this.Damage = t.Damage;
			this.DmgCircleWidth = t.DmgCircleWidth;
			this.EnergieCost = t.EnergieCost;
			this.GoldCost = t.GoldCost;
			this.Graphic = t.Graphic;
			this.GraphicAsset = t.GraphicAsset;
			this.Hitpoints = t.Hitpoints;
			this.MaxHitpoints = t.MaxHitpoints;
			this.maxTargets = t.maxTargets;
			this.SellScale = t.SellScale;
			this.ThrowCount = t.ThrowCount;
			this.ThrowDamage = t.ThrowDamage;
			this.Thumbnail = t.Thumbnail;
			this.ThumbnailAsset = t.ThumbnailAsset;
			this.TowerName = t.TowerName;
			this.UpgradeTowers = t.UpgradeTowers;
			this.MountForEffekt = t.MountForEffekt;
			this.DamageIsAmount = t.DamageIsAmount;
		}
		#endregion

		//LevelUp System
		#region LevelUp System
		public int Level { get; private set; }
		public int LevelUpGoldCost { get { return (int)(GoldCost * (0.4f + Level / 10f)); } }

		public void LevelUp() {
			Level += 1;

			this.Damage = 1 + (int)(Damage * 1.1f);
			this.DmgCircleWidth = 1 + (int)(DmgCircleWidth * 1.1f);
			this.MaxHitpoints = (int)(MaxHitpoints * 1.1f);
			this.Hitpoints = (int)(Hitpoints * 1.1f);
			this.MountForEffekt = (int)(MountForEffekt * 1.1f);
			this.ThrowCount = (int)(ThrowCount * 1.1f);
			this.ThrowDamage = 1 + (int)(ThrowDamage * 1.1f);
			this.AttackSpeed = AttackSpeed * 0.95f;
		}
		#endregion

		//Energie System
		#region EnergieSystem
		public event EventHandler EnergieRecieved;

		public float EnergieNeededPerShot { get; private set; }
		private float CurrentEnergie { get; set; }

		//From Interface
		public void AddEnergie(float amount) { 
			float realamount = Math.Min(amount, GetUnusedFreeEnergie()); 
			CurrentEnergie += realamount;
			if (realamount > 0f) {
				if (EnergieRecieved != null) {
					EnergieRecieved(this, null);
				}
			} 
		}
		public Type GetType() { return typeof(Tower); }
		public float GetMaxEnergie() { return (float)Math.Round(((1f / AttackSpeed) * EnergieNeededPerShot * maxTargets) * 2, 1); } //genügend Energie für 2 Sekunden
		public float GetCurrentEnergie() { return CurrentEnergie; }
		public float GetUnusedFreeEnergie() { return GetMaxEnergie() - GetCurrentEnergie(); }
		public Vector2 GetCenterLocation() { return CenterLocation; }

		public bool CanShot { get { return CurrentEnergie >= EnergieNeededPerShot; } }
		#endregion
		#endregion

		//TODO implement tower statistiks

		#region kontruktoren
		public Tower(string thumbnailAsset, string graphicAsset, string name, int hitPoints, Vector2 location, bool isPlaced, bool isSelected, int circleWidth, Wave wave, Bullet bullet, float attackSpeed, Player owner) {
			this.ThumbnailAsset = thumbnailAsset;
			this.GraphicAsset = graphicAsset;
			this.TowerName = name;
			this.Hitpoints = hitPoints;
			this.MaxHitpoints = this.Hitpoints;
			this.Location = location;
			this.IsPlaced = IsPlaced;
			this.IsSelected = isSelected;
			this.DmgCircleWidth = circleWidth;
			this.currentWave = wave;
			this.BaseBullet = bullet;
			this.DamageIsAmount = true;
			this.MountForEffekt = 0f;

			this.maxTargets = 1; //todo maybe editable throw config
			this.AttackSpeed = attackSpeed;
			this.AttackSpeedTimer = 0f;
			BulletsShooted = new List<Bullet>();
			BulletsToRemove = new List<Bullet>();
			WaitingForShot = true;

			SellScale = 0.75f;

			this.Thumbnail = Content.Load<Texture2D>(@"Graphics\Tower\" + this.ThumbnailAsset);
			this.Graphic = Content.Load<Texture2D>(@"Graphics\Tower\" + this.GraphicAsset);

			SetTowerCosts(0, 0);
			SetTowerDamage(0, 0, 0);
			Attack = AttackType.Physical;
			Element = Element.None;

			this.Owner = owner;
			this.Level = 1;

			ID += 1;
			currentID = ID;

			UpgradeTowers = new List<Tower>();

			CurrentEnergie = 0f;
			EnergieNeededPerShot = 1000f;
		}

		public Tower(string thumbnailAsset, string graphicAsset, string name, int hitPoints, int circleWidth, Bullet bullet, float attackSpeed, Player owner) {
			this.ThumbnailAsset = thumbnailAsset;
			this.GraphicAsset = graphicAsset;
			this.TowerName = name;
			this.Hitpoints = hitPoints;
			this.MaxHitpoints = this.Hitpoints;
			this.IsPlaced = false;
			this.IsSelected = false;
			this.DmgCircleWidth = circleWidth;
			this.BaseBullet = bullet;
			this.Location = Vector2.Zero;
			this.DamageIsAmount = true;
			this.MountForEffekt = 0f;

			this.maxTargets = 1;
			this.AttackSpeed = attackSpeed;
			this.AttackSpeedTimer = 0f;
			BulletsShooted = new List<Bullet>();
			BulletsToRemove = new List<Bullet>();
			WaitingForShot = true;

			SellScale = 0.75f;

			this.Thumbnail = Content.Load<Texture2D>(@"Graphics\Tower\" + this.ThumbnailAsset);
			this.Graphic = Content.Load<Texture2D>(@"Graphics\Tower\" + this.GraphicAsset);

			SetTowerCosts(0, 0);
			SetTowerDamage(0, 0, 0);
			Attack = AttackType.Physical;
			Element = Element.None;

			this.Owner = owner;
			this.Level = 1;

			ID += 1;
			currentID = ID;

			UpgradeTowers = new List<Tower>();

			CurrentEnergie = 0f;
			EnergieNeededPerShot = 1000f;
		}

		public void SetTowerCosts(int gold, int energie) {
			this.GoldCost = gold;
			this.EnergieCost = energie;
		}

		public void SetTowerDamage(int dmg, int throwCount, int throwDmg) {
			this.Damage = dmg;
			this.ThrowCount = throwCount;
			this.ThrowDamage = throwDmg;
		}

		public void SetTowerAmountForEffekt(float amount, bool damageIsAmount) {
			if (!damageIsAmount) {
				this.MountForEffekt = amount;
				this.DamageIsAmount = damageIsAmount;
			}
		}
		#endregion

		#region xna
		public void Update(GameTime time) {
			if (IsPlaced && isEnable) {

				AttackSpeedTimer += (float)time.TimeDiff.TotalSeconds;
				if (AttackSpeedTimer > AttackSpeed || WaitingForShot) {
					AttackSpeedTimer = 0;

					int currentTarget = 0;//wieviele ziele wurden derzeit angegriffen
					foreach (Monster m in currentWave.MonsterOnField) {
						if (MathHelper.CircleIntersectsRectangle(CenterLocation.X, CenterLocation.Y, DmgCircleWidth / 2, m.Rectangle) && currentTarget < maxTargets) {//befindet sich ein monster im gebiet und kann dazu noch angegriffen werden
							WaitingForShot = false;
							PerformShoot(m);//löse einen schuß aus
							currentTarget += 1;//ein weiteres ziel wurde angegriffen
							if (currentTarget == maxTargets) { //brich alles ab wenn du alle ziele getroffen hast, wäre ja blödsinnig trotzdem alle weiteren monster zu durchlaufen
								break;
							}
						}
					}

					if (currentTarget == 0) {
						WaitingForShot = true;
					}
				}

				lock (((ICollection)BulletsShooted).SyncRoot) {
					foreach (Bullet b in BulletsShooted) {
						b.Update(time);
					}
				}

				foreach (Bullet b in BulletsToRemove) {
					b.Graphic.Dispose();
					BulletsShooted.Remove(b);
				}
				BulletsToRemove.Clear();
			}
		}

		public void Draw(Spritebatch batch) {
			if (isEnable) {
				batch.Draw(Graphic, new Rectangle(0, 0, Graphic.Width, Graphic.Height), new Rectangle(Convert.ToInt32(Location.X), Convert.ToInt32(Location.Y), Graphic.Width, Graphic.Height), Color.White);
				if (Hitpoints != MaxHitpoints) {
					Texture2D hitpointsbar = CreateTowerHitpointsBar();
					batch.Draw(hitpointsbar, new Rectangle(0, 0, Graphic.Width, Graphic.Height), new Rectangle(Convert.ToInt32(Location.X), Convert.ToInt32(Location.Y) + Graphic.Height + 5, hitpointsbar.Width, hitpointsbar.Height), Color.White);
				}

				if (IsSelected) {
					batch.DrawCircle(DmgCircleWidth, Color.Red, this.CenterLocation);
				}

				List<Bullet> ListB = BulletsShooted;
				ListB.ForEach(x => x.Draw(batch));
			}
		}
		#endregion

		#region methoden
		public void SellTower() {
			if (TowerWasSelled != null) {
				TowerWasSelled(this, new TowerSellEventArgs(Owner, this, (int)(GoldCost * SellScale), (int)(EnergieCost * SellScale)));
			}
		}

		public void TakeDamge(int amount) {
			Hitpoints -= amount;

			if (Hitpoints <= 0) {
				Hitpoints = 0;
				if (TowerWasDestroyed != null) {
					TowerWasDestroyed(this, new TowerEventArgs(Owner, this));
				}
			}
		}

		private void PerformShoot(Monster m) {
			Bullet b = new Bullet(BaseBullet.Name, BaseBullet.EffektType, BaseBullet.GraphicAsset, BaseBullet.FlightSpeed, m.CenterLocation, this.CenterLocation, this, CurrentDamage, BaseBullet.AreaWidth, BaseBullet.Time, this.Element.Clone(), this.Attack, BaseBullet.HasDirektDamage, m);
			b.AddEffekt(EffektManager.Get(BaseBullet.EffektType, DamageIsAmount ? CurrentDamage : MountForEffekt, BaseBullet.Time, (int)BaseBullet.AreaWidth));
			BulletsShooted.Add(b);
		}

		public void SetCurrentWave(Wave w) {
			currentWave = w;
			Hitpoints = MaxHitpoints;
			foreach (Bullet b in BulletsShooted) {
				b.Graphic.Dispose();
			}
			BulletsShooted.Clear();
			BulletsToRemove.Clear();
		}

		public void RemoveBullet(Bullet b) {
			lock (((ICollection)BulletsShooted).SyncRoot) {
				BulletsToRemove.Add(b);
			}
		}

		//nothing to do only Event
		public void TowerWasBuildet(Player p) {
			if (TowerWasBuild != null) {
				TowerWasBuild(this, new TowerEventArgs(p, this));
			}
		}
		#endregion

		#region funktionen
		private Texture2D CreateTowerHitpointsBar() {
			Bitmap b = new Bitmap(Graphic.Width, 5, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			for (int i = 0; i < b.Width; i++) {
				System.Drawing.Color c = System.Drawing.Color.Green;

				if (b.Width * Hitpoints * 100 / MaxHitpoints / 100 <= i) {
					c = System.Drawing.Color.Red;
				}

				b.SetPixel(i, 0, c);
				b.SetPixel(i, 1, c);
				b.SetPixel(i, 2, c);
				b.SetPixel(i, 3, c);
				b.SetPixel(i, 4, c);
			}

			Texture2D green = new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, b));
			return green;
		}

		internal Tower Clone(Player owner) {
			Tower t = new Tower(this.ThumbnailAsset, this.GraphicAsset, this.TowerName, this.Hitpoints, new Vector2(Location.X, Location.Y), this.IsPlaced, this.IsSelected, this.DmgCircleWidth, this.currentWave, this.BaseBullet, this.AttackSpeed, owner);
			t.maxTargets = this.maxTargets;
			t.AttackSpeed = this.AttackSpeed;
			t.AttackSpeedTimer = this.AttackSpeedTimer;
			t.BaseBullet = this.BaseBullet.Clone();
			t.BulletsShooted = new List<Bullet>(); //wenn der Tower kopiert wird, gibt es keine geschossenen Geschosse
			t.Level = this.Level;

			t.SetTowerCosts(this.GoldCost, this.EnergieCost);
			t.SetTowerDamage(this.Damage, this.ThrowCount, this.ThrowDamage);
			t.SetTowerAmountForEffekt(this.MountForEffekt, this.DamageIsAmount);
			return t;
		}
		#endregion

		#region interfaceimplementierung
		public void PerformClick(Vector2 position) {
			if (IsSelected) {
				IsSelected = false;
				if (TowerIsUnSelected != null) {
					TowerIsUnSelected(this, null);
				}
			} else {
				IsSelected = true;
				if (TowerIsSelected != null) {
					TowerIsSelected(this, null);
				}
			}
		}
		public SharpDX.Rectangle GetPosition() {
			return new Rectangle((int)Location.X, (int)Location.Y, Graphic.Width, Graphic.Height);
		}

		public bool IsEnable() {
			return isEnable;
		}

		public bool IsClickable() {
			return true;
		}

		bool isEnable = true;
		public void SetIsEnable(bool enable) { isEnable = enable; }
		#endregion
	}

	public class TowerEventArgs : EventArgs {
		public Player Owner { get; private set; }
		public Tower Tower { get; private set; }

		public TowerEventArgs(Player p, Tower t) {
			Owner = p;
			Tower = t;
		}
	}

	public class TowerSellEventArgs : EventArgs {
		public Player Owner { get; private set; }
		public Tower Tower { get; private set; }

		public int Gold { get; private set; } //jeder Turm kostet Gold
		public int Energie { get; private set; } //elektische Türme verbrauchen Storm pro Schuß

		public TowerSellEventArgs(Player p, Tower t, int gold, int energie) {
			Owner = p;
			Tower = t;

			Gold = gold;
			Energie = energie;
		}
	}
}
