﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.Animation {
	/// <summary>
	/// Animation eines Bildes.
	/// </summary>
	public class Animation {
		private List<Texture2D> AnimationTiles { get; set; }
		private float AnimationSpeed { get; set; }
		private float AnimationSpeedTimer { get; set; }
		private bool DoLoop { get; set; }
		private int CurrentTileIndex { get; set; }
		public Texture2D CurrentGraphic { get { return AnimationTiles[CurrentTileIndex]; } }

		private int step = 1;
		public void Update(GameTime time) {
			AnimationSpeedTimer += (float)time.TimeDiff.TotalSeconds;

			if (AnimationSpeedTimer > AnimationSpeed) {
				AnimationSpeedTimer = 0f;
				CurrentTileIndex += step;
				
				if (CurrentTileIndex >= AnimationTiles.Count) {
					step = -1;
					CurrentTileIndex -= 2;
				}

				if (CurrentTileIndex < 0) {
					step = 1;
					CurrentTileIndex += 2;
				}
			}
		}

		public void Draw(Spritebatch batch, Vector2 position) {
			batch.Draw(AnimationTiles[CurrentTileIndex], new Rectangle(0, 0, AnimationTiles[CurrentTileIndex].Width, AnimationTiles[CurrentTileIndex].Height), new Rectangle((int)position.X, (int)position.Y, AnimationTiles[CurrentTileIndex].Width, AnimationTiles[CurrentTileIndex].Height), Color.White);
		}

		public Animation(List<Texture2D> animationTiles, float speed) {
			AnimationTiles = animationTiles;
			AnimationSpeed = speed;
			DoLoop = true;
			CurrentTileIndex = 0;
			AnimationSpeedTimer = 0f;
		}

		public void ResetAnimation() {
			CurrentTileIndex = 0;
			step = 1;
		}

		public int Width {
			get {
				return AnimationTiles[CurrentTileIndex].Width;
			}
		}

		public int Height {
			get {
				return AnimationTiles[CurrentTileIndex].Height;
			}
		}

		internal Animation Clone() {
			List<Texture2D> tmpList = new List<Texture2D>();

			foreach (Texture2D t in AnimationTiles) {
				tmpList.Add(new Texture2D(t.Texture));
			}

			Animation tmp = new Animation(tmpList, this.AnimationSpeed);
			return tmp;
		}

		internal void Dispose() {
			foreach (Texture2D t in AnimationTiles) {
				t.Dispose();
			}
			AnimationTiles.Clear();
		}
	}
}
