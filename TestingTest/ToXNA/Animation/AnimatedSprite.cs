﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.Animation {
	public enum AnimationDirection { 
		UP,
		DOWN,
		LEFT,
		RIGHT
	}

	/// <summary>
	/// Sammlung von Animationen. Es gibt 4 Blickrichtungen dabei. Animationen laufen dabei auch selbstständig ab.
	/// </summary>
	public class AnimatedSprite {
		private Dictionary<AnimationDirection, Animation> Animations { get; set; }
		private AnimationDirection CurrentDicretion { get; set; }
		public string Title { get; private set; }
		public Texture2D CurrentGraphic { get { return Animations[CurrentDicretion].CurrentGraphic; } }

		private AnimatedSprite() {
			Animations = new Dictionary<AnimationDirection, Animation>();
		}

		/// <summary>
		/// Vorgabe der animationen sind: DOWN, LEFT, RIGHT, UP
		/// </summary>
		/// <param name="image"></param>
		/// <param name="tileWidth"></param>
		/// <param name="tileHight"></param>
		public AnimatedSprite(string title, System.Drawing.Bitmap image, int widthTileCount, int hightTileCount, float animationspeed) {
			Animations = new Dictionary<AnimationDirection, Animation>();
			this.Title = title;
			int tileWidth = image.Width / widthTileCount;
			int tileHeight = image.Height / hightTileCount;

			List<Texture2D> AnimtaionTiles = new List<Texture2D>();
			AnimationDirection direction = AnimationDirection.DOWN;

			for (int x = 0; x < hightTileCount; x++) {
				for (int y = 0; y < widthTileCount; y++) {
					AnimtaionTiles.Add(new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, image.Clone(new System.Drawing.Rectangle((int)y * tileWidth, (int)x * tileHeight, tileWidth, tileHeight), System.Drawing.Imaging.PixelFormat.Format32bppArgb))));
				}
				Animation ani = new Animation(AnimtaionTiles, animationspeed);
				Animations.Add(direction, ani);
				AnimtaionTiles = new List<Texture2D>();
				if (direction == AnimationDirection.DOWN) {
					direction = AnimationDirection.LEFT;
				} else if (direction == AnimationDirection.LEFT) {
					direction = AnimationDirection.RIGHT;
				} else if (direction == AnimationDirection.RIGHT) {
					direction = AnimationDirection.UP;
				}
			}
		}

		public void Update(GameTime time) {
			Animations[CurrentDicretion].Update(time);
		}

		public void Draw(Spritebatch batch, Vector2 position) {
			Animations[CurrentDicretion].Draw(batch, position);
		}

		public void SetDirection(AnimationDirection d) {
			if (CurrentDicretion != d) {
				CurrentDicretion = d;
				Animations[CurrentDicretion].ResetAnimation();
			}
		}

		public int Width {
			get {
				return Animations[CurrentDicretion].Width;
			}
		}

		public int Height {
			get {
				return Animations[CurrentDicretion].Height;
			}
		}

		internal AnimatedSprite Clone() { 
			AnimatedSprite ani = new AnimatedSprite();
			ani.Title = this.Title;
			ani.Animations = new Dictionary<AnimationDirection,Animation>();

			foreach (KeyValuePair<AnimationDirection, Animation> pair in Animations) {
				ani.Animations.Add(pair.Key, pair.Value.Clone());
			}

			ani.CurrentDicretion = this.CurrentDicretion;

			return ani;
		}

		internal void Dispose() {
			foreach (KeyValuePair<AnimationDirection, Animation> pair in Animations) {
				pair.Value.Dispose();
			}
		}
	}
}
