﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SideProjekt.ToXNA.Log {
	
	/// <summary>
	/// Output von Logger als Konsole
	/// </summary>
	public class ConsoleLogger : iLogOutput {
		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		public void Log(LogEntity l) {
			if (l.Type == LogType.Error) {
				Console.ForegroundColor = ConsoleColor.Red;
			} else if (l.Type == LogType.Debug) {
				Console.ForegroundColor = ConsoleColor.Yellow;
			} else {
				Console.ForegroundColor = ConsoleColor.White;
			}

			Console.WriteLine(string.Format("{0} {1}: {2}", l.TimeStamp, l.Type, l.Text));
		}

		public void Init() {
			AllocConsole();
		}

		public void GetAllEntity(List<LogEntity> l) {
			foreach (LogEntity e in l) {
				Log(e);
			}
		}

		public void Dispose() { 
		
		}
	}
}
