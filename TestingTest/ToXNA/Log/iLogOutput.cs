﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.ToXNA.Log {
	/// <summary>
	/// Schnittstelle für den Output von Logger. z.B.: Konsole, txt, html, ingame
	/// </summary>
	public interface iLogOutput {
		void Log(LogEntity l);
		void Init();
		void GetAllEntity(List<LogEntity> l);
		void Dispose();
	}
}
