﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.ToXNA.Log {
	/// <summary>
	/// Der eigentliche Logger.
	/// Hier wird alles eingetragen was man speichern möchte.
	/// Die Ausgabe erfolgt über die jeweilige Schnittstelle, welche sich auch zur Laufzeit ändern lässt.
	/// </summary>
	public static class Log {
		private static List<LogEntity> LogDataBase = new List<LogEntity>();
		private static iLogOutput Output { get; set; }

		public static void Add(string text, LogType type) {
			LogEntity l = new LogEntity(text, type);
			LogDataBase.Add(l);
			if (Output != null) {
				Output.Log(l);
			}
		}

		public static void Add(string text) {
			Add(text, LogType.Normal);
		}

		public static void SetOutput (iLogOutput outp) {
			if(Output != null) 
				Output.Dispose();
			Output = outp;
			Output.Init();
			Output.GetAllEntity(LogDataBase);
		}
	}
}
