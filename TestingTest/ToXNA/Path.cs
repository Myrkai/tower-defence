﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SideProjekt.TileEngine;
using SharpDX;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Der Pfad auf den die Monster sich bewegen.
	/// </summary>
	public class Path {
		public List<PathNode> Nodes { get; private set; }
		private int CurrentNodeIndex { get; set; }

		public Path() {
			Nodes = new List<PathNode>();
		}

		public Path(List<PathNode> nodes) {
			Nodes = nodes;
			CurrentNodeIndex = 0;
		}

		public void GoToPreviewNode() {
			CurrentNodeIndex -= 1;
		}

		public PathNode GetNextNode() {
			if (CurrentNodeIndex == Nodes.Count - 1) {
				return GetCurrentNode();
			}

			CurrentNodeIndex += 1;
			return Nodes[CurrentNodeIndex];
		}

		public PathNode GetCurrentNode() {
			return Nodes[CurrentNodeIndex];
		}

		public Vector2 GetNextNodeAsVector() {
			if (CurrentNodeIndex == Nodes.Count - 1) {
				return GetCurrentNodeAsVector();
			}

			CurrentNodeIndex += 1;
			return new Vector2(Nodes[CurrentNodeIndex].X, Nodes[CurrentNodeIndex].Y);
		}

		public Vector2 GetCurrentNodeAsVector() {
			return new Vector2(Nodes[CurrentNodeIndex].X, Nodes[CurrentNodeIndex].Y);
		}

		public Vector2 NotCentert(Vector2 n) {
			return n - new Vector2(Tile.TileWidth / 2, Tile.TileHeight / 2);
		}

		public Vector2 LastNode { get { return new Vector2(Nodes[Nodes.Count - 1].X, Nodes[Nodes.Count - 1].Y); } }

		public void AddNewNode(PathNode node) {
			Nodes.Add(node);
		}

		public void AddNewNode(Vector2 v) {
			Nodes.Add(new PathNode((int)v.X, (int)v.Y));
		}

		public void AddNewNode(int x, int y) {
			Nodes.Add(new PathNode(x, y));
		}

		public void AddNewNode(MapCell cell) {
			Nodes.Add(new PathNode((int)cell.Position.X * Tile.TileWidth + Tile.TileWidth / 2, (int)cell.Position.Y * Tile.TileHeight + Tile.TileHeight / 2));
		}

		internal Path Clone() {
			Path r = new Path(Nodes);
			return r;
		}
	}

	public class PathNode {
		public int CenterX;
		public int CenterY;

		public int IndexX { get { return (CenterX - Tile.TileWidth / 2) / Tile.TileWidth; } }
		public int IndexY { get { return (CenterY - Tile.TileHeight / 2) / Tile.TileHeight; } }

		public int X { get { return (CenterX - Tile.TileWidth / 2); } }
		public int Y { get { return (CenterY - Tile.TileHeight / 2); } }

		public PathNode(int x, int y) {
			this.CenterX = x;
			this.CenterY = y;
		}

		public PathNode(int indexX, int indexY, int MapTileHeight, int MapTileWidth) {
			this.CenterX = indexX * MapTileWidth + MapTileWidth / 2;
			this.CenterY = indexY * MapTileHeight + MapTileHeight / 2;
		}

		public void SetNodeFromMapIndex(int indexX, int indexY, int MapTileHeight, int MapTileWidth) {
			this.CenterX = indexX * MapTileWidth + MapTileWidth / 2;
			this.CenterY = indexY * MapTileHeight + MapTileHeight / 2;
		}
	}
}
