﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using TestingTest.ToXNA.EnergieSystem;
using SharpDX;
using System.Collections.ObjectModel;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Der aktuelle Spieler.
	/// Dieser hat die gebauten Tower udn Gold, Leben, Strom, Punkte.
	/// Später auch noch die jeweiligen Forschungen und das Inventar.
	/// </summary>
	public class Player {
		public int Score { get; private set; }
		public int Gold { get; private set; } //jeder Turm kostet Gold
		public int Energie { get; private set; } //elektische Türme verbrauchen Storm pro Schuß
		public int Lives { get; private set; } //Anzahl der Leben die ein Spiele hat bis er stirbt (z.B. wenn der Monster einfach durchlässt);
		public string Name { get; private set; }

		public string ScoreIconAsset { get; private set; }
		public string GoldIconAsset { get; private set; }
		public string EnergieIconAsset { get; private set; }
		public string LivesIconAsset { get; private set; }

		private Texture2D ScoreIcon { get; set; }
		private Texture2D GoldIcon { get; set; }
		private Texture2D EnergieIcon { get; set; }
		private Texture2D LivesIcon { get; set; }

		//towers
		private List<Tower> Buildet_Towers { get; set; } //sind alle Türme die der Spieler bis jetzt gebaut hat.
		public ReadOnlyCollection<Tower> BuildetTowers { get { return new ReadOnlyCollection<Tower>(Buildet_Towers); } }
		public event EventHandler<PlayerTowerEventArgs> TowerIsSelected;
		public event EventHandler<PlayerTowerEventArgs> TowerIsUnSelected;
		public event EventHandler<TowerEventArgs> TowerWasBuild;
		public event EventHandler<TowerEventArgs> TowerWasDestroyed;
		//Generators
		private List<Generator> Buildet_Generators { get; set; } //sind alle Generatoren die der Spieler bis jetzt gebaut hat.
		public ReadOnlyCollection<Generator> BuildetGenerators { get { return new ReadOnlyCollection<Generator>(Buildet_Generators); } }
		public event EventHandler<PlayerGeneratorEventArgs> GeneratorIsSelected;
		public event EventHandler<PlayerGeneratorEventArgs> GeneratorIsUnSelected;
		public event EventHandler<PlayerGeneratorEventArgs> GeneratorWasBuild;
		//Bridge
		private List<Bridge> Buildet_Bridges { get; set; } //sind alle Generatoren die der Spieler bis jetzt gebaut hat.
		public ReadOnlyCollection<Bridge> BuildetBridges { get { return new ReadOnlyCollection<Bridge>(Buildet_Bridges); } }
		public event EventHandler<PlayerBridgeEventArgs> BridgeIsSelected;
		public event EventHandler<PlayerBridgeEventArgs> BridgeIsUnSelected;
		public event EventHandler<PlayerBridgeEventArgs> BridgeWasBuild;

		public event EventHandler PlayerHasNoLives;

		//Ausgewählte Sachen
		private Tower towerToBuild; //aktueller tower der gebaut werden soll
		private Generator generatorToBuild; //aktueller Generator der gebaut werden soll
		private Bridge bridgeToBuild; //aktuelle Brücke die gebaut werden soll

		#region Energie System
		public float CurrentEnergie { get { return BuildetTowers.Sum(x => x.GetCurrentEnergie()); } }
		public float MaxEnergie { get { return BuildetTowers.Sum(x => x.GetMaxEnergie()); } }
		public float EnergieCostPerSecond { get { return BuildetTowers.Sum(x => 1f / x.AttackSpeed * x.EnergieCost); } }
		#endregion

		#region Konstruktoren
		public Player() {
			Buildet_Towers = new List<Tower>();
			Buildet_Bridges = new List<Bridge>();
			Buildet_Generators = new List<Generator>();
			Name = "Ohne Name";

			SetPlayerAmounts(0, 0, 0, 1);
		}

		public Player(string name) {
			Buildet_Towers = new List<Tower>();
			Buildet_Bridges = new List<Bridge>();
			Buildet_Generators = new List<Generator>();
			Name = name;

			SetPlayerAmounts(0, 0, 0, 1);
		}

		public void SetPlayerAmounts(int score, int gold, int energie, int lives) {
			this.Score = score;
			this.Gold = gold;
			this.Energie = energie;
			this.Lives = lives;

			if (Lives <= 0) {
				if (PlayerHasNoLives != null) {
					PlayerHasNoLives(this, null);
				}
			}
		}

		public void AddPlayerAmounts(int score, int gold, int energie, int lives) {
			this.Score += score;
			this.Gold += gold;
			this.Energie += energie;
			this.Lives += lives;

			if (Lives <= 0) {
				if (PlayerHasNoLives != null) {
					PlayerHasNoLives(this, null);
				}
			}
		}
		#endregion

		public void Update(GameTime time) {
			foreach (Tower t in BuildetTowers) {
				t.Update(time);
			}
			Buildet_Generators.ForEach(x => x.Update(time));
			Buildet_Bridges.ForEach(x => x.Update(time));
		}

		public void Draw(Spritebatch batch) {
			Buildet_Towers.ForEach(t => t.Draw(batch));
			Buildet_Generators.ForEach(x => x.Draw(batch));
			Buildet_Bridges.ForEach(x => x.Draw(batch));
			DrawStuffToBuild(batch);
		}

		public Tower GetBuildetTowerByID(string id) {
			return BuildetTowers.Where(x => x.currentID.ToString() == id).FirstOrDefault();
		}

		/// <summary>
		/// Fügt einen Tower dem Spieler hin. Der Spieler hat also einen neuen Tower gebaut.
		/// Überprüfung: ist der Feld noch frei wo der Tower gebaut werden soll.
		/// Zusatz: kann sich der Spieler auch den Tower leisten.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public bool AddBuildetTower(Tower t) {
			if (BuildetTowers.Where(x => x.GetPosition() == t.GetPosition()).Count() == 0) {
				if (Gold - t.GoldCost >= 0
					&& Energie - t.EnergieCost >= 0) {

					Buildet_Towers.Add(t);
					t.TowerWasBuildet(this);
					t.TowerIsSelected += new EventHandler(t_TowerIsSelected);
					t.TowerIsUnSelected += new EventHandler(t_TowerIsUnSelected);
					t.TowerWasDestroyed += new EventHandler<TowerEventArgs>(t_TowerWasDestroyed);

					if (TowerWasBuild != null) {
						TowerWasBuild(this, new TowerEventArgs(this, t));
					}

					this.Gold -= t.GoldCost;
					this.Energie -= t.EnergieCost;

					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		public bool AddAndBuildGenerator(Generator g) {
			if (BuildetGenerators.Where(x => x.Location == g.Location).Count() == 0) {
				if (Gold - g.GoldCost >= 0) {

					Buildet_Generators.Add(g);
					if (GeneratorWasBuild != null) {
						GeneratorWasBuild(this, new PlayerGeneratorEventArgs(this, g));
					}

					this.Gold -= g.GoldCost;

					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		public bool AddAndBuildBridge(Bridge b) {
			if (BuildetBridges.Where(x => x.Location == b.Location).Count() == 0) {
				if (Gold - b.GoldCost >= 0) {

					Buildet_Bridges.Add(b);
					if (BridgeWasBuild != null) {
						BridgeWasBuild(this, new PlayerBridgeEventArgs(this, b));
					}

					this.Gold -= b.GoldCost;

					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		public void RemoveTower(Tower t) {
			Buildet_Towers.Remove(t);
		}

		void t_TowerWasDestroyed(object sender, TowerEventArgs e) {
			if (TowerWasDestroyed != null) {
				TowerWasDestroyed(this, e);
			}
		}

		void t_TowerIsUnSelected(object sender, EventArgs e) {
			if (TowerIsUnSelected != null) {
				TowerIsUnSelected(this, new PlayerTowerEventArgs(this, (Tower)sender));
			}
		}

		void t_TowerIsSelected(object sender, EventArgs e) {
			foreach (Tower t in BuildetTowers.Where(x => x.IsSelected)) {
				t.IsSelected = false;
			}

			Tower o = (Tower)sender;
			o.IsSelected = true;

			if (TowerIsSelected != null) {
				TowerIsSelected(this, new PlayerTowerEventArgs(this, o));
			}
		}

		#region Ausgewählte Sachen
		/// <summary>
		/// Immer nur eins von 3 Sachen übergeben.
		/// </summary>
		public void SetStuffToBuild(Tower t, Generator g, Bridge b) {
			if (t != null) {
				towerToBuild = t;
				generatorToBuild = null;
				bridgeToBuild = null;
			} else if (g != null) {
				generatorToBuild = g;
				towerToBuild = null;
				bridgeToBuild = null;
			} else if (b != null) {
				bridgeToBuild = b;
				generatorToBuild = null;
				towerToBuild = null;
			} else {
				bridgeToBuild = null;
				generatorToBuild = null;
				towerToBuild = null;
			}
		}

		private void DrawStuffToBuild(Spritebatch batch) {
			if (towerToBuild != null) {
				towerToBuild.Draw(batch);
			} else if (generatorToBuild != null) {
				generatorToBuild.Draw(batch);
			} else if (bridgeToBuild != null) {
				bridgeToBuild.Draw(batch);
			}
		}

		public void SetNullStuffToBuild() {
			towerToBuild = null;
			generatorToBuild = null;
			bridgeToBuild = null;
		}

		public void SetLocationForStuffToBuild(Vector2 location) {
			if (towerToBuild != null) {
				towerToBuild.Location = location;
			} else if (generatorToBuild != null) {
				
			} else if (bridgeToBuild != null) {
				
			}
		}

		public bool HasStuffToBuild() {
			return towerToBuild != null || generatorToBuild != null || bridgeToBuild != null;
		}

		public T GetCurrentStuff<T>() where T : class {
			if (towerToBuild != null) {
				return (T)((object)towerToBuild);
			} else if (generatorToBuild != null) {
				return (T)((object)generatorToBuild);
			} else if (bridgeToBuild != null) {
				return (T)((object)bridgeToBuild);
			} else {
				return (T)((object)null);
			}
		}

		public bool BuildCurrentStuff<T>() where T : class {
			if (towerToBuild != null && typeof(T) == typeof(Tower)) {
				return AddBuildetTower(towerToBuild);
			} else if (generatorToBuild != null && typeof(T) == typeof(Generator)) {
				return AddAndBuildGenerator(generatorToBuild);
			} else if (bridgeToBuild != null && typeof(T) == typeof(Bridge)) {
				return AddAndBuildBridge(bridgeToBuild);
			} else {
				return false;
			}
		}
		#endregion	
	}

	public class PlayerTowerEventArgs : EventArgs {
		public Player currentPlayer;
		public Tower selectedTower;

		public PlayerTowerEventArgs(Player player, Tower tower) {
			this.currentPlayer = player;
			this.selectedTower = tower;
		}
	}

	public class PlayerGeneratorEventArgs : EventArgs {
		public Player currentPlayer;
		public Generator selectedGenerator;

		public PlayerGeneratorEventArgs(Player player, Generator generator) {
			this.currentPlayer = player;
			this.selectedGenerator = generator;
		}
	}

	public class PlayerBridgeEventArgs : EventArgs {
		public Player currentPlayer;
		public Bridge selectedBridge;

		public PlayerBridgeEventArgs(Player player, Bridge bridge) {
			this.currentPlayer = player;
			this.selectedBridge = bridge;
		}
	}
}
