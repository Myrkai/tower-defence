﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using SideProjekt.ToXNA.Battle;
using TestingTest.Component;
using SharpDX;
using Rectangle = SharpDX.Rectangle;
using Color = SharpDX.Color;
using TestingTest.Interfaces;
using TestingTest.ToXNA;

namespace SideProjekt.ToXNA {

	/// <summary>
	/// die geschossene Kugel.
	/// Diese verteilt den Schaden auf das Monster und überträgt auch den Effekt aufs Monster.
	/// </summary>
	public class Bullet {
		public string Name { get; private set; }
		public string EffektType { get; private set; }
		public string GraphicAsset { get; private set; }
		public Texture2D Graphic { get; set; }
		public float FlightSpeed { get; private set; }
		private Vector2 TargetLocation { get; set; }
		private Vector2 Location { get; set; }
		private Vector2 CurrentLocation { get; set; }
		private double Winkel { get; set; }
		private Tower CurrentTower { get; set; }
		public float Damage { get; private set; }
		public float AreaWidth { get; set; }
		public Rectangle Rectangle { get { return new Rectangle((int)CurrentLocation.X, (int)CurrentLocation.Y, Graphic.Width, Graphic.Height); } }
		public float Time { get; private set; }
		public Element Element { get; private set; }
		public AttackType Attack { get; private set; }
		public IEffekt Effekt { get; private set; }
		public bool HasDirektDamage { get; private set; }
		public float DPS { get { return 0f; } }
		private Monster Monster { get; set; }

		private static bool HomingBullet = true;
		private static bool BulletFlightEndless = false;

		public Bullet(string name, string effektType, string graphicAsset, float flightSpeed, Vector2 target, Vector2 location, Tower currentTower, float damageAmount, float areaWidth, float dotTime, Element ele, AttackType atk, bool hasDirektDamage, Monster monster) {
			this.Name = name;
			this.EffektType = effektType;
			this.GraphicAsset = graphicAsset;
			this.FlightSpeed = flightSpeed;
			this.TargetLocation = target;
			this.Location = location;
			this.Winkel = MathHelper.GetRotation(target, location);
			this.CurrentTower = currentTower;
			this.Damage = damageAmount;
			this.AreaWidth = areaWidth;
			this.Time = dotTime;
			this.Element = ele;
			this.Attack = atk;
			this.HasDirektDamage = hasDirektDamage;
			this.Monster = monster;

			this.CurrentLocation = location;

			this.Graphic = Content.Load<Texture2D>(@"Graphics\Bullet\" + this.GraphicAsset);
		}

		public Bullet(string name, string effektType, string graphicAsset, float flightSpeed, float damageAmount, float areaWidth, float dotTime, bool hasDirektDamage) {
			this.Name = name;
			this.EffektType = effektType;
			this.GraphicAsset = graphicAsset;
			this.FlightSpeed = flightSpeed;
			this.Damage = damageAmount;
			this.AreaWidth = areaWidth;
			this.TargetLocation = Vector2.Zero;
			this.Location = Vector2.Zero;
			this.Time = dotTime;
			this.HasDirektDamage = hasDirektDamage;

			this.CurrentLocation = Vector2.Zero;

			this.Graphic = Content.Load<Texture2D>(@"Graphics\Bullet\" + this.GraphicAsset);
		}

		public void AddEffekt(IEffekt effekt) {
			Effekt = effekt;
		}

		/// <summary>
		/// clont kein effekt
		/// </summary>
		/// <returns></returns>
		internal Bullet Clone() {
			Bullet b = new Bullet(this.Name, this.EffektType, this.GraphicAsset, this.FlightSpeed, new Vector2(TargetLocation.X, TargetLocation.Y), new Vector2(Location.X, Location.Y), CurrentTower, this.Damage, this.AreaWidth, this.Time, this.Element, this.Attack, this.HasDirektDamage, this.Monster);

			return b;
		}

		/// <summary>
		/// clont kein effekt
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		internal Bullet Clone(Tower t) {
			Bullet b = new Bullet(this.Name, this.EffektType, this.GraphicAsset, this.FlightSpeed, new Vector2(TargetLocation.X, TargetLocation.Y), new Vector2(Location.X, Location.Y), t, this.Damage, this.AreaWidth, this.Time, this.Element, this.Attack, this.HasDirektDamage, this.Monster);

			return b;
		}

		public void Draw(Spritebatch batch) {
			Vector2 l = CurrentLocation;
			l -= new Vector2(Graphic.Width / 2, Graphic.Height / 2);
			batch.Draw(Graphic, new Rectangle(0, 0, Graphic.Width, Graphic.Height), new Rectangle(Convert.ToInt32(l.X), Convert.ToInt32(l.Y), Graphic.Width, Graphic.Height), Color.White);
		}

		public void Update(GameTime time) {
			//zielsuchender Schuß
			if (HomingBullet) {
				//Vector2 diff = Monster.CenterLocation - CurrentLocation; //richtungsvector bestimmen
				//float divisor = Math.Max(Math.Abs(diff.X), Math.Abs(diff.Y)); //größten teiler finden
				//diff = new Vector2(diff.X / divisor * FlightSpeed * (float)time.TimeDiff.TotalSeconds, diff.Y / divisor * FlightSpeed * (float)time.TimeDiff.TotalSeconds); // geschoss verschieben auf den richtungsvector
				//CurrentLocation += diff;

				CurrentLocation += Vector2.Normalize(Monster.CenterLocation - CurrentLocation) * FlightSpeed * (float)time.TimeDiff.TotalSeconds;
			} else {
				//gerade schußrichtung
				Location = new Vector2(Location.X, Location.Y - (int)(this.FlightSpeed * time.TimeDiff.TotalSeconds));
				CurrentLocation = MathHelper.RotateAPoint(Location, Winkel, CurrentTower.CenterLocation);
			}

			foreach (Monster m in CurrentTower.currentWave.MonsterOnField) {
				if (m.Rectangle.Intersects(this.Rectangle)) {
					if (HasDirektDamage) {
						m.TakeDamage((int)this.Damage, CurrentTower.Owner, Element, Attack);
					}
					m.AddEffekt(this.Effekt, CurrentTower.Owner);
					CurrentTower.RemoveBullet(this);
					return;
				}
			}

			if (Vector2.Distance(CurrentLocation, CurrentTower.CenterLocation) > Vector2.Distance(TargetLocation, CurrentTower.CenterLocation) + 10 && !BulletFlightEndless || Monster == null) {
				CurrentTower.RemoveBullet(this);
			}
		}
	}
}
