﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using SideProjekt.TileEngine;
using TestingTest.Component;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Eine Welle von Monster innerhalb eines Levels. Jeder Wave kann verschiedene Monstertypen besitzen, die aber alle gleichzeitig gesendet werden.
	/// </summary>
	public class Wave {
		public float monsterSendSpeed { get; private set; } //geschwindigkeit für den Interval des Monster sendens
		float monsterSendTimer; //timer um den Interval wie schnell Monster gesenet werden zu berechnen
		int currentMonsterCount; //wieviele Monster sind aktuell gesenet worden;
		public List<Tuple<Monster, int, int>> MonsterToSend { get; private set; } //MonsterType, Anzahl, PathIndex
		public List<Monster> MonsterOnField { get; private set; } //hier werden sämtliche Monster gespeichert die sich gerade auf der Map befinden
		private List<Monster> MonsterToRemove { get; set; } //hier werden sämtliche Monster gespeichert die entfernt werden sollen
		private bool AllMonsterSendet; //wurden alle Monster in der Wave gesenet?
		private TileMap currentMap { get; set; }
		public string Name { get; private set; }

		public int GoldBonus { get; private set; } //jeder Turm kostet Gold
		public int EnergieBonus { get; private set; } //elektische Türme verbrauchen Storm pro Schuß
		public int LivesBonus { get; private set; } //Anzahl der Leben die ein Spiele hat bis er stirbt (z.B. wenn der Monster einfach durchlässt);

		public event EventHandler<WaveEventArgs> AllMonsterDead;
		public event EventHandler<MonsterArgs> MonsterHasReachedTheEnd;
		public event EventHandler<MonsterArgs> MonsterWasKilled;
		public event EventHandler<MonsterArgs> MonsterWasSendet;

		#region Konstruktor
		public Wave(string name) {
			MonsterToSend = new List<Tuple<Monster, int, int>>();
			MonsterOnField = new List<Monster>();
			MonsterToRemove = new List<Monster>();
			currentMonsterCount = 0;
			monsterSendSpeed = 2f;
			monsterSendTimer = 0f;
			AllMonsterSendet = false;
			this.Name = name;

			SetWaveBonus(0, 0, 0);
		}

		public Wave(string name, float sendSpeed) {
			MonsterToSend = new List<Tuple<Monster, int, int>>();
			MonsterOnField = new List<Monster>();
			MonsterToRemove = new List<Monster>();
			currentMonsterCount = 0;
			monsterSendSpeed = sendSpeed;
			monsterSendTimer = 0f;
			AllMonsterSendet = false;
			this.Name = name;

			SetWaveBonus(0, 0, 0);
		}

		public void SetWaveBonus(int gold, int energie, int lives) {
			this.GoldBonus = gold;
			this.EnergieBonus = energie;
			this.LivesBonus = lives;
		}
		#endregion

		#region xna
		public void Draw(Spritebatch batch) {
			foreach (Monster m in MonsterOnField) {
				m.Draw(batch);
			}
		}

		public void Update(GameTime time) {
			if (AllMonsterSendet == false) {
				monsterSendTimer += (float)time.TimeDiff.TotalSeconds;
				if (monsterSendTimer > monsterSendSpeed) {
					monsterSendTimer = 0;
					SendMonster();
				}
			}
			foreach (Monster m in MonsterOnField) {
				m.Update(time);
			}

			bool waveIsCompled = false;
			foreach (Monster M in MonsterToRemove) {
				MonsterOnField.Remove(M);
			}
			MonsterToRemove.Clear();

			if (AllMonsterSendet && MonsterOnField.Count() == 0) {
				waveIsCompled = true;
			}

			if (waveIsCompled) {
				MonsterOnField.Clear();
				MonsterToSend.Clear();
				MonsterToRemove.Clear();
				if (AllMonsterDead != null) {
					AllMonsterDead(this, new WaveEventArgs(this, this.GoldBonus, this.EnergieBonus, this.LivesBonus));
				}
			}
		}
		#endregion

		private void SendMonster() {
			currentMonsterCount += 1;
			bool haveSendAny = false;
			foreach (Tuple<Monster, int, int> send in MonsterToSend) {
				//z.B: MonsterCount == 1, dann wird das erste Monster gesendet, nicht Index 1!!!
				if (send.Item2 >= currentMonsterCount) {
					Monster m = (Monster)send.Item1.Clone(this, currentMap.Paths[send.Item3]);
					m.MonsterHasReachedTheEndOfPath += new EventHandler<MonsterArgs>(m_MonsterHasReachedTheEndOfPath);
					m.MonsterWasKilled += new EventHandler<MonsterArgs>(m_MonsterWasKilled);
					MonsterOnField.Add(m);
					if (MonsterWasSendet != null) {
						MonsterWasSendet(this, new MonsterArgs(m, 0, 0, 0, null));
					}
					haveSendAny = true;
				}
			}

			//es wurden anscheint alle Monster gesendet
			if (haveSendAny == false) {
				AllMonsterSendet = true;
			}
		}

		void m_MonsterWasKilled(object sender, MonsterArgs e) {
			if (MonsterWasKilled != null) {
				MonsterWasKilled(this, e);
			}
		}

		void m_MonsterHasReachedTheEndOfPath(object sender, MonsterArgs e) {
			RemoveMonster(e.monster);
			if (MonsterHasReachedTheEnd != null) {
				MonsterHasReachedTheEnd(this, e);
			}
		}

		public void SetMap(TileMap map) {
			currentMap = map;
		}

		public void RemoveMonster(Monster M) {
			MonsterToRemove.Add(M);
		}
	}

	public class WaveEventArgs : EventArgs {
		public Wave CurrentWave { get; private set; }

		public int GoldBonus { get; private set; } //jeder Turm kostet Gold
		public int EnergieBonus { get; private set; } //elektische Türme verbrauchen Storm pro Schuß
		public int LivesBonus { get; private set; } //Anzahl der Leben die ein Spiele hat bis er stirbt (z.B. wenn der Monster einfach durchlässt);

		public WaveEventArgs(Wave wave, int gold, int energie, int lives) {
			this.CurrentWave = wave;

			this.GoldBonus = gold;
			this.EnergieBonus = energie;
			this.LivesBonus = lives;
		}
	}
}
