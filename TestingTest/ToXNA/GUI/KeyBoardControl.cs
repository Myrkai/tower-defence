﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.GUI {
	/// <summary>
	/// Stellt eine komplette Viruelle Tastatur zur Verfügung.
	/// </summary>
	public class KeyBoardControl : IGUIElement {
		private static string firstLineKeys = "1234567890ß´";
		private static string secondLineKeys = "qwertzuiopü+";
		private static string thridLineKeys = "asdfghjklöä#";
		private static string fourdLineKeys = "<>yxcvbnm,.-";
		private static int SingleKeysHight = 32;
		private static int SingleKeysWidth = 32;

		private bool shiftIsPressed = false;
		private bool capsLockIsPressed = false;
		private bool HasUpperLetters = false;

		private ImageControl Background { get; set; }
		public Vector2 Location { get; private set; }
		private Dictionary<string, List<ButtonControl>> Keys { get; set; } //reihenposition, liste der elemente;
		public event EventHandler<ButtonEventArgs> KeyPressed;

		private Color keyBackground;
		private Color keyBoarder;
		private int spaceBetweenKeys;
		private int boarderWidth;
		private float fontHeight;
		private Color fontColor;
		private Color shiftCapsClicked;

		public KeyBoardControl(Vector2 location, int spaceBetweenKeys, int boarderWidth, Color backgroundColor, Color boarderColor, Color keyBackground, Color keyBoarder, Color fontColor, float fontHeight, Color shiftCapsClicked) {
			this.keyBackground = keyBackground;
			this.keyBoarder = keyBoarder;
			this.spaceBetweenKeys = spaceBetweenKeys;
			this.boarderWidth = boarderWidth;
			this.fontHeight = fontHeight;
			this.fontColor = fontColor;
			this.shiftCapsClicked = shiftCapsClicked;
			
			Keys = new Dictionary<string, List<ButtonControl>>();
			Location = location;
			int MaxItemCount = 13;
			Background = new ImageControl(Extensions.CreateTextureWidthBoarder(SingleKeysWidth * MaxItemCount + boarderWidth * 2 + spaceBetweenKeys * (MaxItemCount - 1), SingleKeysHight * 5 + boarderWidth * 2 + 3 * spaceBetweenKeys, backgroundColor, 1, boarderColor), Location);

			Texture2D buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth, SingleKeysHight, keyBackground, 1, keyBoarder);
			int currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1), currentY = boarderWidth;
			List<ButtonControl> l;
			Keys.Add("Line1", new List<ButtonControl>());
			Keys.TryGetValue("Line1", out l);
			foreach (char s in firstLineKeys.ToArray()) {
				ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
				btn.Comment = s.ToString();
				btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				l.Add(btn);
				currentX += spaceBetweenKeys + SingleKeysWidth;
			}
			//insert special key backspace
			ButtonControl btn2 = new ButtonControl("◄", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
			btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
			btn2.Comment = "special backspace";
			l.Add(btn2);
			Keys["line1"] = l;

			Keys.Add("Line2", new List<ButtonControl>());
			Keys.TryGetValue("Line2", out l);
			currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
			currentY += spaceBetweenKeys + SingleKeysHight;
			//insert special key caps
			btn2 = new ButtonControl("▼", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
			btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
			btn2.Comment = "special caps";
			l.Add(btn2);
			currentX += spaceBetweenKeys + SingleKeysWidth;
			foreach (char s in secondLineKeys.ToArray()) {
				ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
				btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn.Comment = s.ToString();
				l.Add(btn);
				currentX += spaceBetweenKeys + SingleKeysWidth;
			}
			Keys["line2"] = l;

			Keys.Add("Line3", new List<ButtonControl>());
			Keys.TryGetValue("Line3", out l);
			currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
			currentY += spaceBetweenKeys + SingleKeysHight;
			//insert special key caps
			btn2 = new ButtonControl("▲", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
			btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
			btn2.Comment = "special shift";
			l.Add(btn2);
			currentX += spaceBetweenKeys + SingleKeysWidth;
			foreach (char s in thridLineKeys.ToArray()) {
				ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
				btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn.Comment = s.ToString();
				l.Add(btn);
				currentX += spaceBetweenKeys + SingleKeysWidth;
			}
			Keys["line3"] = l;

			Keys.Add("Line4", new List<ButtonControl>());
			Keys.TryGetValue("Line4", out l);
			currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
			currentY += spaceBetweenKeys + SingleKeysHight;
			foreach (char s in fourdLineKeys.ToArray()) {
				ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
				btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn.Comment = s.ToString();
				l.Add(btn);
				currentX += spaceBetweenKeys + SingleKeysWidth;
			}
			//insert special key enter
			btn2 = new ButtonControl("OK", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
			btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
			btn2.Comment = "special enter";
			l.Add(btn2);
			Keys["line4"] = l;

			//special für space
			Keys.Add("Line5", new List<ButtonControl>());
			Keys.TryGetValue("Line5", out l);
			currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, -7);
			currentY += spaceBetweenKeys + SingleKeysHight;
			//insert special key space
			buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth * 5 + spaceBetweenKeys * 4, SingleKeysHight, keyBackground, 1, keyBoarder);
			btn2 = new ButtonControl("Space", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
			btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
			btn2.Comment = "special Space";
			l.Add(btn2);
		}

		void btn_ButtonClickEvent(object sender, ButtonEventArgs e) {
			if (e.Comment == "special shift") {
				shiftIsPressed = shiftIsPressed ? false : true;
				bool last = HasUpperLetters;
				HasUpperLetters = !shiftIsPressed && capsLockIsPressed || shiftIsPressed && !capsLockIsPressed ? true : false;
				if (last != HasUpperLetters)
					RecreateButtons();
			} else if (e.Comment == "special caps") {
				capsLockIsPressed = capsLockIsPressed ? false : true;
				bool last = HasUpperLetters;
				HasUpperLetters = !shiftIsPressed && capsLockIsPressed || shiftIsPressed && !capsLockIsPressed ? true : false;
				if (last != HasUpperLetters)
					RecreateButtons();
			} else {
				if (KeyPressed != null) {
					if (HasUpperLetters)
						KeyPressed(this, new ButtonEventArgs(e.ButtonName.ToUpper(), e.Button, e.Comment));
					else
						KeyPressed(this, e);

					shiftIsPressed = false;
					bool last = HasUpperLetters;
					HasUpperLetters = !shiftIsPressed && capsLockIsPressed || shiftIsPressed && !capsLockIsPressed ? true : false;
					if (last != HasUpperLetters)
						RecreateButtons();
				}
			}
		}

		private void RecreateButtons() {
			Keys.Clear();
			Keys = new Dictionary<string, List<ButtonControl>>();
			if (HasUpperLetters) {
				Texture2D buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth, SingleKeysHight, keyBackground, 1, keyBoarder);
				int currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1), currentY = boarderWidth;
				List<ButtonControl> l;
				Keys.Add("Line1", new List<ButtonControl>());
				Keys.TryGetValue("Line1", out l);
				foreach (char s in firstLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString().ToUpper(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString().ToUpper());
					btn.Comment = s.ToString();
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				//insert special key backspace
				ButtonControl btn2 = new ButtonControl("◄", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special backspace";
				l.Add(btn2);
				Keys["line1"] = l;

				Keys.Add("Line2", new List<ButtonControl>());
				Keys.TryGetValue("Line2", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key caps
				btn2 = new ButtonControl("▼", new Vector2(currentX, currentY), fontHeight, capsLockIsPressed ? shiftCapsClicked : fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special caps";
				l.Add(btn2);
				currentX += spaceBetweenKeys + SingleKeysWidth;
				foreach (char s in secondLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString().ToUpper(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString().ToUpper());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				Keys["line2"] = l;

				Keys.Add("Line3", new List<ButtonControl>());
				Keys.TryGetValue("Line3", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key caps
				btn2 = new ButtonControl("▲", new Vector2(currentX, currentY), fontHeight, shiftIsPressed ? shiftCapsClicked : fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special shift";
				l.Add(btn2);
				currentX += spaceBetweenKeys + SingleKeysWidth;
				foreach (char s in thridLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString().ToUpper(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString().ToUpper());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				Keys["line3"] = l;

				Keys.Add("Line4", new List<ButtonControl>());
				Keys.TryGetValue("Line4", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				foreach (char s in fourdLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString().ToUpper(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString().ToUpper());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				//insert special key enter
				btn2 = new ButtonControl("OK", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special enter";
				l.Add(btn2);
				Keys["line4"] = l;

				//special für space
				Keys.Add("Line5", new List<ButtonControl>());
				Keys.TryGetValue("Line5", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, -7);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key space
				buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth * 5 + spaceBetweenKeys * 4, SingleKeysHight, keyBackground, 1, keyBoarder);
				btn2 = new ButtonControl("Space", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special Space";
				l.Add(btn2);
			} else {
				Texture2D buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth, SingleKeysHight, keyBackground, 1, keyBoarder);
				int currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1), currentY = boarderWidth;
				List<ButtonControl> l;
				Keys.Add("Line1", new List<ButtonControl>());
				Keys.TryGetValue("Line1", out l);
				foreach (char s in firstLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
					btn.Comment = s.ToString();
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				//insert special key backspace
				ButtonControl btn2 = new ButtonControl("◄", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special backspace";
				l.Add(btn2);
				Keys["line1"] = l;

				Keys.Add("Line2", new List<ButtonControl>());
				Keys.TryGetValue("Line2", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key caps
				btn2 = new ButtonControl("▼", new Vector2(currentX, currentY), fontHeight, capsLockIsPressed ? shiftCapsClicked : fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special caps";
				l.Add(btn2);
				currentX += spaceBetweenKeys + SingleKeysWidth;
				foreach (char s in secondLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				Keys["line2"] = l;

				Keys.Add("Line3", new List<ButtonControl>());
				Keys.TryGetValue("Line3", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key caps
				btn2 = new ButtonControl("▲", new Vector2(currentX, currentY), fontHeight, shiftIsPressed ? shiftCapsClicked : fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special shift";
				l.Add(btn2);
				currentX += spaceBetweenKeys + SingleKeysWidth;
				foreach (char s in thridLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				Keys["line3"] = l;

				Keys.Add("Line4", new List<ButtonControl>());
				Keys.TryGetValue("Line4", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, 1);
				currentY += spaceBetweenKeys + SingleKeysHight;
				foreach (char s in fourdLineKeys.ToArray()) {
					ButtonControl btn = new ButtonControl(s.ToString(), new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, s.ToString());
					btn.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
					btn.Comment = s.ToString();
					l.Add(btn);
					currentX += spaceBetweenKeys + SingleKeysWidth;
				}
				//insert special key enter
				btn2 = new ButtonControl("OK", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special enter";
				l.Add(btn2);
				Keys["line4"] = l;

				//special für space
				Keys.Add("Line5", new List<ButtonControl>());
				Keys.TryGetValue("Line5", out l);
				currentX = GetCurrentX(spaceBetweenKeys, firstLineKeys, -7);
				currentY += spaceBetweenKeys + SingleKeysHight;
				//insert special key space
				buttonTexture = Extensions.CreateTextureWidthBoarder(SingleKeysWidth * 5 + spaceBetweenKeys * 4, SingleKeysHight, keyBackground, 1, keyBoarder);
				btn2 = new ButtonControl("Space", new Vector2(currentX, currentY), fontHeight, fontColor, buttonTexture, true, "");
				btn2.ButtonClickEvent += new EventHandler<ButtonEventArgs>(btn_ButtonClickEvent);
				btn2.Comment = "special Space";
				l.Add(btn2);
			}
		}

		private int GetCurrentX(int spaceBetweenKeys, string line) {
			return (Width() - ((line.ToArray().Count() * SingleKeysWidth) + ((line.ToArray().Count() - 1) * spaceBetweenKeys))) / 2;
		}

		private int GetCurrentX(int spaceBetweenKeys, string line, int value) {
			return (Width() - (((line.ToArray().Count() + value) * SingleKeysWidth) + (((line.ToArray().Count() - 1 + value)) * spaceBetweenKeys))) / 2;
		}

		#region Interface
		public int Width() {
			return Background.Width();
		}

		public int Height() {
			return Background.Height();
		}

		public void PerformClick(Vector2 position) {
			if (enable) {
				Vector2 newVetor = position - Location;

				var a = Keys.SelectMany(x => x.Value).Where(x => x.GetPosition().Intersects(new Rectangle((int)newVetor.X, (int)newVetor.Y, 1, 1)) && x.IsClickable() && x.IsEnable()).FirstOrDefault();
				if (a != null)
					a.PerformClick(newVetor);
			}
		}

		public Rectangle GetPosition() {
			return new Rectangle((int)Location.X, (int)Location.Y, Width(), Height());
		}

		bool enable = true;
		public bool IsEnable() {
			return enable;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) { this.enable = enable; }

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			if (enable) {
				Background.DrawToPosition(batch, position);
				foreach (List<ButtonControl> l in Keys.Values) {
					foreach (ButtonControl b in l) {
						Vector2 offset = position - Location;
						b.DrawToPosition(batch, new Vector2(Location.X + b.GetPosition().X, Location.Y + b.GetPosition().Y) + offset);
					}
				}
			}
		}

		public void Update(GameTime gameTime) {
			if (enable) {

			}
		}

		public void Draw(Spritebatch batch) {
			if (enable) {
				Background.Draw(batch);
				foreach (List<ButtonControl> l in Keys.Values) {
					foreach (ButtonControl b in l) {
						b.DrawToPosition(batch, new Vector2(Location.X + b.GetPosition().X, Location.Y + b.GetPosition().Y));
					}
				}
			}
		}

		public void Dispose() {
			Keys.Clear();
		}
		#endregion
	}
}
