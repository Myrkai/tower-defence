﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SideProjekt.Interfaces;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.GUI {
	/// <summary>
	/// Ist der Knopf halt
	/// </summary>
	public class ButtonControl : IGUIElement {

		private LinkControl textControl { get; set; }
		private ImageControl imageControl { get; set; }
		private Vector2 Position { get; set; }
		public string Name { get; private set; }
		public event EventHandler<ButtonEventArgs> ButtonClickEvent;
		private Vector2 offSetPositionImage;
		private Vector2 offSetPositionText;
		/// <summary>
		/// feel free to write
		/// </summary>
		public string Comment { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text">Text der aus dem Button erscheint</param>
		/// <param name="position">Position des Buttons (linke obere ecke)</param>
		/// <param name="fontsize">Schriftgröße des Textes</param>
		/// <param name="fontColor">Schriftfarbe des Textes</param>
		/// <param name="texture">Hintegrundbild des Buttons</param>
		/// <param name="centerTextToImage">[True] = Text wird zum Bild ausgerichtet (bild = position). [false] = Bild wird zum Text ausgerichtet (Text = position).</param>
		public ButtonControl(string text, Vector2 position, float fontsize, Color fontColor, Texture2D texture, bool centerTextToImage, string name) {
			if (centerTextToImage) {
				imageControl = new ImageControl(texture, position);
				int textWidth = Spritebatch.GetTextDimension(text, new SpriteFont(fontsize, fontColor).Font).Width;
				int textHeight = Spritebatch.GetTextDimension(text, new SpriteFont(fontsize, fontColor).Font).Height;
				textControl = new LinkControl(text, new Vector2(position.X - imageControl.Width() / 2 - textWidth / 2, position.Y - imageControl.Height() / 2 - textHeight / 2), new SpriteFont(fontsize, fontColor));

				offSetPositionImage = Vector2.Zero;
				offSetPositionText = new Vector2(imageControl.Width() / 2 - textWidth / 2, imageControl.Height() / 2 - textHeight / 2);
			} else {
				textControl = new LinkControl(text, position, new SpriteFont(fontsize, fontColor));
				imageControl = new ImageControl(texture, new Vector2(0, 0));
			}
			
			Position = position;
			Name = name;
		}

		#region Interface
		public int Width() {
			return textControl.Width() > imageControl.Width() ? textControl.Width() : imageControl.Width();
		}

		public int Height() {
			return textControl.Height() > imageControl.Height() ? textControl.Height() : imageControl.Height(); ;
		}

		public void PerformClick(Vector2 position) {
			if (ButtonClickEvent != null) {
				ButtonClickEvent(this, new ButtonEventArgs(this.Name, this, this.Comment));
			}
		}

		public Rectangle GetPosition() {
			return new Rectangle((int)Position.X, (int)Position.Y, Width(), Height());
		}

		public bool IsEnable() {
			return isEnable;
		}

		public bool IsClickable() {
			return true;
		}

		bool isEnable = true;
		public void SetIsEnable(bool enable) { isEnable = enable; }

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			if (isEnable) {
				imageControl.DrawToPosition(batch, position + offSetPositionImage);
				textControl.DrawToPosition(batch, position + offSetPositionText);
			}
		}

		public void Update(GameTime gameTime) {
			if (isEnable) { 
			
			}
		}

		public void Draw(Spritebatch batch) {
			if (isEnable) {
				imageControl.Draw(batch);
				textControl.Draw(batch);
			}
		}
		#endregion

	}

	public class ButtonEventArgs : EventArgs {
		public string ButtonName;
		public ButtonControl Button;
		public string Comment;

		public ButtonEventArgs(string name, ButtonControl button, string comment) {
			this.ButtonName = name;
			this.Button = button;
			this.Comment = comment;
		}
	}
}
