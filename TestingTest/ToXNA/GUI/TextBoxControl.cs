﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.GUI {
	public enum Alignment { 
		Left,
		Right,
		Center
	}
	
	/// <summary>
	/// Eine Textbox.
	/// </summary>
	public class TextBoxControl : IGUIElement {

		private LinkControl textControl;
		private ImageControl background;
		private Vector2 location;
		public event EventHandler TextBoxWasSelected;
		public event EventHandler TextBoxWasUnSelected;
		private bool isSelected = false;
		private float textSize = 5f;
		private Alignment alignment = Alignment.Center;
		public string Text {
			get {
				return textControl.Text;
			}

			set {
				int textWidth = Spritebatch.GetTextDimension(value, new SpriteFont(textSize, textControl.FontColor)).Width;
				int textHeight = Spritebatch.GetTextDimension(value, new SpriteFont(textSize, textControl.FontColor)).Height;

				if (Alignment.Center == alignment) {
					textControl = new LinkControl(value, new Vector2(location.X + (background.Width() / 2 - textWidth / 2), location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, textControl.FontColor));
				} else if (Alignment.Left == alignment) {
					textControl = new LinkControl(value, new Vector2(location.X + 3, location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, textControl.FontColor));
				} else if (Alignment.Right == alignment) {
					textControl = new LinkControl(value, new Vector2(location.X + Width() - 3 - textWidth, location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, textControl.FontColor));
				}
			}
		}

		public TextBoxControl(string text, Vector2 position, Color fontColor, Color backgroundColor, Color boarderColor, int width, int height, Alignment align) {
			this.location = position;
			alignment = align;
			background = new ImageControl(Extensions.CreateTextureWidthBoarder(width, height, backgroundColor, 1, boarderColor), position);
			int textWidth = Spritebatch.GetTextDimension(text, new SpriteFont(textSize, fontColor).Font).Width;
			int textHeight = Spritebatch.GetTextDimension(text, new SpriteFont(textSize, fontColor).Font).Height;

			//jaja ist blöd ich weiß...
			//aber ich will die Textgröße ich bei jeder zuweisung neu berechnen müssen (Text())
			bool noName = false;
			if (text == "") {
				text = "Testg'´g˚yp";
				noName = true;
			}

			while (textHeight < background.Height() - 10) {
				textSize += 0.1f;
				textHeight = Spritebatch.GetTextDimension(text, new SpriteFont(textSize, fontColor).Font).Height;
				textWidth = Spritebatch.GetTextDimension(text, new SpriteFont(textSize, fontColor).Font).Width;
			}

			if (noName) {
				text = "";
			}

			if (Alignment.Center == align) {
				textControl = new LinkControl(text, new Vector2(location.X + (background.Width() / 2 - textWidth / 2), location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, fontColor));
			} else if (Alignment.Left == align) {
				textControl = new LinkControl(text, new Vector2(location.X + 3, location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, fontColor));
			} else if (Alignment.Right == align) {
				textControl = new LinkControl(text, new Vector2(location.X + Width() - 3 - textWidth, location.Y + (background.Height() / 2 - textHeight / 2)), new SpriteFont(textSize, fontColor));
			}
		}

		#region Interface
		public int Width() {
			return background.Width();
		}

		public int Height() {
			return background.Height();
		}

		public void PerformClick(Vector2 position) {
			if (isSelected) {
				isSelected = false;
				if (TextBoxWasUnSelected != null) {
					TextBoxWasUnSelected(this, null);
				}
			} else {
				isSelected = true;
				if (TextBoxWasSelected != null) {
					TextBoxWasSelected(this, null);
				}
			}
		}

		public Rectangle GetPosition() {
			return new Rectangle((int)location.X, (int)location.Y, Width(), Height());
		}

		bool enable = true;
		public bool IsEnable() {
			return enable;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) { this.enable = enable; }	

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			Vector2 offset = new Vector2(this.textControl.GetPosition().X - this.location.X, this.textControl.GetPosition().Y - this.location.Y);
			background.DrawToPosition(batch, position);
			textControl.DrawToPosition(batch, position + offset);
		}

		public void Update(GameTime gameTime) {
			if (enable) { 
			
			}
		}

		public void Draw(Spritebatch batch) {
			if (enable) {
				background.Draw(batch);
				textControl.Draw(batch);
			}
		}
		#endregion

	}
}
