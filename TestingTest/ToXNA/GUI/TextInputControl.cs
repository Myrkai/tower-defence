﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.GUI {

	/// <summary>
	/// Textbox mit Tastatureingabe.
	/// </summary>
	public class TextInputControl : IGUIElement {

		private TextBoxControl TextBox { get; set; }
		private KeyBoardControl KeyBoard { get; set; }
		private Vector2 Location { get; set; }
		public string Text {
			get { return TextBox.Text; }
			set { TextBox.Text = value; }
		}
		private bool keyboardIsVisible = false;
		public event EventHandler<ButtonEventArgs> EnterWasPressed;

		public TextInputControl(string text, Vector2 position, Color textBoxFontColor, Color textBoxBackgroundColor, Color textBoxBoarderColor, int textBoxWidth, int textBoxHeight, Alignment textAlign, Vector2 keyBoardLocation, int keyBoardSpaceBetweenKeys, int keyBoardBoarderWidth, Color keyBoardBackgroundColor, Color keyBoardBoarderColor, Color keyBoardKeyBackground, Color keyBoardKeyBoarder, Color keyBoardFontColor, float keyBoardFontHeight, Color keyBoardShiftCapsClicked) {
			Location = position;
			TextBox = new TextBoxControl(text, position, textBoxFontColor, textBoxBackgroundColor, textBoxBoarderColor, textBoxWidth, textBoxHeight, textAlign);
			KeyBoard = new KeyBoardControl(keyBoardLocation, keyBoardSpaceBetweenKeys, keyBoardBoarderWidth, keyBoardBackgroundColor, keyBoardBoarderColor, keyBoardKeyBackground, keyBoardKeyBoarder, keyBoardFontColor, keyBoardFontHeight, keyBoardShiftCapsClicked);
			KeyBoard.SetIsEnable(false);

			TextBox.TextBoxWasSelected += new EventHandler(TextBox_TextBoxWasSelected);
			TextBox.TextBoxWasUnSelected += new EventHandler(TextBox_TextBoxWasUnSelected);
			KeyBoard.KeyPressed += new EventHandler<ButtonEventArgs>(KeyBoard_KeyPressed);
		}

		#region Interface
		public int Width() {
			if (keyboardIsVisible) {
				return Math.Max(TextBox.GetPosition().X + TextBox.Width(), KeyBoard.GetPosition().X + KeyBoard.Width()) - Math.Min(TextBox.GetPosition().X, KeyBoard.GetPosition().X);
			} else {
				return TextBox.Width();
			}
		}

		public int Height() {
			if (keyboardIsVisible) {
				return Math.Max(TextBox.GetPosition().Y + TextBox.Height(), KeyBoard.GetPosition().Y + KeyBoard.Height()) - Math.Min(TextBox.GetPosition().Y, KeyBoard.GetPosition().Y);
			} else {
				return TextBox.Height();
			}
		}

		public void PerformClick(Vector2 position) {
			if (TextBox.GetPosition().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))) {
				TextBox.PerformClick(position);
			}

			if (KeyBoard.GetPosition().Intersects(new Rectangle((int)position.X, (int)position.Y, 1, 1))) {
				KeyBoard.PerformClick(position);
			}
		}

		public Rectangle GetPosition() {
			if (keyboardIsVisible) {
				return new Rectangle((int)Math.Min(TextBox.GetPosition().X, KeyBoard.GetPosition().X), (int)Math.Min(TextBox.GetPosition().Y, KeyBoard.GetPosition().Y), Width(), Height());
			} else {
				return TextBox.GetPosition();
			}
		}

		bool enable = true;
		public bool IsEnable() {
			return enable;
		}

		public bool IsClickable() {
			return true;
		}

		public void SetIsEnable(bool enable) { this.enable = enable; }

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			TextBox.DrawToPosition(batch, position);
			Vector2 offset = position - Location;
			KeyBoard.DrawToPosition(batch, new Vector2(KeyBoard.GetPosition().X, KeyBoard.GetPosition().Y) + offset);
		}

		public void Update(GameTime gameTime) {
			if (enable) {
				TextBox.Update(gameTime);
				KeyBoard.Update(gameTime);
			}
		}

		public void Draw(Spritebatch batch) {
			if (enable) {
				TextBox.Draw(batch);
				KeyBoard.Draw(batch);
			}
		}

		public void Dispose() {
			KeyBoard.Dispose();
		}
		#endregion

		#region Events
		void KeyBoard_KeyPressed(object sender, ButtonEventArgs e) {
			if (e.Comment == "special backspace") {
				if (TextBox.Text.Length > 0) {
					TextBox.Text = TextBox.Text.Substring(0, TextBox.Text.Length - 1);
				}
			} else if (e.Comment == "special Space") {
				TextBox.Text += " ";
			} else if (e.Comment == "special enter") {
				if (EnterWasPressed != null) {
					EnterWasPressed(this, new ButtonEventArgs(e.ButtonName, e.Button, e.Comment));
				}
			} else {
				TextBox.Text += e.ButtonName;
			}
		}

		void TextBox_TextBoxWasUnSelected(object sender, EventArgs e) {
			KeyBoard.SetIsEnable(false);
			keyboardIsVisible = false;
		}

		void TextBox_TextBoxWasSelected(object sender, EventArgs e) {
			KeyBoard.SetIsEnable(true);
			keyboardIsVisible = true;
		}
		#endregion
	}
}
