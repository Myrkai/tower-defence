﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Rectangle = SharpDX.Rectangle;
using RectangleF = SharpDX.RectangleF;
using Color = SharpDX.Color;

namespace SideProjekt.ToXNA.GUI {
	/// <summary>
	/// Zeichnet ein Clíckbares Bild.
	/// </summary>
	public class ImageControl : IGUIElement {

		public Texture2D Texture { get; private set; }
		private Vector2 Position { get; set; }

		public ImageControl(Texture2D texture, Vector2 position) {
			this.Texture = texture;
			this.Position = position;
		}

		#region Interface
		public int Width() {
			return Texture.Width;
		}

		public int Height() {
			return Texture.Height;
		}

		public void PerformClick(Vector2 position) { }

		public Rectangle GetPosition() {
			return new Rectangle((int)Position.X, (int)Position.Y, Width(), Height());
		}

		public RectangleF GetPositionF() {
			return new RectangleF((int)Position.X, (int)Position.Y, Width(), Height());
		}

		public bool IsEnable() {
			return isEnable;
		}

		public bool IsClickable() {
			return false;
		}

		bool isEnable = true;
		public void SetIsEnable(bool enable) { isEnable = enable; }

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			if (isEnable) {
				batch.Draw(Texture, new RectangleF(0, 0, Width(), Height()), new RectangleF((int)position.X, (int)position.Y, Width(), Height()), Color.White);
			}
		}

		public void Update(GameTime gameTime) {
			if(isEnable) {
			
			}
		}

		public void Draw(Spritebatch batch) {
			if (isEnable) {
				batch.Draw(Texture, new RectangleF(0, 0, Width(), Height()), new RectangleF((int)Position.X, (int)Position.Y, Width(), Height()), Color.White);
			}
		}
		#endregion

	}
}
