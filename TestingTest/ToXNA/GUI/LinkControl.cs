﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Point = SharpDX.Point;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.ToXNA.GUI {
	/// <summary>
	/// Ein clickbarer Text.
	/// </summary>
	public class LinkControl : IGUIElement {

		public string Text { get; private set; }
		private Vector2 Position { get; set; }
		private SpriteFont Font { get; set; }
		public SharpDX.Color FontColor { get { return Font.Color; } }

		public LinkControl(string text, Vector2 position, SpriteFont font) {
			this.Text = text;
			this.Position = position;
			this.Font = font;
		}

		#region Interface
		public int Width() {
			return Spritebatch.GetTextDimension(Text, Font).Width;
		}

		public int Height() {
			return Spritebatch.GetTextDimension(Text, Font).Height;
		}

		public void PerformClick(Vector2 position) { }

		public SharpDX.Rectangle GetPosition() {
			return new Rectangle((int)Position.X, (int)Position.Y, Width(), Height());
		}

		public bool IsEnable() {
			return isEnable;
		}

		public bool IsClickable() {
			return false;
		}

		bool isEnable = true;
		public void SetIsEnable(bool enable) { isEnable = enable; }	

		public void DrawToPosition(Spritebatch batch, Vector2 position) {
			if (isEnable) {
				batch.DrawString(Text, new Point((int)position.X, (int)position.Y), Font);
			}
		}

		public void Update(GameTime gameTime) {
			if (isEnable) { 
			
			}
		}

		public void Draw(Spritebatch batch) {
			if (isEnable) {
				batch.DrawString(Text, new Point((int)Position.X, (int)Position.Y), Font);
			}
		}
		#endregion

	}
}
