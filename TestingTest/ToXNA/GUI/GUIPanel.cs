﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Rectangle = SharpDX.Rectangle;
using RectangleF = SharpDX.RectangleF;

namespace SideProjekt.ToXNA {
	public enum BorderType {
		Top,
		Right,
		Left,
		Bottom,
		TopRight,
		TopLeft,
		BottomRight,
		BottomLeft,
		TopLeftRight,
		BottomLeftRight,
		LeftTopBottom,
		RightTopBottom,
		FULL,
		None
	}

	/// <summary>
	/// Ist ein Panel. Auf diesen können beliebig viele GUIElemente hinzugefügt werden und haben damit auch eine Zentrale Zugriffsstelle.
	/// Die Positionen innerhalb dieses Panels sind alle relativ zur Position vom Panel selbst.
	/// </summary>
	public class GUIPanel : IDrawable, IUpdateable, IClickable {

		private Dictionary<string, IGUIElement> Objekts { get; set; }
		public Vector2 Location { get; private set; }
		public int Width { get; private set; }
		public int Height { get; private set; }

		public string BackgroundAsset { get; private set; }
		private Texture2D Background { get; set; }

		private BorderType BorderTyp { get; set; }
		private Color BorderColor { get; set; }
		private bool isEnable;
		private string text;

		private List<Tuple<Texture2D, Rectangle>> Borders { get; set; }

		#region Konstruktor
		public GUIPanel(string t) {
			Objekts = new Dictionary<string, IGUIElement>();
			BorderTyp = BorderType.None;
			Location = Vector2.Zero;
			Width = 10;
			Height = 10;
			SetIsEnable(true);

			Borders = new List<Tuple<Texture2D, Rectangle>>();
			text = t;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="backgroundAsset">@"Graphics\GUI\" + backgroundAsset</param>
		public GUIPanel(string t, string backgroundAsset, Vector2 location, int width, int height) {
			Objekts = new Dictionary<string, IGUIElement>();
			BorderTyp = BorderType.None;

			this.Location = location;
			this.Width = width;
			this.Height = height;

			this.BackgroundAsset = backgroundAsset;
			Background = Content.Load<Texture2D>(@"Graphics\GUI\" + backgroundAsset);

			SetIsEnable(true);

			Borders = new List<Tuple<Texture2D, Rectangle>>();
			text = t;
		}

		public GUIPanel(string t, BorderType typ, Color borderColor, Vector2 location, int width, int height) {
			Objekts = new Dictionary<string, IGUIElement>();
			BorderTyp = typ;
			this.BorderColor = borderColor;

			this.Location = location;
			this.Width = width;
			this.Height = height;

			SetIsEnable(true);

			Borders = new List<Tuple<Texture2D, Rectangle>>();
			CreateBorders();
			text = t;
		}

		private void CreateBorders() {
			if (BorderTyp != BorderType.None) {
				int borderWidth = 3;
				switch (BorderTyp) {
					case BorderType.Top:
						Texture2D textureTop = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						break;
					case BorderType.Left:
						Texture2D textureLeft = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureLeft.Width, textureLeft.Height)));
						break;
					case BorderType.Right:
						Texture2D textureRight = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y, textureRight.Width, textureRight.Height)));
						break;
					case BorderType.Bottom:
						Texture2D textureBottom = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						break;
					case BorderType.TopLeft:
						textureTop = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureLeft = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureLeft.Width, textureLeft.Height)));
						break;
					case BorderType.TopRight:
						textureTop = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X + this.Width - textureTop.Width, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureRight = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y, textureRight.Width, textureRight.Height)));
						break;
					case BorderType.BottomLeft:
						textureBottom = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						textureLeft = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureLeft.Height, textureLeft.Width, textureLeft.Height)));
						break;
					case BorderType.BottomRight:
						textureBottom = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X + this.Width - textureBottom.Width, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						textureRight = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y + this.Height - textureRight.Height, textureRight.Width, textureRight.Height)));
						break;
					case BorderType.TopLeftRight:
						textureTop = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureLeft = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureLeft.Width, textureLeft.Height)));
						textureRight = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y, textureRight.Width, textureRight.Height)));
						break;
					case BorderType.BottomLeftRight:
						textureBottom = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						textureLeft = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureLeft.Height, textureLeft.Width, textureLeft.Height)));
						textureRight = GetBorderTexture(borderWidth, (int)(this.Height * 0.25), BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y + this.Height - textureRight.Height, textureRight.Width, textureRight.Height)));
						break;
					case BorderType.LeftTopBottom:
						textureLeft = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureLeft.Width, textureLeft.Height)));
						textureTop = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureBottom = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						break;
					case BorderType.RightTopBottom:
						textureRight = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y, textureRight.Width, textureRight.Height)));
						textureTop = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X + this.Width - textureTop.Width, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureBottom = GetBorderTexture((int)(this.Width * 0.25), borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X + this.Width - textureBottom.Width, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						break;
					case BorderType.FULL:
						textureTop = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureTop, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureTop.Width, textureTop.Height)));
						textureLeft = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureLeft, new Rectangle((int)this.Location.X, (int)this.Location.Y, textureLeft.Width, textureLeft.Height)));
						textureRight = GetBorderTexture(borderWidth, this.Height, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureRight, new Rectangle((int)this.Location.X + this.Width - textureRight.Width, (int)this.Location.Y, textureRight.Width, textureRight.Height)));
						textureBottom = GetBorderTexture(this.Width, borderWidth, BorderColor);
						Borders.Add(new Tuple<Texture2D, Rectangle>(textureBottom, new Rectangle((int)this.Location.X, (int)this.Location.Y + this.Height - textureBottom.Height, textureBottom.Width, textureBottom.Height)));
						break;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="backgroundAsset">@"Graphics\GUI\" + backgroundAsset</param>
		/// <param name="typ"></param>
		/// <param name="borderColor"></param>
		public GUIPanel(string t, string backgroundAsset, BorderType typ, Color borderColor, Vector2 location, int width, int height) {
			Objekts = new Dictionary<string, IGUIElement>();
			BorderTyp = typ;
			this.BorderColor = borderColor;

			this.Location = location;
			this.Width = width;
			this.Height = height;

			this.BackgroundAsset = backgroundAsset;
			Background = Content.Load<Texture2D>(@"Graphics\GUI\" + backgroundAsset);

			SetIsEnable(true);

			Borders = new List<Tuple<Texture2D, Rectangle>>();
			CreateBorders();
			text = t;
		}
		#endregion

		public void Draw(Spritebatch batch) {
			if (isEnable) {

				if (Background != null) {
					batch.Draw(Background, new RectangleF((int)Location.X, (int)Location.Y, Width, Height), new RectangleF(0, 0, Background.Width, Background.Height), Color.White);
				}

				foreach (Tuple<Texture2D, Rectangle> i in Borders) {
					batch.Draw(i.Item1, new Rectangle(0, 0, i.Item1.Width, i.Item1.Height), i.Item2, Color.White);
				}

				foreach (IGUIElement o in Objekts.Select(x => x.Value).ToList<IGUIElement>()) {
					o.DrawToPosition(batch, new Vector2(Location.X + (float)o.GetPosition().X, Location.Y + (float)o.GetPosition().Y));
				}

			}
		}

		public void Update(GameTime gameTime) {
			if (isEnable) { 
			
			}
		}

		private Texture2D GetBorderTexture(int width, int height, Color color) {
			System.Drawing.Bitmap b = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					b.SetPixel(x, y, System.Drawing.Color.FromArgb(color.R, color.G, color.B));
				}
			}

			return new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, b));
		}

		public void AddObject(string name, IGUIElement o) {
			if (Objekts.ContainsKey(name)) {
				Objekts[name] = o;
			} else {
				Objekts.Add(name, o);
			}
		}

		public void ClearObjects() {
			Objekts.Clear();
		}

		public IGUIElement GetElementByName(string name) {
			IGUIElement o;
			this.Objekts.TryGetValue(name, out o);
			return o;
		}

		#region interface
		public void PerformClick(Vector2 position) {
			if (isEnable) {
				Vector2 newVetor = position - Location;
				var a = Objekts.Select(x => x.Value).Where(x => x.GetPosition().Intersects(new Rectangle((int)newVetor.X, (int)newVetor.Y, 1, 1)) && x.IsClickable() && x.IsEnable()).FirstOrDefault();
				if(a != null)
					a.PerformClick(newVetor);
			}
		}
		public Rectangle GetPosition() { return new Rectangle((int)Location.X, (int)Location.Y, Width, Height); }
		public bool IsEnable() { return isEnable; }
		public void SetIsEnable(bool enable) { this.isEnable = enable; Log.Log.Add("GuiPanel "+ this.text + ": SetEnable->" + enable.ToString()); }
		public bool IsClickable() { return true; }
		#endregion
	}
}
