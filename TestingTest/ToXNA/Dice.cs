﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Ein Zufallsgenerator.
	/// </summary>
	public static class Dice {
		private static int seedCounter = new Random(Guid.NewGuid().GetHashCode()).Next();

		[ThreadStatic]
		private static Random rng;

		public static Random Instance {
			get {
				if (rng == null) {
					int seed = Interlocked.Increment(ref seedCounter);
					rng = new Random(seed);
				}
				return rng;
			}
		}

		public static int GetRandomNumber(int min, int max) {
			return Dice.Instance.Next(min, max);
		}

		public static int GetRandomNumber(int max) {
			return GetRandomNumber(1, max);
		}

		public static int GetManyRandomNumber(int max, int Count) {
			if (max == 0 || Count == 0) { return 0; }

			int a = 0;
			for (int i = 0; i < Count; i++) {
				int b = GetRandomNumber(max + 1); //random can not generate the max number
				a += b > max ? max : b; //nur zur absicherung, falls das mal in .net gefixt wird
			}
			return a;
		}
	}
}
