﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Zum Tranfsverrieren von Objekten zwischen den Screens.
	/// </summary>
	public static class Transfer {
		private static Dictionary<string, object> DB = new Dictionary<string,object>();

		public static void Set<T>(T o, string key) {
			if (DB.ContainsKey(key)) {
				DB[key] = (T)o;
			} else {
				DB.Add(key, (T)o);
			}
		}

		/// <summary>
		/// After Get Removes the Key from Transfer
		/// </summary>
		/// <typeparam name="T">welcher Typ wird gewünscht</typeparam>
		/// <param name="key">wleches objekt</param>
		/// <returns></returns>
		public static T Get<T>(string key) {
			object o;
			DB.TryGetValue(key, out o);
			DB.Remove(key);

			return (T)o;
		}
	}
}
