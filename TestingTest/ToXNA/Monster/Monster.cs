﻿using System;
using System.Collections.Generic;
using System.Linq;
using SideProjekt.Komponents;
using System.Drawing;
using SideProjekt.ToXNA.Animation;
using SideProjekt.Interfaces;
using SideProjekt.ToXNA.Battle;
using TestingTest.Component;
using SharpDX;
using Color = SharpDX.Color;
using Point = SharpDX.Point;
using Rectangle = SharpDX.Rectangle;
using TestingTest.Interfaces;
using TestingTest.ToXNA;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Monster welches auf der Karten läuft und von Towern angegriffen wird.
	/// Das Monster kann unter mehreren Effekt (z.B. Gift und Ice) leiden.
	/// Der Schaden und der Effekt wird nur von Kugeln(Bullet) aufs Monster übertragen.
	/// 
	/// Monster besitzen zudem noch eine Rüstung und ein Element. (z.B. Feuerlement mit einer Rüstung die 25% des Schadens reduziert.
	/// </summary>
	public class Monster : IClickable {
		public string ThumbnailAsset { get; private set; } //Vorschaubild
		private Texture2D Thumbnail { get; set; }

		public string GraphicAsset { get; private set; } //Monsteraussehen
		public AnimatedSprite Sprite { get; set; }

		public float Movespeed { get; set; } //laufgeschwindigkeit
		public int Hitpoints { get; private set; } //Anzahl der Lebenspunkte
		public int MaxHitpoints { get; private set; } //Anzahl der MaximalenLebenspunkte
		
		public string Name { get; private set; } //Name des Monsters

		private Vector2 NextLocation { get; set; }
		public Vector2 Location { get; private set; } //wo befindet sich der Monster
		public Vector2 CenterLocation { get { return new Vector2(Location.X + Sprite.Width / 2, Location.Y + Sprite.Height / 2); } }
		public Rectangle Rectangle { //irgendwie schein location den centerpunkt zu sein.
			get {
				Vector2 offset = new Vector2((TileEngine.Tile.TileWidth - Sprite.Width) / 2, (TileEngine.Tile.TileHeight - Sprite.Height));
				return new Rectangle((int)Location.X + (int)offset.X, (int)Location.Y + (int)offset.Y, Sprite.Width, Sprite.Height);
			}
		}

		private Wave CurrentWave { get; set; }
		private Path Path { get; set; }
		public event EventHandler<MonsterArgs> MonsterHasReachedTheEndOfPath;
		public event EventHandler<MonsterArgs> MonsterWasKilled;

		private List<IEffekt> Effects = new List<IEffekt>();
		private List<IEffekt> EffectsToRemove = new List<IEffekt>();

		public Element Element { get; private set; }//todo set element and armor
		public ArmorType Armor { get; private set; }

		public int ScoreBonus { get; private set; }
		public int GoldBonus { get; private set; } //jeder Turm kostet Gold
		public int EnergieBonus { get; private set; } //elektische Türme verbrauchen Storm pro Schuß

		public Monster(string name, int hitpoints, string thumbnailAsset, string graphicAsset, float moveSpeed, Wave currentWave, Path path, bool loadSprite) {
			this.Name = name;
			this.Hitpoints = hitpoints;
			this.MaxHitpoints = this.Hitpoints;
			this.ThumbnailAsset = thumbnailAsset;
			this.GraphicAsset = graphicAsset;
			this.Movespeed = moveSpeed;
			this.CurrentWave = currentWave;

			this.Path = path;
			this.Location = Path.GetCurrentNodeAsVector();//location is first node vom Path
			CalcNextPathNode();

			if (loadSprite) {
				this.Sprite = Content.Load<AnimatedSprite>(@"Graphics\Monster\Ani\" + this.GraphicAsset);
			}
			this.Thumbnail = Content.Load<Texture2D>(@"Graphics\Monster\" + this.ThumbnailAsset);

			SetBonus(0, 0, 0);

			Element = Element.None;
			Armor = ArmorType.None;
		}

		public Monster(string name, int hitpoints, string thumbnailAsset, string graphicAsset, float moveSpeed) {
			this.Name = name;
			this.Hitpoints = hitpoints;
			this.MaxHitpoints = this.Hitpoints;
			this.ThumbnailAsset = thumbnailAsset;
			this.GraphicAsset = graphicAsset;
			this.Movespeed = moveSpeed;

			this.Thumbnail = Content.Load<Texture2D>(@"Graphics\Monster\" + this.ThumbnailAsset);
			this.Sprite = Content.Load<AnimatedSprite>(@"Graphics\Monster\Ani\" + this.GraphicAsset);

			SetBonus(0, 0, 0);
			Element = Element.None;
			Armor = ArmorType.None;
		}

		public void SetBonus(int score, int gold, int energie) {
			this.ScoreBonus = score;
			this.GoldBonus = gold;
			this.EnergieBonus = energie;
		}

		public void Update(GameTime time) {
			Sprite.Update(time);

			float distanceX = this.NextLocation.X - this.Location.X;
			float distanceY = this.NextLocation.Y - this.Location.Y;

			if (distanceX == 0f && distanceY != 0f) {
				if (distanceY < 0f) {
					Location = new Vector2(Location.X, (float)(Movespeed * time.TimeDiff.TotalSeconds) < distanceY ? Location.Y - distanceY : Location.Y - (float)(Movespeed * time.TimeDiff.TotalSeconds));
					Sprite.SetDirection(AnimationDirection.UP);
				} else {
					Location = new Vector2(Location.X, (float)(Movespeed * time.TimeDiff.TotalSeconds) > distanceY ? Location.Y + distanceY : Location.Y + (float)(Movespeed * time.TimeDiff.TotalSeconds));
					Sprite.SetDirection(AnimationDirection.DOWN);
				}
			}

			if (distanceY == 0f && distanceX != 0f) {
				if (distanceX < 0f) {
					Location = new Vector2((float)(Movespeed * time.TimeDiff.TotalSeconds) < distanceX ? Location.X - distanceX : Location.X - (float)(Movespeed * time.TimeDiff.TotalSeconds), Location.Y);
					Sprite.SetDirection(AnimationDirection.LEFT);
				} else {
					Location = new Vector2((float)(Movespeed * time.TimeDiff.TotalSeconds) > distanceX ? Location.X + distanceX : Location.X + (float)(Movespeed * time.TimeDiff.TotalSeconds), Location.Y);
					Sprite.SetDirection(AnimationDirection.RIGHT);
				}
			}

			if (distanceX == 0f && distanceY == 0f) {
				CalcNextPathNode();

				if (this.Location == NextLocation) {
					if (MonsterHasReachedTheEndOfPath != null) {
						MonsterHasReachedTheEndOfPath(this, new MonsterArgs(this, 0, 0, 0, null));
					}
				}
			}

			EffectsToRemove.ForEach(x => Effects.Remove(x));
			EffectsToRemove.Clear();
			Effects.ForEach(x => x.Update(time));
		}

		public void Draw(Spritebatch batch) {
			Vector2 offset = new Vector2((TileEngine.Tile.TileWidth - Sprite.Width) / 2, (TileEngine.Tile.TileHeight - Sprite.Height));
			Sprite.Draw(batch, Location + offset);

			if (Hitpoints != MaxHitpoints) {
				Texture2D hitpointsbar = CreateMonsterHitpointsBar();
				batch.Draw(hitpointsbar, new Rectangle(0, 0, Sprite.Width, Sprite.Height), new Rectangle(Convert.ToInt32(Location.X), Convert.ToInt32(Location.Y) + Sprite.Height + 5, hitpointsbar.Width, hitpointsbar.Height), Color.White);
				hitpointsbar.Dispose();
			}

			Effects.ForEach(x => x.Draw(batch));
		}

		internal Monster Clone() {
			Monster M = new Monster(this.Name, this.Hitpoints, this.ThumbnailAsset, this.GraphicAsset, this.Movespeed, this.CurrentWave, this.Path.Clone(), false);
			M.SetBonus(ScoreBonus, GoldBonus, EnergieBonus);
			M.Sprite = this.Sprite.Clone();
			return M;
		}

		internal Monster Clone(Wave w) {
			Monster M = new Monster(this.Name, this.Hitpoints, this.ThumbnailAsset, this.GraphicAsset, this.Movespeed, w, this.Path.Clone(), false);
			M.SetBonus(ScoreBonus, GoldBonus, EnergieBonus);
			M.Sprite = this.Sprite.Clone();
			return M;
		}

		internal Monster Clone(Wave w, Path p) {
			Monster M = new Monster(this.Name, this.Hitpoints, this.ThumbnailAsset, this.GraphicAsset, this.Movespeed, w, p.Clone(), false);
			M.SetBonus(ScoreBonus, GoldBonus, EnergieBonus);
			M.Sprite = this.Sprite.Clone();
			return M;
		}

		public void TakeDamage(int amount, Player p, Element ele, AttackType atk) {
			int realAmount = Calculator.CalcDmgOut(amount, Armor, atk, ele, Element);
			if (realAmount != amount) {
				Log.Log.Add("Fehler bei der Schadensberechnung, weil es noch keine Elemente gibt", Log.LogType.Error);
			}
			Hitpoints -= amount;
			if (this.Hitpoints <= 0) {
				if (MonsterWasKilled != null) {
					MonsterWasKilled(this, new MonsterArgs(this, ScoreBonus, GoldBonus, EnergieBonus, p));
				}
				CurrentWave.RemoveMonster(this);
			}
			ShowDamageAmountE(amount);
		}

		internal void AddEffekt(IEffekt iEffekt, Player p) {
			if (iEffekt == null)
				return;

			List<IEffekt> l = Effects.Where(x => x.GetType() == iEffekt.GetType() && x.IsActiv()).ToList();
			if (l.Count() > 0) {
				l.ForEach(x => x.ResetDuration());
			} else if (iEffekt != null) {
				IEffekt e = iEffekt.Clone(this, CurrentWave, p);
				e.HasEnded +=new EventHandler(e_HasEnded);
				Effects.Add(e);
			}
			l.Clear();
		}

		void e_HasEnded(object sender, EventArgs e) {
			EffectsToRemove.Add((IEffekt)sender);
		}

		private void CalcNextPathNode() {
			bool calc = true;
			int position = -1;
			Vector2 v = Vector2.Zero;
			NextLocation = Vector2.Zero;
			while (calc) {
				v = NextLocation;
				this.NextLocation = Path.GetNextNodeAsVector();

				if (v == NextLocation) {
					break;
				}

				float distanceX = this.NextLocation.X - this.Location.X;
				float distanceY = this.NextLocation.Y - this.Location.Y;

				if (distanceX > 0f && distanceY == 0f && (position == 1 || position == -1)) {
					position = 1;
					continue;
				}

				if (distanceX < 0f && distanceY == 0f && (position == 2 || position == -1)) {
					position = 2;
					continue;
				}

				if (distanceX == 0f && distanceY > 0f && (position == 3 || position == -1)) {
					position = 3;
					continue;
				}

				if (distanceX == 0f && distanceY < 0f && (position == 4 || position == -1)) {
					position = 4;
					continue;
				}

				calc = false;
			}

			Path.GoToPreviewNode();
			NextLocation = Path.GetCurrentNodeAsVector();
		}

		private Texture2D CreateMonsterHitpointsBar() {
			System.Drawing.Bitmap b = new Bitmap(Sprite.Width, 5, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			for (int i = 0; i < b.Width; i++) {
				System.Drawing.Color c = System.Drawing.Color.Green;

				if (b.Width * Hitpoints * 100 / MaxHitpoints / 100 <= i) {
					c = System.Drawing.Color.Red;
				}

				b.SetPixel(i, 0, c);
				b.SetPixel(i, 1, c);
				b.SetPixel(i, 2, c);
				b.SetPixel(i, 3, c);
				b.SetPixel(i, 4, c);
			}

			Texture2D green = new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, b));
			return green;
		}

		private void ShowDamageAmountE(int damage) {
			IEffekt e = new ShowDamageEffekt(1.0f, damage);
			e.SetMonster(this);
			e.HasEnded +=new EventHandler(e_HasEnded);
			AddEffekt(e, null);
		}

		#region Clickable Interface
		public void PerformClick(Vector2 position) { if (MonsterWasClicked != null) { MonsterWasClicked(this, new MonsterArgs(this, 0, 0, 0, null)); } }
		public Rectangle GetPosition() { return Rectangle; }
		public bool IsEnable() { return true; }
		public bool IsClickable() { return true; }
		public void SetIsEnable(bool enable) { }
		public event EventHandler<MonsterArgs> MonsterWasClicked;
		#endregion


		internal void Dispose() {
			Thumbnail.Dispose();
			Sprite.Dispose();
		}
	}

	public class MonsterArgs : EventArgs {
		public Monster monster { get; private set; }

		public int Score { get; private set; }
		public int Gold { get; private set; } //jeder Turm kostet Gold
		public int Energie { get; private set; } //elektische Türme verbrauchen Storm pro Schuß

		public Player FromPlayer { get; private set; }

		public MonsterArgs(Monster m, int score, int gold, int energie, Player p) {
			this.monster = m;
			this.Score = score;
			this.Gold = gold;
			this.Energie = energie;
			this.FromPlayer = p;
		}
	}
}
