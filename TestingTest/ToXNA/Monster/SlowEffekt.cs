﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Interfaces;
using SideProjekt.ToXNA;
using TestingTest.Component;

namespace TestingTest.ToXNA {
	/// <summary>
	/// Eine Verlangsamung des Monsters wird hiermit erreicht.
	/// Effekt wird über die Kugel (Bullet) aufs Monster übertragen.
	/// </summary>
	public class SlowEffekt : IEffekt {
		
		public float Amount { get; private set; }
		public float Duration { get; private set; }
		private float DurationTimer { get; set; }
		private float OldWalkAmount { get; set; }
		private Monster Monster { get; set; }

		public event EventHandler HasEnded;
		bool deleteMe;

		public SlowEffekt(float duration, float amount) {
			if (amount < 0 || amount > 1) {
				throw new Exception("Es sind nur Zahlen zwischen 0 und 1 erlaubt bei amount.");
			}

			if (duration < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei duration.");
			}

			Amount = amount;
			Duration = duration;
			DurationTimer = 0f;

			deleteMe = false;
		}

		public void SetMonster(Monster m) {
			Monster = m;
			DurationTimer = 0f;
			OldWalkAmount = Monster.Movespeed;
			Monster.Movespeed *= 1f - Amount;
		}

		public void SetMonsterAndPlayer(Monster m, Player p) {
			SetMonster(m);
		}

		public void Update(GameTime time) {
			if (!deleteMe) {
				DurationTimer += (float)time.TimeDiff.TotalSeconds;

				if (DurationTimer > Duration) {
					Monster.Movespeed /= 1f - Amount;
					if (HasEnded != null) {
						HasEnded(this, null);
					}
					deleteMe = true;
				}
			}
		}

		public void Draw(Spritebatch batch) {
			if (!deleteMe) { 
			
			}
		}


		public IEffekt Clone(Monster m, Wave w, Player p) {
			SlowEffekt returnValue = new SlowEffekt(this.Duration, this.Amount);
			returnValue.SetMonsterAndPlayer(m, p);
			returnValue.SetWave(w);
			returnValue.ResetDuration();

			return returnValue;
		}

		public void ResetDuration() {
			DurationTimer = 0f;
			deleteMe = false;
		}

		public bool IsActiv() {
			return !deleteMe;
		}

		public void SetWave(Wave w) { 
		
		}
	}
}
