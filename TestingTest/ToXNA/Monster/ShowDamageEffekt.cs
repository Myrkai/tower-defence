﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Interfaces;
using SideProjekt.ToXNA;
using TestingTest.Component;
using SideProjekt.Komponents;

namespace TestingTest.ToXNA {
	/// <summary>
	/// Der erlittende Schaden wird über dem Monster dargestellt.
	/// Effekt wird über die Kugel (Bullet) aufs Monster übertragen.
	/// </summary>
	public class ShowDamageEffekt : IEffekt {
		
		public float Amount { get; private set; }
		public float Duration { get; private set; }
		private float DurationTimer { get; set; }
		private Monster Monster { get; set; }

		float opercity = 1f;
		float positionY = 0f;
		int rotation;

		public event EventHandler HasEnded;
		bool deleteMe;

		public ShowDamageEffekt(float duration, float amount) {
			if (duration < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei duration.");
			}

			Amount = amount;
			Duration = duration;
			DurationTimer = 0f;
			rotation = 360 + Dice.GetRandomNumber(90) - 45;
			if (rotation > 360)
				rotation -= 360;

			deleteMe = false;
		}

		public void SetMonster(Monster m) {
			Monster = m;
			DurationTimer = 0f;
		}

		public void SetMonsterAndPlayer(Monster m, Player p) {
			SetMonster(m);
		}

		public void Update(GameTime time) {
			if (!deleteMe) {
				DurationTimer += (float)time.TimeDiff.TotalSeconds;
				opercity = 1f - (1f * ((DurationTimer * 100f / Duration) / 100f));
				positionY = 30f * DurationTimer;

				if (DurationTimer > Duration) {
					if (HasEnded != null) {
						HasEnded(this, null);
					}
					deleteMe = true;
				}
			}
		}


		//ich möchte das die Texte zufällig vom monster nach oben wegschieben und verblassen.
		public void Draw(Spritebatch batch) {
			if (!deleteMe) {
				SpriteFont font = new SpriteFont(10f, SharpDX.Color.Yellow);
				SharpDX.Vector2 p = new SharpDX.Vector2(Monster.GetPosition().X + Monster.GetPosition().Width / 2, Monster.GetPosition().Y);
				batch.DrawText(Amount.ToString(), font, MathHelper.RotateAPoint(p + new SharpDX.Vector2(0, -10 - positionY), rotation, p), opercity);
			}
		}


		public IEffekt Clone(Monster m, Wave w, Player p) {
			ShowDamageEffekt returnValue = new ShowDamageEffekt(this.Duration, this.Amount);
			returnValue.SetMonsterAndPlayer(m, p);
			returnValue.SetWave(w);
			returnValue.ResetDuration();

			return returnValue;
		}

		public void ResetDuration() {
			DurationTimer = 0f;
			opercity = 1f;
			positionY = 0f;
			deleteMe = false;
		}

		/// <summary>
		/// ist immer false.
		/// Denn es können unendliche viele Texte hinzugefügt werden.
		/// Diese werden dann auch das Event aufgeräumt in der Monster-Klasse.
		/// </summary>
		/// <returns></returns>
		public bool IsActiv() {
			return false;//!deleteMe;
		}

		public void SetWave(Wave w) { 
		
		}
	}
}
