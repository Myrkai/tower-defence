﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Interfaces;

namespace TestingTest.ToXNA {
	/// <summary>
	/// Es kann das jeweilige Effekt schnell erstellt werden.
	/// Zentrale Anlaufstelle zur Generierung von Effekten.
	/// </summary>
	public static class EffektManager {

		public static IEffekt Get(string type, float amount, float duration) {
			return EffektManager.Get(type, amount, duration, 0);
		}

		public static IEffekt Get(string type, float amount, float duration, int width) {
			switch (type) {
				case "NULL": return null;
				case "PoisonEffekt":
					return new PoisonEffekt(duration, (int)amount);
				case "SlowEffekt":
					return new SlowEffekt(duration, amount);
				case "AoEEffekt":
					return new AoEEffekt(duration, amount, width);
				default:
					throw new NotImplementedException();
			}
		}
	}
}
