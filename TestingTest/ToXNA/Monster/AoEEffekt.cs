﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Interfaces;
using SideProjekt.ToXNA;
using TestingTest.Component;
using SideProjekt.ToXNA.Battle;
using SideProjekt.Komponents;
using SharpDX;

namespace TestingTest.ToXNA {

	/// <summary>
	/// Hier wird ein flächenschaden Umgesetzt.
	/// Die Fläche ist dabei ein Kreis und alle Monster die den Kreis berühren erhalten dann den jeweiligen Schaden.
	/// Effekt wird über die Kugel (Bullet) aufs Monster übertragen und das Monster bildet dann den Mittelpunkt.
	/// 
	/// Kurz gesagt Flächenschaden(AoE).
	/// </summary>
	public class AoEEffekt : IEffekt {
		
		public float Amount { get; private set; }
		public float Duration { get; private set; }
		private float DurationTimer { get; set; }
		private Monster Monster { get; set; }
		private Player Player { get; set; }
		private Wave Wave { get; set; }
		private float AreaWidth;

		bool oneTimeDamageDone = false;

		public event EventHandler HasEnded;
		bool deleteMe;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="duration">wie lange wird der effekt gezeichnet</param>
		/// <param name="amount">höhe des schadens</param>
		public AoEEffekt(float duration, float amount, float area) {
			if (amount < 0f) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei amount.");
			}

			if (duration < 0f) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei duration.");
			}

			Amount = amount;
			Duration = duration;
			DurationTimer = 0f;
			AreaWidth = area;

			deleteMe = false;
		}

		public void SetMonster(Monster m) {
			Monster = m;
			DurationTimer = 0f;
			deleteMe = false;
		}

		public void SetMonsterAndPlayer(Monster m, Player p) {
			SetMonster(m);
			Player = p;
		}

		public void Update(GameTime time) {
			if (!deleteMe) {
				if (!oneTimeDamageDone && Monster != null && Wave != null && Player != null) {
					foreach (Monster monster in Wave.MonsterOnField) {
						if (MathHelper.IsPointInCircle(Monster.CenterLocation.X, Monster.CenterLocation.Y, AreaWidth / 2f, monster.CenterLocation.X, monster.CenterLocation.Y)) {
							monster.TakeDamage((int)Amount, Player, Element.None, AttackType.None);
						}
					}

					oneTimeDamageDone = true;
				}
				
				DurationTimer += (float)time.TimeDiff.TotalSeconds;

				if (DurationTimer > Duration) {
					if (HasEnded != null) {
						HasEnded(this, null);
					}
					deleteMe = true;
				}
			}
		}

		public void Draw(Spritebatch batch) {
				batch.DrawCircle((int)AreaWidth, Color.White, Monster.CenterLocation);
		}


		public IEffekt Clone(Monster m, Wave w, Player p) {
			AoEEffekt returnValue = new AoEEffekt(this.Duration, this.Amount, this.AreaWidth);
			returnValue.SetMonsterAndPlayer(m, p);
			returnValue.SetWave(w);
			returnValue.DurationTimer = 0f;
			returnValue.deleteMe = false;

			return returnValue;
		}

		public void ResetDuration() {
			DurationTimer = 0f;
			deleteMe = true;
			if (!oneTimeDamageDone && Monster != null) {
				foreach (Monster monster in Wave.MonsterOnField) {
					if (MathHelper.IsPointInCircle(Monster.CenterLocation.X, Monster.CenterLocation.Y, AreaWidth / 2f, monster.CenterLocation.X, monster.CenterLocation.Y)) {
						monster.TakeDamage((int)Amount, Player, Element.None, AttackType.None);
					}
				}

				oneTimeDamageDone = true;
			}
			if (HasEnded != null) {
				HasEnded(this, null);
			}
		}

		public bool IsActiv() {
			return !deleteMe;
		}

		public void SetWave(Wave w) {
			this.Wave = w;
			DurationTimer = 0f;
			deleteMe = false;
		}
	}
}
