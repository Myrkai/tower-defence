﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Interfaces;
using SideProjekt.ToXNA;
using TestingTest.Component;
using SideProjekt.ToXNA.Battle;

namespace TestingTest.ToXNA {
	/// <summary>
	/// Eine kontinuierliche Verletzung wird erreicht. Sodas z.B. 20 Schaden über 5 Sekunden gleichmäßig verteilt werden.
	/// Effekt wird über die Kugel (Bullet) aufs Monster übertragen.
	/// 
	/// Kurz gesagt ein Schaden über Zeit (DoT).
	/// </summary>
	public class PoisonEffekt : IEffekt {
		
		private Monster Monster { get; set; }

		public float deltaHitpoints { get; private set; } //Lebenspunkte pro Ticket die verursachtg wurden. nur Wichtig bei DoT (Damage over Time).
		public int DotDamage { get; private set; }
		public float DotTime { get; private set; }
		private Player DotPlayer { get; set; }

		private int StartDotDamage { get; set; }
		private float StartDotTime { get; set; }

		public event EventHandler HasEnded;
		bool deleteMe;

		public PoisonEffekt(float duration, int amount) {
			if (amount < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei amount.");
			}

			if (duration < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei duration.");
			}

			StartDotDamage = DotDamage = amount;
			StartDotTime = DotTime = duration;

			deltaHitpoints = 0f;

			deleteMe = false;
		}

		public PoisonEffekt(float duration, int amount, Monster m, Player p) {
			if (amount < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei amount.");
			}

			if (duration < 0) {
				throw new Exception("Es sind nur Zahlen größer 0 erlaubt bei duration.");
			}

			StartDotDamage = DotDamage = amount;
			StartDotTime = DotTime = duration;

			deltaHitpoints = 0f;

			SetMonsterAndPlayer(m, p);
			deleteMe = false;
		}

		public void SetMonsterAndPlayer(Monster m, Player p) {
			Monster = m;
			DotPlayer = p;
			ResetDuration();
		}

		public void SetMonster(Monster m) {
			Monster = m;
			ResetDuration();
		}

		public void ResetDuration() {
			deleteMe = false;
			DotTime = StartDotTime;
			DotDamage = StartDotDamage;
		}

		public void Update(GameTime time) {
			if (!deleteMe) {
				if (DotTime > 0f && DotDamage > 0) {
					float DotTimer = (float)time.TimeDiff.TotalSeconds;
					deltaHitpoints += DotDamage / DotTime * DotTimer;
					
					int dmg = (int)deltaHitpoints;
					if (dmg > 0) {
						Monster.TakeDamage(dmg, DotPlayer, Element.None, AttackType.None); //der schaden wurde schon berechnet
					}
					
					DotDamage -= (int)deltaHitpoints;
					deltaHitpoints -= (int)deltaHitpoints;
					DotTime -= DotTimer;
				} else {
					if (HasEnded != null) {
						HasEnded(this, null);
					}
					deleteMe = true;
				}
			}
		}

		public void Draw(Spritebatch batch) {
			if (!deleteMe) { 
			
			}
		}

		public IEffekt Clone(Monster m, Wave w, Player p) {
			PoisonEffekt returnValue = new PoisonEffekt(this.StartDotTime, this.StartDotDamage, m, p);
			returnValue.SetWave(w);
			returnValue.ResetDuration();

			return returnValue;
		}

		public bool IsActiv() {
			return !deleteMe;
		}

		public void SetWave(Wave w) { 
		
		}
	}
}
