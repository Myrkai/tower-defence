﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Verwaltet alle Clickbaren Objekte pro Screen.
	/// Dieser löst auch die Klick-Events aus.
	/// </summary>
	public class ObjektManager {
		private List<IClickable> clickableObjekts;

		public ObjektManager() {
			clickableObjekts = new List<IClickable>();
		}

		public void Update(GameTime gameTime) {
			if (VirtualGamepad.IsMouseClicked()) {
				PerformClick(new Vector2(VirtualGamepad.MouseClickPoint().X, VirtualGamepad.MouseClickPoint().Y));
			}
		}

		public void AddClickableObjekt(IClickable o) {
			clickableObjekts.Add(o);
		}

		public void RemoveClickableObjekt(IClickable o) {
			clickableObjekts.Remove(o);
		}

		public void PerformClick(Vector2 position) {
			Log.Log.Add("try perform a Click to a object");
			var a = clickableObjekts.Where(x => x.GetPosition().Intersects(new SharpDX.Rectangle((int)position.X, (int)position.Y, 1, 1)) && x.IsClickable() && x.IsEnable()).FirstOrDefault();

			if (a != null && a.IsEnable())
				((IClickable)a).PerformClick(position);
		}

		public void Reset() {
			clickableObjekts.Clear();
		}

		public int Count() {
			return clickableObjekts.Count();
		}
	}
}
