﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace SideProjekt.ToXNA.Battle {
	/// <summary>
	/// Stellt das jeweilige Element mit seinen Schwächen und Stärken dar, ala Pokemon. 
	/// Z.B. Feuer. Dieses kann als Stärke Wasser haben und als Schwäche Erde.
	/// 
	/// Monster können Elemente haben, sowie die Tower. 
	/// Sodas ein Feuerturm mehr Schaden an Wassermonster macht und weniger and Erd-Monstern.
	/// </summary>
	public class Element {
		private List<ElementType> StrongAgainst;
		private List<ElementType> WeakAgainst;
		public ElementType ElementType { get; private set; }
		public string Title { get; private set; }
		public ReadOnlyCollection<ElementType> Strongs { get { return new ReadOnlyCollection<ElementType>(StrongAgainst); } }
		public ReadOnlyCollection<ElementType> Weaks { get { return new ReadOnlyCollection<ElementType>(WeakAgainst); } }
		public string StrongAgainstString {
			get { 
				string t = "";
				foreach(ElementType e in Strongs) {
					t += e.ToString() + ", ";
				}

				if (t == "") {
					return ElementType.None.ToString();
				} else {
					return t.Trim().Substring(0, t.Trim().Length - 1);
				}
			}
		}
		public string WeakAgainstString {
			get {
				string t = "";
				foreach (ElementType e in Weaks) {
					t += e.ToString() + ", ";
				}

				if (t == "") {
					return ElementType.None.ToString();
				} else {
					return t.Trim().Substring(0, t.Trim().Length - 1);
				}
			}
		}

		public Element() {
			StrongAgainst = new List<ElementType>();
			WeakAgainst = new List<ElementType>();
			ElementType = Battle.ElementType.None;
			Title = ElementType.ToString();
		}

		public Element(string title, ElementType element, ElementType weak, ElementType strong) {
			this.Title = title;
			StrongAgainst = new List<ElementType>();
			WeakAgainst = new List<ElementType>();
			StrongAgainst.Add(strong);
			WeakAgainst.Add(weak);
			this.ElementType = element;
		}

		public Element(string title, ElementType element, ElementType strong) {
			this.Title = title;
			StrongAgainst = new List<ElementType>();
			WeakAgainst = new List<ElementType>();
			StrongAgainst.Add(strong);
			this.ElementType = element;
		}

		public Element(string title, ElementType element) {
			this.Title = title;
			StrongAgainst = new List<ElementType>();
			WeakAgainst = new List<ElementType>();
			this.ElementType = element;
		}

		public void AddStrongElement(ElementType e) {
			if (StrongAgainst.Where(x => x == e).Count() == 0) {
				StrongAgainst.Add(e);
			}
		}

		public void AddWeakElement(ElementType e) {
			if (WeakAgainst.Where(x => x == e).Count() == 0) {
				WeakAgainst.Add(e);
			}
		}

		public bool IsElementStrongAgainst(ElementType e) {
			return StrongAgainst.Where(x => x == e).Count() > 0;
		}

		public bool IsElementWeakAgainst(ElementType e) {
			return WeakAgainst.Where(x => x == e).Count() > 0;
		}

		public Element Clone() {
			Element tmp = new Element();
			tmp.Title = this.Title;
			tmp.ElementType = this.ElementType;

			foreach (ElementType e in this.Weaks) {
				tmp.AddWeakElement(e);
			}

			foreach (ElementType e in this.Strongs) {
				tmp.AddStrongElement(e);
			}

			return tmp;
		}

		public static Element None { get { return new Element("None", ElementType.None); } }
	}
}
