﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.ToXNA.Battle {
	public enum ArmorType { 
		None, // 100% dmg both
		Light, //90%dmg phy, 100%mag
		Medium, //80% dmg phy, 100%mag
		Heavy, //70% dmg phy, 100%mag
		Resistant, //100% dmg phy, 70%mag
		Spectral, //95%phy, 95% mag
		Mystic, //85%phy, 85%mag
		Godly //75%phy, 75%mag
	}

	public enum AttackType { 
		Physical, //phischer Dmg, like Sword
		Magic, //magic dmg, like fireball
		Both, //ist sowohl physisch wie magisch
		PiercePhysical, //Pierceing dmg verursacht 25% direkten schaden, vorbei an der Rüstung
		PierceMagic, //Pierceing dmg verursacht 25% direkten schaden, vorbei an der Rüstung
		PierceBoth, //Pierceing dmg verursacht 25% direkten schaden, vorbei an der Rüstung
		None
	}

	//Elemente können anfällig gegen etwas sein (200% dmg) oder stark gegen etwas sein (50%dmg).
	public enum ElementType { 
		None,
		Normal,
		Fire,
		Water,
		Earth,
		Wind,
		Dark,
		Holy,
		Dragon,
		Metal,
		Fight,
		Fly,
		Posion,
		Bug,
		Gohst,
		Natur,
		Elektric,
		Psycho,
		Ice,
		Fee
	}

	/// <summary>
	/// Hilfestellung für die Berechnung des Schadens.
	/// Der Berücksichtig Rüstungstypen, Waffentypen und die jeweiligen Elemente;
	/// </summary>
	public static class Calculator {
		/// <summary>
		/// Berechnet den erlittenden Schaden
		/// </summary>
		/// <param name="dmg">Schadenhöhe des Angreifers</param>
		/// <param name="armor">Welche Rüstung hat der angegriffende</param>
		/// <param name="atk">Welcher Angrifstyp hat der Angreifer</param>
		/// <returns></returns>
		public static double CalcDmgOut(double dmg, ArmorType armor, AttackType atk) {
			double direktDmg  = 0;
			double redurceDmg = 0;

			if (atk == AttackType.PierceBoth || atk == AttackType.PierceMagic || atk == AttackType.PiercePhysical) {
				direktDmg = dmg * 0.25;
				redurceDmg = dmg * 0.75;
			} else {
				redurceDmg = dmg;
			}

			switch(armor) {
				case ArmorType.None:
					break;
				case ArmorType.Light:
					if (atk == AttackType.PiercePhysical || atk == AttackType.PierceBoth || atk == AttackType.Physical || atk == AttackType.Both) {
						redurceDmg = redurceDmg * 0.9;
					}
					break;
				case ArmorType.Medium:
					if (atk == AttackType.PiercePhysical || atk == AttackType.PierceBoth || atk == AttackType.Physical || atk == AttackType.Both) {
						redurceDmg = redurceDmg * 0.8;
					}
					break;
				case ArmorType.Heavy:
					if (atk == AttackType.PiercePhysical || atk == AttackType.PierceBoth || atk == AttackType.Physical || atk == AttackType.Both) {
						redurceDmg = redurceDmg * 0.7;
					}
					break;
				case ArmorType.Resistant:
					if (atk == AttackType.PierceMagic || atk == AttackType.PierceBoth || atk == AttackType.Magic || atk == AttackType.Both) {
						redurceDmg = redurceDmg * 0.7;
					}
					break;
				case ArmorType.Spectral:
					redurceDmg = redurceDmg * 0.95;
					break;
				case ArmorType.Mystic:
					redurceDmg = redurceDmg * 0.85;
					break;
				case ArmorType.Godly:
					redurceDmg = redurceDmg * 0.75;
					break;
			}

			return direktDmg + redurceDmg;
		}

		public static int CalcDmgOut(int dmg, ArmorType armor, AttackType atk) {
			return (int)CalcDmgOut((double)dmg, armor, atk);
		}

		public static double CalcDmgOut(double dmg, ArmorType armor, AttackType atk, Element attacker, Element defender) {

			double a = dmg * (attacker.IsElementStrongAgainst(defender.ElementType) ? 2 : 1);
			a = dmg * (attacker.IsElementWeakAgainst(defender.ElementType) ? 0.5 : 1);

			return CalcDmgOut(a, armor, atk);
		}

		public static double CalcDmgOut(double dmg, ArmorType armor, AttackType atk, List<Element> attacker, List<Element> defender) {

			double multi = 1;

			foreach (Element e in attacker) {
				foreach (Element def in defender) {
					if (e.IsElementStrongAgainst(def.ElementType)) {
						multi *= 2;
					}

					if (e.IsElementWeakAgainst(def.ElementType)) {
						multi *= 0.5;
					}
				}
			}
			
			return CalcDmgOut(dmg * multi, armor, atk);
		}

		public static int CalcDmgOut(int dmg, ArmorType armor, AttackType atk, Element attacker, Element defender) { 
			return (int)CalcDmgOut((double)dmg, armor, atk, attacker, defender);
		}

		public static int CalcDmgOut(int dmg, ArmorType armor, AttackType atk, List<Element> attacker, List<Element> defender) {
			return (int)CalcDmgOut((double)dmg, armor, atk, attacker, defender);
		}

		/// <summary>
		/// Trägt automatisch die Schwächen in die Elementen ein. (z.B. Feuer-StrongAgainst-Wood => dann wird automatisch bei Wood-WeakAgainst-Feuer eingetragen)
		/// </summary>
		/// <param name="elements">sämtliche Elemente die es gibt</param>
		public static void CalcWeakerElementsFromStronger(ref List<Element> elements) {
			foreach (Element e in elements) {
				foreach (ElementType eStrong in e.Strongs) {
					var a = elements.SingleOrDefault(x => x.ElementType == eStrong);
					if (a != null) { 
						a.AddWeakElement(e.ElementType);
					}
				}
			}
		}
	}
}
