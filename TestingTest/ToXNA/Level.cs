﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SideProjekt.TileEngine;
using TestingTest.Component;

namespace SideProjekt.ToXNA {
	/// <summary>
	/// Das jeweilige Level welches die Unterschiedlichen Waves beinnhaltet.
	/// Im Level sind auch die Tower vorgegeben und auch welche Monster erscheinen können.
	/// </summary>
	public class Level {
		public List<Wave> Waves;
		public List<Tower> Towers; //welche Tower können gebaut werden.
		public TileMap myMap;

		private int currentIndexForWave;

		public Wave currentWave { get; private set; }
		public event EventHandler<LevelArgs> CurrentWaveChanged;
		public event EventHandler<WaveEventArgs> WaveIsCompleted;
		public event EventHandler LevelIsCompleted;
		public event EventHandler<MonsterArgs> MonsterHasReachedEndOfPath;
		public event EventHandler<MonsterArgs> MonsterWasKilled;
		public event EventHandler<MonsterArgs> MonsterWasSendet;

		public Level() {
			Waves = new List<Wave>();
			Towers = new List<Tower>();
			myMap = new TileMap(@"TileSets\basesquare");
		}

		public void SetCurrentWave(int index) {
			currentIndexForWave = index;

			if (index >= Waves.Count()) {
				if (LevelIsCompleted != null) {
					LevelIsCompleted(this, null);
				}
			} else {
				currentWave = Waves[index];
				currentWave.SetMap(myMap);
				currentWave.AllMonsterDead += new EventHandler<WaveEventArgs>(currentWave_AllMonsterDead);
				currentWave.MonsterHasReachedTheEnd += new EventHandler<MonsterArgs>(currentWave_MonsterHasReachedTheEnd);
				currentWave.MonsterWasKilled += new EventHandler<MonsterArgs>(currentWave_MonsterWasKilled);
				currentWave.MonsterWasSendet += new EventHandler<MonsterArgs>(currentWave_MonsterWasSendet);

				if (CurrentWaveChanged != null) {
					CurrentWaveChanged(this, new LevelArgs(currentWave));
				}
			}
		}

		void currentWave_MonsterWasSendet(object sender, MonsterArgs e) {
			if (MonsterWasSendet != null) {
				MonsterWasSendet(this, e);
			}
		}

		void currentWave_MonsterWasKilled(object sender, MonsterArgs e) {
			if (MonsterWasKilled != null) {
				MonsterWasKilled(this, e);
			}
		}

		void currentWave_MonsterHasReachedTheEnd(object sender, MonsterArgs e) {
			if (MonsterHasReachedEndOfPath != null) {
				MonsterHasReachedEndOfPath(this, e);
			}
		}

		void currentWave_AllMonsterDead(object sender, WaveEventArgs e) {
			if (WaveIsCompleted != null) {
				WaveIsCompleted(this, new WaveEventArgs(e.CurrentWave, e.GoldBonus, e.EnergieBonus, e.LivesBonus));
			}
			SetCurrentWave(currentIndexForWave+1);
		}

		/// <summary>
		/// Hat die Funktionen DrawMap & DrawCurrentWave
		/// </summary>
		/// <param name="batch"></param>
		public void DrawAll(Spritebatch batch) {
			DrawMap(batch);
			DrawCurrentWave(batch);
		}

		public void DrawMap(Spritebatch batch) {
			myMap.Draw(batch);
		}

		public void DrawCurrentWave(Spritebatch batch) {
			currentWave.Draw(batch);
		}

		public void Update(GameTime time) {
			myMap.Update(time);
			currentWave.Update(time);
		}

		public void AddTower(Tower t) {
			t.SetCurrentWave(currentWave);
			this.Towers.Add(t);
		}

		public Tower GetTowerByName(string name) {
			return Towers.Where(x => x.TowerName == name).First();
		}

		public int GetMapWidthInPixel() {
			return myMap.TotalMapWidth;
		}

		public int GetMapHeightInPixel() {
			return myMap.TotalMapHeight;
		}

		public int GetTileWidth() {
			return myMap.TileWidth;
		}

		public int GetTileHeight() {
			return myMap.TileHeight;
		}
	}

	public class LevelArgs : EventArgs {
		public Wave currentWave;
		public LevelArgs(Wave W) {
			currentWave = W;
		}
	}
}
