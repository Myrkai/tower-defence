﻿using System;
using SideProjekt.Komponents;
using System.Drawing;
using SideProjekt.ToXNA.GUI;
using SideProjekt.ToXNA.Animation;
using TestingTest.Component;
using SharpDX;

using Color = SharpDX.Color;
using SharpDX_Tutorial01;

namespace SideProjekt.ToXNA.Screens {
	/// <summary>
	/// Screen zur Namenseingabe für den Spieler.
	/// </summary>
	public class NameInputScreen : BaseScreen {
		private TextInputControl textInput;
		private LinkControl text;
		private AnimatedSprite sprite;

		public NameInputScreen(Game01 game, ScreenManager manager)
			: base(game, manager) {

		}

		public void Initialize() {
			base.Init();
			textInput = new TextInputControl(
				"Spieler 1", new Vector2(245, 90), Color.White, Color.Black.Lighter(0.30f), Color.White, 495, 50, Alignment.Center, //TextBox
				new Vector2(245, 150), 5, 10, Color.Black, Color.White, Color.Black, Color.White, Color.White, 13f, Color.Red //KeyBoard
			);
			textInput.EnterWasPressed += new EventHandler<ButtonEventArgs>(textInput_EnterWasPressed);
			//textInput.PerformClick(new Vector2(250, 92));

			text = new LinkControl("Hello Guy, please enter your name!", new Vector2(215, 20), new SpriteFont(33f, Color.White));

			ObjektManager.AddClickableObjekt(textInput);

			sprite = Content.Load<AnimatedSprite>(@"Graphics\Monster\Ani\OrcShamanWoman1");
			sprite.SetDirection(AnimationDirection.DOWN);
		}

		public void LoadContent() {

		}

		public override void Update(GameTime time) {
			if (isActiv && doUpdate) {
				base.Update(time);

				textInput.Update(time);
				text.Update(time);

				sprite.Update(time);
			}

			totalTime = time.TotalSecondsFromStart;

		}

		float totalTime;
		public override void Draw(Spritebatch batch) {
			if (isActiv && doDraw) {
				//batch.BeginScene();
				batch.ClearScreen(Color.Black);
				base.Draw(batch);

				textInput.Draw(batch);
				text.Draw(batch);

				sprite.Draw(batch, new Vector2(200, 95));
				sprite.Draw(batch, new Vector2(750, 95));
				//batch.EndScene();

				//das A fliegt nun in einer Sinuskurve
				//todo mach daraus eine linie
				//float weite = 50f; //amplitudenweite/-intervall
				//float höhe = 40f;  //amplitudenhöhe
				//float speed = 1f; //animationsgeschwindigkeit
				//batch.DrawText("A", SpriteFont.GetFont(10f), new Vector2(100f + totalTime * weite * speed, 300f + (float)Math.Sin(totalTime * speed) * höhe));
				
				batch.DrawSinus(new Vector2(100f, 300f), 800f);
			}
		}

		#region Events
		void textInput_EnterWasPressed(object sender, ButtonEventArgs e) {
			Transfer.Set<string>(textInput.Text, "PlayerName");
			GameRef.gameScreen.Initialize();
			ScreenManagerRef.Transfer(GameRef.gameScreen);
		}
		#endregion

	}
}
