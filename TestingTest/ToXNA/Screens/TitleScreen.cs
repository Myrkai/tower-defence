﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX_Tutorial01;
using Rectangle = SharpDX.Rectangle;
using Color = SharpDX.Color;

namespace SideProjekt.ToXNA.Screens {
	/// <summary>
	/// Der Titelbildschirm. Anzeige eines Bilders und auffordern eine beliebige Taste zu drücken.
	/// </summary>
	public class TitleScreen : BaseScreen {
		Texture2D background;
		float timer;

		public TitleScreen(Game01 game, ScreenManager manager)
			: base(game, manager) {
				timer = 0f;
		}

		public void Initialize() {
			base.Init();
		}

		public void LoadContent() {
			background = Content.Load<Texture2D>(@"Graphics\Screens\TitleScreen");
		}

		public override void Update(GameTime time) {
			if (isActiv && doUpdate) {
				base.Update(time);
				timer += (float)time.TimeDiff.TotalSeconds;
				if (timer > 5f) {
					ScreenManagerRef.Transfer(GameRef.nameInputScreen);
					doUpdate = false;
				}

				if (VirtualGamepad.IsMouseClicked()) {
					ScreenManagerRef.Transfer(GameRef.nameInputScreen);
					doUpdate = false;
				}
			}
		}

		public override void Draw(Spritebatch batch) {
			if (isActiv && doDraw) {
				base.Draw(batch);
				batch.ClearScreen(SharpDX.Color.Black);
				batch.Draw(background, new Rectangle(0, 0, background.Width, background.Height), new Rectangle(0, 0, background.Width, background.Height), Color.White);
			}
		}
	}
}
