﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Interfaces;
using SideProjekt.Komponents;
using TestingTest.Component;
using SharpDX_Tutorial01;

namespace SideProjekt.ToXNA.Screens {
	/// <summary>
	/// Basisklassen für alle Bildschirme.
	/// Stelle das eigentliche Spiel nochmals zur Verfügung.
	/// Sowie den Screenmanager(zum schnelleren welchseln auf anderen Bildschirme).
	/// Und den ObjektManager, der alle GUIElement verwalter und die jeweiligen Klick-Events aufruft.
	/// </summary>
	public class BaseScreen : IDrawable, IUpdateable, IScreen {
		public Game01 GameRef { get; private set; }
		public ScreenManager ScreenManagerRef { get; private set; }
		public ObjektManager ObjektManager { get; private set; }
		internal bool isActiv;
		internal bool doUpdate;
		internal bool doDraw;

		public void Init() {
			ObjektManager.Reset();
		}

		public BaseScreen(Game01 game, ScreenManager manager) {
			this.GameRef = game;
			ScreenManagerRef = manager;
			this.ObjektManager = new ObjektManager();
			isActiv = true;
			doUpdate = true;
			doDraw = true;

			Log.Log.Add("create " + this.ToString().Substring(this.ToString().LastIndexOf('.') + 1));
		}

		public BaseScreen(Game01 game, ScreenManager manager, bool isActiv, bool doUpdate, bool doDraw) {
			this.GameRef = game;
			ScreenManagerRef = manager;
			this.ObjektManager = new ObjektManager();
			this.isActiv = isActiv;
			this.doUpdate = doUpdate;
			this.doDraw = doDraw;

			Log.Log.Add("create " + this.ToString().Substring(this.ToString().LastIndexOf('.') + 1));
		}

		public virtual void Draw(Spritebatch batch) {
			if (isActiv && doDraw) { 
			
			}
		}

		public virtual void Update(GameTime gameTime) {
			if (isActiv && doUpdate) {
				this.ObjektManager.Update(gameTime);
			}
		}

		//ist das Fenstergerade aktiv
		public bool IsActiv() {
			return isActiv;
		}

		// soll das aktive Fenster seine DrawMethoden aufführen.
		public bool DoDraw() {
			return doDraw;
		}

		//soll das aktive Fenster seine UpdateMethoden ausführen
		public bool DoUpdate() {
			return doUpdate;
		}

		public void SetIsActiv(bool activ) { isActiv = activ; }
		public void SetDoDraw(bool draw) { doDraw = draw; }
		public void SetDoUpdate(bool update) { doUpdate = update; }
	}
}
