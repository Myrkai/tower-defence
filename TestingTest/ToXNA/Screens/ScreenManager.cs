﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SideProjekt.Interfaces;
using SideProjekt.ToXNA.Log;
using TestingTest.Component;

namespace SideProjekt.ToXNA.Screens {
	/// <summary>
	/// Verwaltet alle Screens und zeichnet immer nur diese die derzeit aktiv sind.
	/// </summary>
	public class ScreenManager {
		private static Stack<IScreen> Screens { get; set; }

		public ScreenManager() {
			Screens = new Stack<IScreen>();
		}

		public void Update(GameTime time) {
			List<IScreen> l = Screens.Where(x => x.IsActiv() && x.DoUpdate()).ToList();
			try {
				foreach (IScreen i in l) {
					i.Update(time);
				}
			} catch (NotImplementedException ex) {
				throw;
			} catch (Exception ex) {
				Log.Log.Add(string.Format("{0}: {1}", ex.GetType().ToString(), ex.Message), LogType.Error);
				Log.Log.Add(string.Format(ex.ToString()), LogType.Debug);
			}
		}

		public void Draw(Spritebatch batch) {
			List<IScreen> l = Screens.Where(x => x.IsActiv() && x.DoDraw()).ToList();
			try {
				foreach (IScreen i in l) {
					i.Draw(batch);
				}
			} catch (NotImplementedException ex) {
				throw;
			} catch (Exception ex) {
				Log.Log.Add(string.Format("{0}: {1}", ex.GetType().ToString(), ex.Message), LogType.Error);
				Log.Log.Add(string.Format(ex.ToString()), LogType.Debug);
			}
			l.Clear();
		}

		/// <summary>
		/// fügt einen Screen auf den Stack hinzu
		/// </summary>
		/// <param name="screen"></param>
		public void AddScreen(IScreen screen) {
			Log.Log.Add("Add Screen " + screen.ToString().Substring(screen.ToString().LastIndexOf('.') + 1) + " to ScreenManager");
			Screens.Push(screen);
		}

		/// <summary>
		/// fügt einen Screen auf den Stack hinzu
		/// </summary>
		/// <param name="screen"></param>
		/// <param name="stopAllOther">sollen alle anderen Screen deaktiviert werden?</param>
		public void AddScreen(IScreen screen, bool stopAllOther) {
			Log.Log.Add("Add Screen " + screen.ToString().Substring(screen.ToString().LastIndexOf('.') + 1) + " to ScreenManager AND stop all other");
			if (stopAllOther) {
				foreach (IScreen i in Screens) {
					i.SetIsActiv(false);
				}
			}
			Screens.Push(screen);
		}

		/// <summary>
		/// entfernt den obersten Screen
		/// </summary>
		public void RemoveScreen() {
			IScreen s = Screens.Pop();
			Log.Log.Add("Remove Screen " + s.ToString().Substring(s.ToString().LastIndexOf('.') + 1) + " from ScreenManager");
		}

		/// <summary>
		/// entfernt alle Screens
		/// </summary>
		public void RemoveAllScreens() {
			Log.Log.Add("Remove all Screens from ScreenManager");
			Screens.Clear();
		}

		/// <summary>
		/// wechsel zum Aktuellen screen. Alle anderen werden entfernt
		/// </summary>
		/// <param name="screen"></param>
		public void Transfer(IScreen screen) {
			Log.Log.Add("Transfer to Screen " + screen.ToString().Substring(screen.ToString().LastIndexOf('.') + 1) + " in ScreenManager");
			RemoveAllScreens();
			AddScreen(screen);
		}
	}
}
