﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpDX;
using SharpDX_Tutorial01;
using SideProjekt.Komponents;
using SideProjekt.ToXNA.GUI;
using TestingTest.Component;
using TestingTest.ToXNA;
using TestingTest.ToXNA.EnergieSystem;

namespace SideProjekt.ToXNA.Screens {

	/// <summary>
	/// Der eigentliche SpielScreen.
	/// Hier laufen die Monster, stehen die Tower usw.
	/// </summary>
	public class GameScreen : BaseScreen {
		private Level currentLevel;
		private Player player;

		private GUIPanel playerInformationPanel; //alle Informationen über den aktuellen spieler anzeigen
		private Monster selectedMonster;
		private GUIPanel selectedMonsterPanel;
		private GUIPanel selectedTowerPanel;
		private GUIPanel towerBuildPanel; //alle Tower die in dem aktuellem Level gebaut werden können
		private GUIPanel towerSelectedToBuildPanel; //aktuell ausgewählter tower der gebaut werden soll/kann
		 //aktuell ausgewähltes monster welches auf dem Feld läuft

		public GameScreen(Game01 game, ScreenManager manager)
			: base(game, manager) {
			
		}

		#region funktionen und methoden
		public void resetPanels() { 
			playerInformationPanel.SetIsEnable(true);
			selectedMonsterPanel.SetIsEnable(false);
			selectedTowerPanel.SetIsEnable(false);
			towerBuildPanel.SetIsEnable(true);
			towerSelectedToBuildPanel.SetIsEnable(false);
			player.SetNullStuffToBuild();
			selectedMonster = null;

			var a = player.BuildetTowers.Where(x => x.IsSelected).SingleOrDefault();
			if (a != null) {
				a.PerformClick(a.CenterLocation);
			}

			VirtualGamepad.RightMouseIsClicked(false, System.Drawing.Point.Empty);
		}

		public void resetPanels(bool resetTower) {
			playerInformationPanel.SetIsEnable(true);
			selectedMonsterPanel.SetIsEnable(false);
			selectedTowerPanel.SetIsEnable(false);
			towerBuildPanel.SetIsEnable(true);
			towerSelectedToBuildPanel.SetIsEnable(false);
			player.SetNullStuffToBuild();
			selectedMonster = null;

			if (resetTower) {
				var a = player.BuildetTowers.Where(x => x.IsSelected).SingleOrDefault();
				if (a != null) {
					a.PerformClick(a.CenterLocation);
				}
			}

			VirtualGamepad.RightMouseIsClicked(false, System.Drawing.Point.Empty);
		}

		public void disablePanels() {
			playerInformationPanel.SetIsEnable(false);
			selectedMonsterPanel.SetIsEnable(false);
			selectedTowerPanel.SetIsEnable(false);
			towerBuildPanel.SetIsEnable(false);
			towerSelectedToBuildPanel.SetIsEnable(false);
			player.SetNullStuffToBuild();
			selectedMonster = null;

			var a = player.BuildetTowers.Where(x => x.IsSelected).SingleOrDefault();
			if (a != null) {
				a.PerformClick(a.CenterLocation);
			}
		}
		#endregion

		#region XNA

		public override void Draw(Spritebatch batch) {
			if (isActiv && doDraw) {
				base.Draw(batch);
				batch.ClearScreen(Color.Black);

				//currentLevel.DrawAll(batch); //<--I want to Draw the Towers(from Player) under the Monster and there Effekts
				currentLevel.DrawMap(batch);
				player.Draw(batch);
				currentLevel.DrawCurrentWave(batch);

				playerInformationPanel.Draw(batch);
				towerBuildPanel.Draw(batch);
				towerSelectedToBuildPanel.Draw(batch);
				selectedTowerPanel.Draw(batch);
				selectedMonsterPanel.Draw(batch);

				if (player.HasStuffToBuild() && VirtualGamepad.GetMousePoint().X > 0 && VirtualGamepad.GetMousePoint().Y > 0 && VirtualGamepad.GetMousePoint().X < currentLevel.GetMapWidthInPixel() && VirtualGamepad.GetMousePoint().Y < currentLevel.GetMapHeightInPixel()) {
					player.SetLocationForStuffToBuild(new Vector2(VirtualGamepad.GetMousePoint().X  - VirtualGamepad.GetMousePoint().X % currentLevel.GetTileWidth(), VirtualGamepad.GetMousePoint().Y - VirtualGamepad.GetMousePoint().Y % currentLevel.GetTileHeight()));
				}
			}
		}

		public void Initialize() {
			base.Init();
			Log.Log.Add("Init GameScreen");
			currentLevel = Content.Load<Level>("test");
			Content.Save<Level>("test2", currentLevel);
			currentLevel = Content.Load<Level>("test2");

			player = new Player(Transfer.Get<string>("PlayerName"));
			player.SetPlayerAmounts(0, 250, 0, 1);
			player.TowerIsSelected += new EventHandler<PlayerTowerEventArgs>(player_TowerIsSelected);
			player.TowerIsUnSelected += new EventHandler<PlayerTowerEventArgs>(player_TowerIsUnSelected);
			player.TowerWasBuild += new EventHandler<TowerEventArgs>(player_TowerWasBuild);
			player.PlayerHasNoLives += new EventHandler(player_PlayerHasNoLives);
			player.TowerWasDestroyed += new EventHandler<TowerEventArgs>(player_TowerWasDestroyed);

			//test for energy-system
			player.AddAndBuildGenerator(new Generator());
			player.AddAndBuildBridge(new Bridge());
			player.BuildetGenerators[0].AddUnit(player.BuildetBridges[0]);

			playerInformationPanel = new GUIPanel("player information", BorderType.LeftTopBottom, Color.White.Darker(0.80f), new Vector2(775, 10), 235, 90);
			CreatePlayerGUIPanel(player, playerInformationPanel);

			towerBuildPanel = new GUIPanel("towerbuild", BorderType.LeftTopBottom, Color.White.Darker(0.80f), new Vector2(775, playerInformationPanel.Location.Y + playerInformationPanel.Height + 10), 235, 310);
			CreateTowerBuildPanel(currentLevel.Towers, towerBuildPanel);

			towerSelectedToBuildPanel = new GUIPanel("selected tower to build", BorderType.LeftTopBottom, Color.White.Darker(0.80f), new Vector2(775, towerBuildPanel.Location.Y + towerBuildPanel.Height + 10), 235, 300);
			towerSelectedToBuildPanel.SetIsEnable(false);

			selectedTowerPanel = new GUIPanel("selectedtower", BorderType.LeftTopBottom, Color.White.Darker(0.80f), new Vector2(775, playerInformationPanel.Location.Y + playerInformationPanel.Height + 10), 235, 310);
			selectedTowerPanel.SetIsEnable(false);

			selectedMonsterPanel = new GUIPanel("selectedMonster", BorderType.LeftTopBottom, Color.White.Darker(0.80f), new Vector2(775, playerInformationPanel.Location.Y + playerInformationPanel.Height + 10), 235, 310);
			selectedMonsterPanel.SetIsEnable(false);

			this.ObjektManager.AddClickableObjekt(playerInformationPanel);
			this.ObjektManager.AddClickableObjekt(towerBuildPanel);
			this.ObjektManager.AddClickableObjekt(towerSelectedToBuildPanel);
			this.ObjektManager.AddClickableObjekt(selectedTowerPanel);
			this.ObjektManager.AddClickableObjekt(selectedMonsterPanel);

			currentLevel.CurrentWaveChanged += new EventHandler<LevelArgs>(currentLevel_CurrentWaveChanged);
			currentLevel.WaveIsCompleted += new EventHandler<WaveEventArgs>(currentLevel_WaveIsCompleted);
			currentLevel.LevelIsCompleted += new EventHandler(currentLevel_LevelIsCompleted);
			currentLevel.MonsterHasReachedEndOfPath += new EventHandler<MonsterArgs>(currentLevel_MonsterHasReachedEndOfPath);
			currentLevel.MonsterWasKilled += new EventHandler<MonsterArgs>(currentLevel_MonsterWasKilled);
			currentLevel.MonsterWasSendet += new EventHandler<MonsterArgs>(currentLevel_MonsterWasSendet);
			currentLevel.SetCurrentWave(0);
		}

		public void LoadContent() {
		}

		public override void Update(GameTime time) {
			if (isActiv && doUpdate) {
				if (player.HasStuffToBuild() && VirtualGamepad.IsMouseClicked() && VirtualGamepad.GetMousePoint().X > 0 && VirtualGamepad.GetMousePoint().Y > 0 && VirtualGamepad.GetMousePoint().X < currentLevel.GetMapWidthInPixel() && VirtualGamepad.GetMousePoint().Y < currentLevel.GetMapHeightInPixel()) {
					BuildStuffForPlayer();
					VirtualGamepad.MouseIsClicked(false, System.Drawing.Point.Empty);//jaja das ist nicht schön aber, trotzdem ;p
					VirtualGamepad.RightMouseIsClicked(false, System.Drawing.Point.Empty);
				}

				//right click setzt alles wieder zurück. das heißt kein tower ausgewählt und es möchte kein Tower gebaut werden.
				if (VirtualGamepad.IsRightMouseClicked()) {
					resetPanels();
				}

				if (selectedMonster != null) {
					CreateSelectedMonsterPanel(selectedMonster, selectedMonsterPanel);
				}

				base.Update(time);
				player.Update(time);
				currentLevel.Update(time);
			}
		}

		private void BuildStuffForPlayer() {
			Tower tmp = player.GetCurrentStuff<Tower>().Clone(player);
			tmp.IsSelected = false;
			tmp.IsPlaced = true;
			tmp.TowerWasSelled += new EventHandler<TowerSellEventArgs>(tmp_TowerWasSelled);
			tmp.EnergieRecieved += new EventHandler(tmp_EnergieRecieved);
			tmp.SetCurrentWave(currentLevel.currentWave);
			if (player.AddBuildetTower(tmp)) { //can Tower be setted?
				ObjektManager.AddClickableObjekt(tmp);
				CreatePlayerGUIPanel(player, playerInformationPanel);
				Log.Log.Add("Player build the Tower " + tmp.TowerName + " for wave " + tmp.currentWave.Name);
				player.BuildetBridges[0].AddUnit(tmp);
			} else {
				Log.Log.Add("Player can not build the Tower " + tmp.TowerName + " for wave " + tmp.currentWave.Name);
			}
		}
		#endregion XNA

		#region Events

		public void currentLevel_CurrentWaveChanged(object sender, LevelArgs e) {
			Log.Log.Add("current Wave changed: " + e.currentWave.Name);
			foreach (Tower t in player.BuildetTowers) {
				t.SetCurrentWave(e.currentWave);
			}
			CreatePlayerGUIPanel(player, playerInformationPanel);
		}

		//next tower zu Build Event
		private void button_ButtonClickEvent(object sender, ButtonEventArgs e) {
			resetPanels();
			Tower tmp = player.GetCurrentStuff<Tower>();
			if (player.HasStuffToBuild() && tmp.TowerName == e.Comment) {
				player.SetNullStuffToBuild();
				towerSelectedToBuildPanel.SetIsEnable(false);
			} else {
				Tower t = currentLevel.GetTowerByName(e.Comment).Clone(player);
				player.SetStuffToBuild(t, null, null);
				t.SetCurrentWave(currentLevel.currentWave);
				t.IsSelected = true;
				towerSelectedToBuildPanel.SetIsEnable(true);
				CreateSelectedTowerToBuildPanel(t, towerSelectedToBuildPanel);
			}
		}

		//next tower zu Build Event
		private void button_ButtonClickSellEvent(object sender, ButtonEventArgs e) {
			resetPanels();
			Tower tmp = player.BuildetTowers.Where(x => x.currentID.ToString() == e.Comment).First();
			tmp.SellTower();
			player.RemoveTower(tmp);
			ObjektManager.RemoveClickableObjekt(tmp);
			towerBuildPanel.SetIsEnable(true);
			selectedTowerPanel.SetIsEnable(false);
		}

		private void currentLevel_LevelIsCompleted(object sender, EventArgs e) {
			Log.Log.Add("Level is completed");
			Environment.Exit(0);
		}

		private void currentLevel_MonsterHasReachedEndOfPath(object sender, MonsterArgs e) {
			Log.Log.Add("Monster " + e.monster.Name + " has reached the end of Path");
			player.AddPlayerAmounts(0, 0, 0, -1);
			CreatePlayerGUIPanel(player, playerInformationPanel);
			this.ObjektManager.RemoveClickableObjekt(e.monster);

			if (selectedMonster == e.monster) {
				resetPanels();
				selectedMonster = null;
				towerBuildPanel.SetIsEnable(true);
				selectedMonsterPanel.SetIsEnable(false);
			}
		}

		private void currentLevel_MonsterWasKilled(object sender, MonsterArgs e) {
			Log.Log.Add("Monster " + e.monster.Name + " was killed by Player " + e.FromPlayer.Name);
			e.FromPlayer.AddPlayerAmounts(e.Score, e.Gold, e.Energie, 0);
			CreatePlayerGUIPanel(player, playerInformationPanel);
			this.ObjektManager.RemoveClickableObjekt(e.monster);

			if (selectedMonster == e.monster) {
				resetPanels();
			}
		}

		private void currentLevel_MonsterWasSendet(object sender, MonsterArgs e) {
			Log.Log.Add("Monster " + e.monster.Name + " was sendet");
			this.ObjektManager.AddClickableObjekt(e.monster);
			e.monster.MonsterWasClicked += new EventHandler<MonsterArgs>(monster_MonsterWasClicked);
		}

		private void currentLevel_WaveIsCompleted(object sender, WaveEventArgs e) {
			Log.Log.Add("Wave " + e.CurrentWave.Name + " isCompleted");
			player.AddPlayerAmounts(0, e.GoldBonus, e.EnergieBonus, e.LivesBonus);
			CreatePlayerGUIPanel(player, playerInformationPanel);
		}

		private void monster_MonsterWasClicked(object sender, MonsterArgs e) {
			disablePanels();
			CreateSelectedMonsterPanel(e.monster, selectedMonsterPanel);

			playerInformationPanel.SetIsEnable(true);
			selectedMonsterPanel.SetIsEnable(true);
			selectedMonster = e.monster;
		}

		private void player_PlayerHasNoLives(object sender, EventArgs e) {
			Log.Log.Add("Eventtriggert player_PlayerHasNoLives: Name:" + ((Player)sender).Name);
			//nothing to do for the moment
		}

		private void player_TowerIsSelected(object sender, PlayerTowerEventArgs e) {
			resetPanels(false);
			Log.Log.Add("Player " + e.currentPlayer.Name + " select a Tower: " + e.selectedTower.TowerName);
			towerBuildPanel.SetIsEnable(false);
			selectedTowerPanel.SetIsEnable(true);
			CreateSelectedTowerPanel(e.selectedTower, e.currentPlayer, selectedTowerPanel);
		}

		private void player_TowerIsUnSelected(object sender, PlayerTowerEventArgs e) {
			Log.Log.Add("Player " + e.currentPlayer.Name + " Unselect a Tower: " + e.selectedTower.TowerName);
			towerBuildPanel.SetIsEnable(true);
			selectedTowerPanel.SetIsEnable(false);
		}
		private void player_TowerWasBuild(object sender, TowerEventArgs e) {
			Log.Log.Add("Eventtriggert player_TowerWasBuild: Name:" + e.Tower.TowerName + ", player: " + e.Owner.Name);
			//nothing to do. kosten usw. sind ja auch schon alle weg
		}
		private void player_TowerWasDestroyed(object sender, TowerEventArgs e) {
			Log.Log.Add("Eventtriggertplayer_TowerWasDestroyed: Name:" + e.Tower.TowerName + ", player: " + e.Owner.Name);
			//nothing to do for the moment
		}

		private void tmp_TowerWasSelled(object sender, TowerSellEventArgs e) {
			e.Owner.AddPlayerAmounts(0, e.Gold, e.Energie, 0);
			CreatePlayerGUIPanel(player, playerInformationPanel);
		}

		void lvlUpButton_ButtonClickEvent(object sender, ButtonEventArgs e) {
			Tower t = player.GetBuildetTowerByID(e.ButtonName.Replace("btnLvlUp", ""));
			Player owner = t.Owner;

			if (owner.Gold - t.LevelUpGoldCost >= 0) {
				owner.AddPlayerAmounts(0, t.LevelUpGoldCost * -1, 0, 0);
				t.LevelUp();
			}

			CreatePlayerGUIPanel(player, playerInformationPanel);
			CreateSelectedTowerPanel(t, owner, selectedTowerPanel);
		}

		void tmp_EnergieRecieved(object sender, EventArgs e) {
			if (selectedTowerPanel.IsEnable()) {
				CreateSelectedTowerPanel((Tower)sender, player, selectedTowerPanel);
			}
		}
		#endregion Events

		#region PanelCreation

		private void CreatePlayerGUIPanel(Player p, GUIPanel gui) {
			Log.Log.Add("Create Player GUI");

			int widthToNextControl = 5;
			gui.AddObject("NameFlag", new LinkControl("Name:", new Vector2(5, 5), new SpriteFont(10, Color.White)));
			gui.AddObject("Name", new LinkControl(p.Name, new Vector2(5 + 5 + gui.GetElementByName("NameFlag").Width(), 5), new SpriteFont(10, Color.White)));

			gui.AddObject("GoldFlag", new LinkControl("Gold:", new Vector2(5, (float)gui.GetElementByName("NameFlag").GetPosition().Y + (float)gui.GetElementByName("NameFlag").GetPosition().Height + widthToNextControl), new SpriteFont(10, Color.White)));
			int StringWidth = Spritebatch.GetTextDimension(p.Gold.ToString(), new SpriteFont(10, Color.White).Font).Width;
			gui.AddObject("Gold", new LinkControl(p.Gold.ToString(), new Vector2(225 - StringWidth, gui.GetElementByName("GoldFlag").GetPosition().Y), new SpriteFont(10, Color.White)));

			gui.AddObject("EnergieFlag", new LinkControl("Energie:", new Vector2(5, (float)gui.GetElementByName("GoldFlag").GetPosition().Y + (float)gui.GetElementByName("GoldFlag").GetPosition().Height + widthToNextControl), new SpriteFont(10, Color.White)));
			StringWidth = Spritebatch.GetTextDimension(p.Energie.ToString(), new SpriteFont(10, Color.White).Font).Width;
			gui.AddObject("Energie", new LinkControl(p.Energie.ToString(), new Vector2(225 - StringWidth, gui.GetElementByName("EnergieFlag").GetPosition().Y), new SpriteFont(10, Color.White)));

			gui.AddObject("LivesFlag", new LinkControl("Lives:", new Vector2(5, (float)gui.GetElementByName("EnergieFlag").GetPosition().Y + (float)gui.GetElementByName("EnergieFlag").GetPosition().Height + widthToNextControl), new SpriteFont(10, Color.White)));
			StringWidth = Spritebatch.GetTextDimension(p.Lives.ToString(), new SpriteFont(10, Color.White).Font).Width;
			gui.AddObject("Lives", new LinkControl(p.Lives.ToString(), new Vector2(225 - StringWidth, gui.GetElementByName("LivesFlag").GetPosition().Y), new SpriteFont(10, Color.White)));

			gui.AddObject("ScoreFlag", new LinkControl("Score:", new Vector2(5, (float)gui.GetElementByName("LivesFlag").GetPosition().Y + (float)gui.GetElementByName("LivesFlag").GetPosition().Height + widthToNextControl), new SpriteFont(10, Color.White)));
			StringWidth = Spritebatch.GetTextDimension(p.Score.ToString(), new SpriteFont(10, Color.White).Font).Width;
			gui.AddObject("Score", new LinkControl(p.Score.ToString(), new Vector2(225 - StringWidth, gui.GetElementByName("ScoreFlag").GetPosition().Y), new SpriteFont(10, Color.White)));
		}

		private void CreateSelectedMonsterPanel(Monster selectedmonster, GUIPanel gui) {
			gui.AddObject("MonsterGraphic", new ImageControl(selectedmonster.Sprite.CurrentGraphic, new Vector2(10, 10)));

			int widthPositionRight = 225;
			int widthToNextControl = 5;
			Color FontColor = Color.White;

			int stringWidth = Spritebatch.GetTextDimension(selectedmonster.Name, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("NameFlag", new LinkControl("Name:", new Vector2(15 + gui.GetElementByName("MonsterGraphic").Width(), gui.GetElementByName("MonsterGraphic").GetPosition().Y + gui.GetElementByName("MonsterGraphic").Height() - 10), new SpriteFont(10, FontColor)));
			gui.AddObject("Name", new LinkControl(selectedmonster.Name + " " + selectedmonster.Location.ToString(), new Vector2(gui.GetElementByName("NameFlag").GetPosition().X + gui.GetElementByName("NameFlag").Width(), gui.GetElementByName("MonsterGraphic").GetPosition().Y + gui.GetElementByName("MonsterGraphic").Height() - 10), new SpriteFont(10, FontColor)));

			gui.AddObject("LifeFlag", new LinkControl("Life:", new Vector2(10, gui.GetElementByName("NameFlag").GetPosition().Y + gui.GetElementByName("NameFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedmonster.Hitpoints + "/" + selectedmonster.MaxHitpoints, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Life", new LinkControl(selectedmonster.Hitpoints.ToString() + "/" + selectedmonster.MaxHitpoints.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("LifeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("WalkSpeedFlag", new LinkControl("WalkSpeed:", new Vector2(10, gui.GetElementByName("LifeFlag").GetPosition().Y + gui.GetElementByName("LifeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedmonster.Movespeed.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("WalkSpeed", new LinkControl(selectedmonster.Movespeed.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("WalkSpeedFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("ArmorTypeFlag", new LinkControl("Armor:", new Vector2(10, gui.GetElementByName("WalkSpeedFlag").GetPosition().Y + gui.GetElementByName("WalkSpeedFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedmonster.Armor.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Armor", new LinkControl(selectedmonster.Armor.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("ArmorTypeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("ElementStrongFlag", new LinkControl("Stark gegen Element:", new Vector2(10, gui.GetElementByName("ArmorTypeFlag").GetPosition().Y + gui.GetElementByName("ArmorTypeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedmonster.Element.StrongAgainstString, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("ElementStrong", new LinkControl(selectedmonster.Element.StrongAgainstString, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("ElementStrongFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("ElementWeakFlag", new LinkControl("Schwach gegen Element:", new Vector2(10, gui.GetElementByName("ElementStrongFlag").GetPosition().Y + gui.GetElementByName("ElementStrongFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedmonster.Element.WeakAgainstString, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("ElementWeak", new LinkControl(selectedmonster.Element.WeakAgainstString, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("ElementWeakFlag").GetPosition().Y), new SpriteFont(10, FontColor)));
		}

		private void CreateSelectedTowerPanel(Tower selectedTower, Player p, GUIPanel gui) {
			Log.Log.Add("Create Selected Tower GUI");
			gui.ClearObjects();

			gui.AddObject("TowerGraphic", new ImageControl(selectedTower.Graphic, new Vector2(10, 10)));

			int widthPositionRight = 225;
			int widthToNextControl = 3;
			Color FontColor = Color.White;

			int stringWidth = Spritebatch.GetTextDimension(selectedTower.TowerName, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("NameFlag", new LinkControl("Name:", new Vector2(15 + gui.GetElementByName("TowerGraphic").Width(), gui.GetElementByName("TowerGraphic").GetPosition().Y + gui.GetElementByName("TowerGraphic").Height() - 10), new SpriteFont(10, FontColor)));
			gui.AddObject("Name", new LinkControl(selectedTower.TowerName, new Vector2(gui.GetElementByName("NameFlag").GetPosition().X + gui.GetElementByName("NameFlag").Width(), gui.GetElementByName("TowerGraphic").GetPosition().Y + gui.GetElementByName("TowerGraphic").Height() - 10), new SpriteFont(10, FontColor)));

			gui.AddObject("RangeFlag", new LinkControl("Range:", new Vector2(10, gui.GetElementByName("NameFlag").GetPosition().Y + gui.GetElementByName("NameFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.DmgCircleWidth.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Range", new LinkControl(selectedTower.DmgCircleWidth.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("RangeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("DmgFlag", new LinkControl("Dmg:", new Vector2(10, gui.GetElementByName("RangeFlag").GetPosition().Y + gui.GetElementByName("RangeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			string dmgText = "";
			switch (selectedTower.BaseBullet.EffektType) {
				case "NULL": dmgText = selectedTower.DamageString; break;
				case "PoisonEffekt": dmgText = string.Concat(selectedTower.DamageString, " in ", selectedTower.BaseBullet.Time.ToString(), "s"); break;
				case "AoEEffekt": dmgText = string.Concat(selectedTower.DamageString, " with area ", selectedTower.BaseBullet.AreaWidth.ToString()); break;
				case "SlowEffekt": dmgText = string.Concat(selectedTower.DamageString, " slowing ", selectedTower.MountForEffekt * 100f, "%", " for ", selectedTower.BaseBullet.Time.ToString(), "s"); break;
			}
			stringWidth = Spritebatch.GetTextDimension(dmgText, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Dmg", new LinkControl(dmgText, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("DmgFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("TypeFlag", new LinkControl("Type:", new Vector2(10, gui.GetElementByName("DmgFlag").GetPosition().Y + gui.GetElementByName("DmgFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.BaseBullet.EffektType.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Type", new LinkControl(selectedTower.BaseBullet.EffektType.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("TypeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("AtkSpeedFlag", new LinkControl("Schußfrequenz:", new Vector2(10, gui.GetElementByName("TypeFlag").GetPosition().Y + gui.GetElementByName("TypeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.AttackSpeed.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("AtkSpeed", new LinkControl(selectedTower.AttackSpeed.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("AtkSpeedFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("BulletSpeedFlag", new LinkControl("Bulletspeed:", new Vector2(10, gui.GetElementByName("AtkSpeedFlag").GetPosition().Y + gui.GetElementByName("AtkSpeedFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.BaseBullet.FlightSpeed.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("BulletSpeed", new LinkControl(selectedTower.BaseBullet.FlightSpeed.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("BulletSpeedFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("AttackTypeFlag", new LinkControl("Attack:", new Vector2(10, gui.GetElementByName("BulletSpeedFlag").GetPosition().Y + gui.GetElementByName("BulletSpeedFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.Attack.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("AttackType", new LinkControl(selectedTower.Attack.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("AttackTypeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("ElementTypeFlag", new LinkControl("Element:", new Vector2(10, gui.GetElementByName("AttackTypeFlag").GetPosition().Y + gui.GetElementByName("AttackTypeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.Element.ElementType.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("ElementType", new LinkControl(selectedTower.Element.ElementType.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("ElementTypeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("StrongAgainstFlag", new LinkControl("Stark gegen:", new Vector2(10, gui.GetElementByName("ElementTypeFlag").GetPosition().Y + gui.GetElementByName("ElementTypeFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.Element.StrongAgainstString, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("StrongAgainst", new LinkControl(selectedTower.Element.StrongAgainstString, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("StrongAgainstFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("WeakAgainstFlag", new LinkControl("Schwach gegen:", new Vector2(10, gui.GetElementByName("StrongAgainstFlag").GetPosition().Y + gui.GetElementByName("StrongAgainstFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.Element.WeakAgainstString, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("WeakAgainst", new LinkControl(selectedTower.Element.StrongAgainstString, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("WeakAgainstFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("DPSFlag", new LinkControl("DPS:", new Vector2(10, gui.GetElementByName("WeakAgainstFlag").GetPosition().Y + gui.GetElementByName("WeakAgainstFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.DPS.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("DPS", new LinkControl(selectedTower.DPS.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("DPSFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("EnergieFlag", new LinkControl("Energie:", new Vector2(10, gui.GetElementByName("DPSFlag").GetPosition().Y + gui.GetElementByName("DPSFlag").Height() + widthToNextControl), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(Math.Round(selectedTower.GetCurrentEnergie(), 1) + "/" + selectedTower.GetMaxEnergie(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Energie", new LinkControl(Math.Round(selectedTower.GetCurrentEnergie(), 1) + "/" + selectedTower.GetMaxEnergie(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("EnergieFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			ButtonControl button = new ButtonControl("Sell", new Vector2(10, 265), 10f, Color.White, selectedTower.Thumbnail, true, "btnSell" + selectedTower.currentID);
			button.Comment = selectedTower.currentID.ToString();
			button.ButtonClickEvent += new EventHandler<ButtonEventArgs>(button_ButtonClickSellEvent);
			gui.AddObject("Sell", button);

			ButtonControl upgradeButton = new ButtonControl("Upg.", new Vector2(50, 265), 10f, Color.White, selectedTower.Thumbnail, true, "btnUpg" + selectedTower.currentID);
			upgradeButton.Comment = selectedTower.TowerName;
			upgradeButton.ButtonClickEvent += new EventHandler<ButtonEventArgs>(upgradeButton_ButtonClickEvent);
			gui.AddObject("Upg.", upgradeButton);

			ButtonControl lvlUpButton = new ButtonControl("LUP " + (selectedTower.Level + 1).ToString(), new Vector2(90, 265), 9f, Color.White, selectedTower.Thumbnail, true, "btnLvlUp" + selectedTower.currentID);
			lvlUpButton.Comment = selectedTower.TowerName;
			lvlUpButton.ButtonClickEvent += new EventHandler<ButtonEventArgs>(lvlUpButton_ButtonClickEvent);
			gui.AddObject("LvlUp", lvlUpButton);
		}

		private void CreateSelectedTowerToBuildPanel(Tower selectedTower, GUIPanel gui) {
			Log.Log.Add("Create Selected Tower to bild GUI");
			gui.ClearObjects();

			gui.AddObject("TowerGraphic", new ImageControl(selectedTower.Graphic, new Vector2(10, 10)));

			int widthPositionRight = 225;
			Color FontColor = Color.White;

			int stringWidth = Spritebatch.GetTextDimension(selectedTower.TowerName, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("NameFlag", new LinkControl("Name:", new Vector2(15 + gui.GetElementByName("TowerGraphic").Width(), gui.GetElementByName("TowerGraphic").GetPosition().Y + gui.GetElementByName("TowerGraphic").Height() - 10), new SpriteFont(10, FontColor)));
			gui.AddObject("Name", new LinkControl(selectedTower.TowerName, new Vector2(gui.GetElementByName("NameFlag").GetPosition().X + gui.GetElementByName("NameFlag").Width(), gui.GetElementByName("TowerGraphic").GetPosition().Y + gui.GetElementByName("TowerGraphic").Height() - 10), new SpriteFont(10, FontColor)));

			gui.AddObject("RangeFlag", new LinkControl("Range:", new Vector2(10, gui.GetElementByName("NameFlag").GetPosition().Y + gui.GetElementByName("NameFlag").Height() + 10), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.DmgCircleWidth.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Range", new LinkControl(selectedTower.DmgCircleWidth.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("RangeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("DmgFlag", new LinkControl("Dmg:", new Vector2(10, gui.GetElementByName("RangeFlag").GetPosition().Y + gui.GetElementByName("RangeFlag").Height() + 10), new SpriteFont(10, FontColor)));
			string dmgText = "";
			switch (selectedTower.BaseBullet.EffektType) {
				case "NULL": dmgText = selectedTower.DamageString; break;
				case "PoisonEffekt": dmgText = string.Concat(selectedTower.DamageString, " in ", selectedTower.BaseBullet.Time.ToString(), "s"); break;
				case "AoEEffekt": dmgText = string.Concat(selectedTower.DamageString, " with area ", selectedTower.BaseBullet.AreaWidth.ToString()); break;
				case "SlowEffekt": dmgText = string.Concat(selectedTower.DamageString, " slowing ", selectedTower.MountForEffekt * 100f, "%", " for ", selectedTower.BaseBullet.Time.ToString(), "s"); break;
			}
			stringWidth = Spritebatch.GetTextDimension(dmgText, new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Dmg", new LinkControl(dmgText, new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("DmgFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("TypeFlag", new LinkControl("Type:", new Vector2(10, gui.GetElementByName("DmgFlag").GetPosition().Y + gui.GetElementByName("DmgFlag").Height() + 10), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.BaseBullet.EffektType.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("Type", new LinkControl(selectedTower.BaseBullet.EffektType.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("TypeFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("AtkSpeedFlag", new LinkControl("Schußfrequenz:", new Vector2(10, gui.GetElementByName("TypeFlag").GetPosition().Y + gui.GetElementByName("TypeFlag").Height() + 10), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.AttackSpeed.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("AtkSpeed", new LinkControl(selectedTower.AttackSpeed.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("AtkSpeedFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			gui.AddObject("BulletSpeedFlag", new LinkControl("Bulletspeed:", new Vector2(10, gui.GetElementByName("AtkSpeedFlag").GetPosition().Y + gui.GetElementByName("AtkSpeedFlag").Height() + 10), new SpriteFont(10, FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.BaseBullet.FlightSpeed.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("BulletSpeed", new LinkControl(selectedTower.BaseBullet.FlightSpeed.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("BulletSpeedFlag").GetPosition().Y), new SpriteFont(10, FontColor)));

			FontColor = Color.White.Darker(0.3f);
			gui.AddObject("GoldCostFlag", new LinkControl("Goldkosten:", new Vector2(10, gui.GetElementByName("BulletSpeedFlag").GetPosition().Y + gui.GetElementByName("BulletSpeedFlag").Height() + 10), new SpriteFont(10, selectedTower.GoldCost > 0 ? Color.White : FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.GoldCost.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("GoldCost", new LinkControl(selectedTower.GoldCost.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("GoldCostFlag").GetPosition().Y), new SpriteFont(10, selectedTower.GoldCost > 0 ? Color.White : FontColor)));

			gui.AddObject("EnergieCostFlag", new LinkControl("Energiekosten:", new Vector2(10, gui.GetElementByName("GoldCostFlag").GetPosition().Y + gui.GetElementByName("GoldCostFlag").Height() + 2), new SpriteFont(10, selectedTower.EnergieCost > 0 ? Color.White : FontColor)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.EnergieCost.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("EnergieCost", new LinkControl(selectedTower.EnergieCost.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("EnergieCostFlag").GetPosition().Y), new SpriteFont(10, selectedTower.EnergieCost > 0 ? Color.White : FontColor)));

			gui.AddObject("DPSFlag", new LinkControl("DPS:", new Vector2(10, gui.GetElementByName("EnergieCostFlag").GetPosition().Y + gui.GetElementByName("EnergieCostFlag").Height() + 10), new SpriteFont(10, Color.White)));
			stringWidth = Spritebatch.GetTextDimension(selectedTower.DPS.ToString(), new SpriteFont(10, FontColor).Font).Width;
			gui.AddObject("DPS", new LinkControl(selectedTower.DPS.ToString(), new Vector2(widthPositionRight - stringWidth, gui.GetElementByName("DPSFlag").GetPosition().Y), new SpriteFont(10, Color.White)));
		}

		private void CreateTowerBuildPanel(List<Tower> avaiableTowers, GUIPanel gui) {
			Log.Log.Add("Create Tower Build GUI");
			int offsetX = 7;
			int offsetY = 7;

			int x = offsetX;
			int y = offsetY;

			foreach (Tower t in avaiableTowers) {
				ButtonControl button = new ButtonControl("", new Vector2(x, y), 10f, Color.White, t.Thumbnail, true, "btnCreate" + t.TowerName);
				button.Comment = t.TowerName;
				button.ButtonClickEvent += new EventHandler<ButtonEventArgs>(button_ButtonClickEvent);
				gui.AddObject(t.TowerName, button);

				if (x + button.Width() + offsetX > gui.Width) {
					x = offsetX;
					y += button.Height() + offsetY;
				} else {
					x += button.Width() + offsetX;
				}
			}
		}
		//todo finish upgrade system
		private void upgradeButton_ButtonClickEvent(object sender, ButtonEventArgs e) {
			Tower t = player.GetBuildetTowerByID(e.ButtonName.Replace("btnUpg", ""));
			Player owner = t.Owner;

			//only for Test, will it create a auto better tower. This Upgrade will Cost always 50 Gold.
			Tower newT = t.Clone(t.Owner);
			newT.SetTowerDamage(t.Damage + 1, t.ThrowCount + 1, t.ThrowDamage + 1);
			newT.SetTowerCosts(t.GoldCost + 50, t.EnergieCost);
			t.AddUpgradeableTower(newT);

			Tower upgradeTower = t.GetUpgradedTower(0);

			if (owner.Gold - upgradeTower.GoldCost >= 0 &&
				owner.Energie - upgradeTower.EnergieCost >= 0) {
				t.Upgrade(0);
				owner.AddPlayerAmounts(0, upgradeTower.GoldCost * -1, upgradeTower.EnergieCost * -1, 0);
				CreateSelectedTowerPanel(t, owner, selectedTowerPanel);
				CreatePlayerGUIPanel(owner, playerInformationPanel);
			}

			
		}
		#endregion PanelCreation
	}
}