﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using TestingTest.Component;

namespace SideProjekt.Interfaces {
	public interface IUpdateable {

		void Update(GameTime gameTime);
	}
}
