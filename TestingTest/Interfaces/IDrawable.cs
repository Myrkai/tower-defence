﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using TestingTest.Component;

namespace SideProjekt.Interfaces {
	/// <summary>
	/// Zeichenbares Objekt auf dem Bildschirm,
	/// </summary>
	public interface IDrawable {

		void Draw(Spritebatch batch);
	}
}
