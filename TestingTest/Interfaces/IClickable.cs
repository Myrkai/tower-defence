﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SideProjekt.Komponents;
using SharpDX;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.Interfaces {
	/// <summary>
	/// Alle Klickbaren Objekte auf dem Bildschirm.
	/// Verwaltet über den ObjektManager pro Screen.
	/// </summary>
	public interface IClickable {

		void PerformClick(Vector2 position);
		Rectangle GetPosition();
		bool IsEnable();
		bool IsClickable();
		void SetIsEnable(bool enable);
	}
}
