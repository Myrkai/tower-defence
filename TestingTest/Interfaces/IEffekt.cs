﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingTest.Component;
using SideProjekt.ToXNA;

namespace TestingTest.Interfaces {
	/// <summary>
	/// Effekt welches eine Kugel(Bullet) erhalten kann. z.B. Gifteffekt/Verlangsamung.
	/// Jede Kugel hat dabei Platz für genau ein IEffekt-Objekt welches dann auf das Monster übertragen wird.
	/// </summary>
	public interface IEffekt {
		void SetMonster(Monster m);
		void SetMonsterAndPlayer(Monster m, Player p);
		void SetWave(Wave w);
		void Update(GameTime time);
		void Draw(Spritebatch batch);
		event EventHandler HasEnded;
		void ResetDuration();
		bool IsActiv();
		IEffekt Clone(Monster m, Wave w, Player p);
	}
}
