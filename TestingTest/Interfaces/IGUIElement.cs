﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using TestingTest.Component;
using SharpDX;

namespace SideProjekt.Interfaces {
	/// <summary>
	/// Schnittstelle für die Interaktionsobjekte mit dem Spieler.
	/// Z.B. Button, Textbox usw.
	/// </summary>
	public interface IGUIElement : IDrawable, IUpdateable, IClickable {
		void DrawToPosition(Spritebatch batch, Vector2 position);
		int Width();
		int Height();
	}
}
