﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.Interfaces {
	public interface IScreen : IDrawable, IUpdateable {

		bool IsActiv(); //ist das Fenstergerade aktiv
		bool DoDraw(); // soll das aktive Fenster seine DrawMethoden aufführen.
		bool DoUpdate(); //soll das aktive Fenster seine UpdateMethoden ausführen

		void SetIsActiv(bool activ); //ist das Fenstergerade aktiv
		void SetDoDraw(bool draw); // soll das aktive Fenster seine DrawMethoden aufführen.
		void SetDoUpdate(bool update); //soll das aktive Fenster seine UpdateMethoden ausführen
	}
}
