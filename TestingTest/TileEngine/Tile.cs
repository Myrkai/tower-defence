﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using TestingTest.Component;
using SharpDX;
using Rectangle = SharpDX.Rectangle;

namespace SideProjekt.TileEngine {
	/// <summary>
	/// Einzelnes Qudarte vom TileSet(Sammlung von vielen einzelnen Bildern in einem Bild)
	/// </summary>
	public static class Tile {
		public static Texture2D TileSetTexture;

		static public int TileWidth = 32;
		static public int TileHeight = 32;
		static public int TileStepX = 32;
		static public int TileStepY = 32;
		static public int OddRowXOffset = 0;

		static public Vector2 originPoint = new Vector2(19, 39);

		static public Rectangle GetSourceRectangle(int tileIndex) {
			int tileY = tileIndex / (TileSetTexture.Width / TileWidth);
			int tileX = tileIndex % (TileSetTexture.Width / TileWidth);

			return new Rectangle(tileX * TileWidth, tileY * TileHeight, TileWidth, TileHeight);
		}
	}
}
