﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SharpDX;

namespace SideProjekt.TileEngine {
	/// <summary>
	/// Stellt die Momentane Blickrichtung auf der Karte da.
	/// Die Karte bewegt um die Kamera herum.
	/// </summary>
	public static class Camera {
		public static Vector2 Location = Vector2.Zero;
	}
}
