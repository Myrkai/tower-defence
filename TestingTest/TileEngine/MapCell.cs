﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using SharpDX;

namespace SideProjekt.TileEngine {
	/// <summary>
	/// einzelnes Quadrat auf der Karte
	/// </summary>
	public class MapCell {
		public int TileID { get; set; }
		public Vector2 Position { get; set; } 

		public MapCell(int tileID, Vector2 position) {
			TileID = tileID;
			Position = position;
		}

		public MapCell() {
			TileID = 0;
			Position = Vector2.Zero;
		}
	}
}
