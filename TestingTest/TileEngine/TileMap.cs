﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SideProjekt.Komponents;
using System.Drawing;
using SideProjekt.ToXNA;
using TestingTest.Component;
using Rectangle = SharpDX.Rectangle;
using Color = SharpDX.Color;
using SharpDX;

namespace SideProjekt.TileEngine {
	public class MapRow {
		public List<MapCell> Columns = new List<MapCell>();
	}

	/// <summary>
	/// Graphice Karte. Darstellung über Tiles. Enthält auch die Pfade der Monster.
	/// Eher schlecht als Recht umgesetzt, wirklich nur eine einfache Karte mit einer Ebene.
	/// </summary>
	public class TileMap {
		public List<MapRow> Rows = new List<MapRow>();
		public int MapWidth = 24;
		public int MapHeight = 25;
		public List<Path> Paths = new List<Path>(); //es können mehere Wege für die monster geben auf einer Karte
		public int TileWidth = 32;
		public int TileHeight = 32;
		public string TileSetAsset;

		public TileMap(string tileSetAsset) {
			this.TileSetAsset = tileSetAsset;
			Tile.TileSetTexture = Content.Load<Texture2D>(this.TileSetAsset);
						
			for (int y = 0; y < MapHeight; y++) {
				MapRow thisRow = new MapRow();
				for (int x = 0; x < MapWidth; x++) {
					thisRow.Columns.Add(new MapCell(0, new Vector2(x,y)));
				}
				Rows.Add(thisRow);
			}

			// Create Sample Map Data
			Rows[0].Columns[6].TileID = 1;
			Rows[1].Columns[6].TileID = 1;
			Rows[2].Columns[6].TileID = 1;
			Rows[3].Columns[6].TileID = 1;
			Rows[4].Columns[6].TileID = 1;
			Rows[5].Columns[6].TileID = 1;
			Rows[6].Columns[6].TileID = 1;
			Rows[7].Columns[6].TileID = 1;
			Rows[8].Columns[6].TileID = 1;
			Rows[9].Columns[6].TileID = 1;
			Rows[10].Columns[6].TileID = 1;
			Rows[10].Columns[5].TileID = 1;
			Rows[10].Columns[4].TileID = 1;
			Rows[10].Columns[3].TileID = 1;
			Rows[10].Columns[2].TileID = 1;
			Rows[9].Columns[2].TileID = 1;
			Rows[8].Columns[2].TileID = 1;
			Rows[7].Columns[2].TileID = 1;
			Rows[6].Columns[2].TileID = 1;
			Rows[6].Columns[3].TileID = 1;
			Rows[6].Columns[4].TileID = 1;
			Rows[6].Columns[5].TileID = 1;
			//Rows[6].Columns[6].TileID = 1;
			Rows[6].Columns[7].TileID = 1;
			Rows[6].Columns[8].TileID = 1;
			Rows[6].Columns[9].TileID = 1;
			Rows[6].Columns[10].TileID = 1;
			Rows[7].Columns[10].TileID = 1;
			Rows[8].Columns[10].TileID = 1;
			Rows[9].Columns[10].TileID = 1;
			Rows[10].Columns[10].TileID = 1;
			Rows[11].Columns[10].TileID = 1;
			Rows[12].Columns[10].TileID = 1;
			Rows[13].Columns[10].TileID = 1;
			Rows[14].Columns[10].TileID = 1;
			Rows[15].Columns[10].TileID = 1;
			Rows[16].Columns[10].TileID = 1;
			Rows[17].Columns[10].TileID = 1;
			Rows[18].Columns[10].TileID = 1;
			Rows[19].Columns[10].TileID = 1;
			Rows[20].Columns[10].TileID = 1;
			Rows[21].Columns[10].TileID = 1;
			Rows[22].Columns[10].TileID = 1;
			Rows[23].Columns[10].TileID = 1;
			// End Create Sample Map Data

			Paths.Add(new Path());
			//set custom Path
			Paths[0].AddNewNode(Rows[0].Columns[6]);
			Paths[0].AddNewNode(Rows[1].Columns[6]);
			Paths[0].AddNewNode(Rows[2].Columns[6]);
			Paths[0].AddNewNode(Rows[3].Columns[6]);
			Paths[0].AddNewNode(Rows[4].Columns[6]);
			Paths[0].AddNewNode(Rows[5].Columns[6]);
			Paths[0].AddNewNode(Rows[6].Columns[6]);
			Paths[0].AddNewNode(Rows[7].Columns[6]);
			Paths[0].AddNewNode(Rows[8].Columns[6]);
			Paths[0].AddNewNode(Rows[9].Columns[6]);
			Paths[0].AddNewNode(Rows[10].Columns[6]);
			Paths[0].AddNewNode(Rows[10].Columns[5]);
			Paths[0].AddNewNode(Rows[10].Columns[4]);
			Paths[0].AddNewNode(Rows[10].Columns[3]);
			Paths[0].AddNewNode(Rows[10].Columns[2]);
			Paths[0].AddNewNode(Rows[9].Columns[2]);
			Paths[0].AddNewNode(Rows[8].Columns[2]);
			Paths[0].AddNewNode(Rows[7].Columns[2]);
			Paths[0].AddNewNode(Rows[6].Columns[2]);
			Paths[0].AddNewNode(Rows[6].Columns[3]);
			Paths[0].AddNewNode(Rows[6].Columns[4]);
			Paths[0].AddNewNode(Rows[6].Columns[5]);
			Paths[0].AddNewNode(Rows[6].Columns[6]);
			Paths[0].AddNewNode(Rows[6].Columns[7]);
			Paths[0].AddNewNode(Rows[6].Columns[8]);
			Paths[0].AddNewNode(Rows[6].Columns[9]);
			Paths[0].AddNewNode(Rows[6].Columns[10]);
			Paths[0].AddNewNode(Rows[7].Columns[10]);
			Paths[0].AddNewNode(Rows[8].Columns[10]);
			Paths[0].AddNewNode(Rows[9].Columns[10]);
			Paths[0].AddNewNode(Rows[10].Columns[10]);
			Paths[0].AddNewNode(Rows[11].Columns[10]);
			Paths[0].AddNewNode(Rows[12].Columns[10]);
			Paths[0].AddNewNode(Rows[13].Columns[10]);
			Paths[0].AddNewNode(Rows[14].Columns[10]);
			Paths[0].AddNewNode(Rows[15].Columns[10]);
			Paths[0].AddNewNode(Rows[16].Columns[10]);
			Paths[0].AddNewNode(Rows[17].Columns[10]);
			Paths[0].AddNewNode(Rows[18].Columns[10]);
			Paths[0].AddNewNode(Rows[19].Columns[10]);
			Paths[0].AddNewNode(Rows[20].Columns[10]);
			Paths[0].AddNewNode(Rows[21].Columns[10]);
			Paths[0].AddNewNode(Rows[22].Columns[10]);
			Paths[0].AddNewNode(Rows[23].Columns[10]);
			//end set custom Path
		}

		public void Draw(Spritebatch batch) {
			int squaresAcross = MapWidth;
			int squaresDown = MapHeight;
			int baseOffsetX = 0;
			int baseOffsetY = 0;
			
			Vector2 firstSquare = new Vector2(Camera.Location.X / Tile.TileStepX, Camera.Location.Y / Tile.TileStepY);
			int firstX = (int)firstSquare.X;
			int firstY = (int)firstSquare.Y;

			Vector2 squareOffset = new Vector2(Camera.Location.X % Tile.TileStepX, Camera.Location.Y % Tile.TileStepY);
			int offsetX = (int)squareOffset.X;
			int offsetY = (int)squareOffset.Y;

			for (int y = 0; y < squaresDown; y++) {

				int rowOffset = 0;
				if ((firstY + y) % 2 == 1)
					rowOffset = Tile.OddRowXOffset;

				for (int x = 0; x < squaresAcross; x++) {
					batch.Draw(
							Tile.TileSetTexture,
							Tile.GetSourceRectangle(Rows[y + firstY].Columns[x + firstX].TileID),
							new Rectangle(
								(x * Tile.TileStepX) - offsetX + rowOffset + baseOffsetX,
								(y * Tile.TileStepY) - offsetY + baseOffsetY,
								Tile.TileWidth, Tile.TileHeight),
							Color.White);
				}
			}

		}

		public void SwitchMapAndReset(int tileWidth, int tileHeight, string tileSetAsset) {
			this.TileWidth = tileWidth;
			this.TileHeight = tileHeight;
			this.TileSetAsset = tileSetAsset;
			Tile.TileHeight = TileHeight;
			Tile.TileWidth = TileWidth;
			Tile.TileStepX = TileWidth;
			Tile.TileStepY = TileHeight;

			Tile.TileSetTexture = Content.Load<Texture2D>(this.TileSetAsset);
			Rows.Clear();
			Paths.Clear();
		}

		public void Update(GameTime time) {
			
		}

		public int TotalMapWidth { get { return MapWidth * TileWidth; } }
		public int TotalMapHeight { get { return MapHeight * TileHeight; } }
	}
}
