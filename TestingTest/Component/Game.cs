﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Windows;
using AlphaMode = SharpDX.Direct2D1.AlphaMode;
using Bitmap = SharpDX.Direct2D1.Bitmap;
using PixelFormat = SharpDX.Direct2D1.PixelFormat;
using Device1 = SharpDX.Direct3D11.Device;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using TestingTest;
using TestingTest.Component;
#endregion

namespace SharpDX {
	/// <summary>
	/// Basis Klasse für alle Spiele. 
	/// Über nimmt die vollständige DirectX Initialisierung.
	/// Zusätzlich enthält sie 2 Konvertierungsmöglichkeiten von Bitmaps.
	/// </summary>
	public abstract class Game : IDisposable {
		public static int _drawXoffset = 0;
		public static int _drawYoffset = 0;
		public static System.Drawing.Rectangle ViewPort;

		public RenderForm renderWindow;

		private RenderTarget d2dRenderTarget;
		public RenderTarget D2DRenderTarget { get { return d2dRenderTarget; } }
		private SharpDX.Direct2D1.Factory d2dFactory;

		int fps = 0;
		int lastFps = 0;
		public int FPS { get { return lastFps; } }
		Spritebatch spriteBatch;

		public Game(string title, int width, int height) {
			renderWindow = new RenderForm(title);
			renderWindow.ClientSize = new System.Drawing.Size(width, height);
			renderWindow.MaximizeBox = false;
			renderWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
	
			d2dFactory = new SharpDX.Direct2D1.Factory(FactoryType.MultiThreaded, DebugLevel.Error);
			d2dFactory.ReloadSystemMetrics();

			HwndRenderTargetProperties properties = new HwndRenderTargetProperties();
			properties.Hwnd = renderWindow.Handle;
			properties.PixelSize = new Size2(renderWindow.ClientSize.Width, renderWindow.ClientSize.Height);
			properties.PresentOptions = PresentOptions.Immediately; //war vorher none auf damit auf 60FPS

			d2dRenderTarget = new WindowRenderTarget(d2dFactory, new RenderTargetProperties(new PixelFormat(SharpDX.DXGI.Format.B8G8R8A8_UNorm, AlphaMode.Ignore)), properties);
			d2dRenderTarget.AntialiasMode = AntialiasMode.Aliased;
			d2dRenderTarget.TextAntialiasMode = TextAntialiasMode.Cleartype;
			d2dRenderTarget.DotsPerInch = new Size2F(96f, 96f);

			ViewPort = new System.Drawing.Rectangle(0, 0, renderWindow.Width, renderWindow.Size.Height);

			spriteBatch = new Spritebatch(this.d2dRenderTarget);
		}

		public virtual void Initialize() {

		}

		public virtual void LoadContent() {

		}

		public virtual void UnloadContent() {

		}

		float timer = 0f;
		public virtual void Update(GameTime gameTime) {
			fps += 1;
			timer += (float)gameTime.TimeDiff.TotalSeconds;
			if (timer > 1f) {
				lastFps = fps;
				fps = 0;
				timer = 0f;
			}
		}

		public virtual void Draw(Spritebatch batch) {
			
		}

		private void BeginFrame() {
			d2dRenderTarget.BeginDraw();
		}

		private void EndFrame() {
			d2dRenderTarget.EndDraw();
		}

		public void Run() {
			Initialize();
			LoadContent();

			GameTime gameTime = new GameTime();
			RenderLoop.Run(renderWindow, () => {
				gameTime.GetNextStep();
				Update(gameTime);
				//BeginFrame();
				Draw(spriteBatch);
				//EndFrame();
			});

			UnloadContent();
		}

		public void Dispose() {
			renderWindow.Dispose();
			d2dFactory.Dispose();
			d2dRenderTarget.Dispose();
			spriteBatch.Dispose();
		}

		/// <summary>
		/// Loads a Direct2D Bitmap from a file using System.Drawing.Image.FromFile(...)
		/// </summary>
		/// <param name="renderTarget">The render target.</param>
		/// <param name="file">The file.</param>
		/// <returns>A D2D1 Bitmap</returns>
		public static SharpDX.Direct2D1.Bitmap BitmapLoadFromFile(RenderTarget renderTarget, string fileName) {
			// Loads from file using System.Drawing.Image
			MemoryStream steam;
			MemoryStream ms = new MemoryStream();
				using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read)) {
					byte[] bytes = new byte[file.Length];
					file.Read(bytes, 0, (int)file.Length);
					ms.Write(bytes, 0, (int)file.Length);
				}
			ms.Position = 0;
			steam = ms;

			using (var bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(steam)) {
				var sourceArea = new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height);
				var bitmapProperties = new BitmapProperties(new SharpDX.Direct2D1.PixelFormat(Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied));
				var size = new Size2(bitmap.Width, bitmap.Height);

				// Transform pixels from BGRA to RGBA
				int stride = bitmap.Width * sizeof(int);
				using (var tempStream = new DataStream(bitmap.Height * stride, true, true)) {
					// Lock System.Drawing.Bitmap
					var bitmapData = bitmap.LockBits(sourceArea, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

					// Convert all pixels 
					for (int y = 0; y < bitmap.Height; y++) {
						int offset = bitmapData.Stride * y;
						for (int x = 0; x < bitmap.Width; x++) {
							// Not optimized 
							byte B = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte G = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte R = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte A = Marshal.ReadByte(bitmapData.Scan0, offset++);
							//int rgba = R | (G << 8) | (B << 16) | (A << 24);
							int rgba = B | (G << 8) | (R << 16) | (A << 24);
							tempStream.Write(rgba);
						}

					}
					bitmap.UnlockBits(bitmapData);
					tempStream.Position = 0;

					ms.Close();
					ms.Dispose();
					return new SharpDX.Direct2D1.Bitmap(renderTarget, size, tempStream, stride, bitmapProperties);
				}
			}
		}

		public static SharpDX.Direct2D1.Bitmap BitmapToBitmap(RenderTarget renderTarget, System.Drawing.Bitmap bmp) {
			using (var bitmap = bmp) {
				var sourceArea = new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height);
				var bitmapProperties = new BitmapProperties(new SharpDX.Direct2D1.PixelFormat(Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied));
				var size = new Size2(bitmap.Width, bitmap.Height);

				// Transform pixels from BGRA to RGBA
				int stride = bitmap.Width * sizeof(int);
				using (var tempStream = new DataStream(bitmap.Height * stride, true, true)) {
					// Lock System.Drawing.Bitmap
					var bitmapData = bitmap.LockBits(sourceArea, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

					// Convert all pixels 
					for (int y = 0; y < bitmap.Height; y++) {
						int offset = bitmapData.Stride * y;
						for (int x = 0; x < bitmap.Width; x++) {
							// Not optimized 
							byte B = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte G = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte R = Marshal.ReadByte(bitmapData.Scan0, offset++);
							byte A = Marshal.ReadByte(bitmapData.Scan0, offset++);
							//int rgba = R | (G << 8) | (B << 16) | (A << 24);
							int rgba = B | (G << 8) | (R << 16) | (A << 24);
							tempStream.Write(rgba);
						}

					}
					bitmap.UnlockBits(bitmapData);
					tempStream.Position = 0;

					return new SharpDX.Direct2D1.Bitmap(renderTarget, size, tempStream, stride, bitmapProperties);
				}
			}
		}
	}
}