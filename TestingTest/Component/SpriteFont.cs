﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;

namespace TestingTest.Component {
	/// <summary>
	/// Schrift die verwendet wird für die Darstellung auf dem Monitor.
	/// </summary>
	public class SpriteFont {
		public string FontType = "Arial";
		public SharpDX.DirectWrite.FontWeight Weight = SharpDX.DirectWrite.FontWeight.Bold;
		public SharpDX.DirectWrite.FontStyle Style = SharpDX.DirectWrite.FontStyle.Normal;
		public SharpDX.DirectWrite.FontStretch Strech = SharpDX.DirectWrite.FontStretch.Normal;
		public float Height = 12f;
		public SharpDX.Color Color = SharpDX.Color.White;

		public SpriteFont() { 
		
		}

		public SpriteFont(float p, SharpDX.Color color) {
			this.Height = p;
			this.Color = color;
		}

		public static SpriteFont GetFont(float height) {
			SpriteFont font = new SpriteFont();
			font.Height = height;

			return font;
		}

		public static SpriteFont GetFont(float height, Color c) {
			SpriteFont font = new SpriteFont();
			font.Height = height;
			font.Color = c;

			return font;
		}

		public SpriteFont Font { get { return this; } }
	}
}
