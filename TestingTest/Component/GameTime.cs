﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingTest.Component {
	/// <summary>
	/// Der Herzschlag eines jeden Spiel.
	/// Stellt die Spielzeit dar und gibt nur an wieviel Zeit seit dem letzten durchlauf(herzschlag) vergangen ist.
	/// </summary>
	public class GameTime {
		private DateTime? lastTimeStap;
		private DateTime? Now;
		public TimeSpan TimeDiff { get; private set; }
		public float TotalSecondsFromStart { get; private set; }

		public void GetNextStep() {
			if (lastTimeStap == null) {
				lastTimeStap = DateTime.Now;
			}

			if (Now == null) {
				Now = DateTime.Now;
			}

			if (TotalSecondsFromStart == null) {
				TotalSecondsFromStart = 0f;
			}

			TimeDiff = (DateTime)Now - (DateTime)lastTimeStap;
			lastTimeStap = Now;
			Now = DateTime.Now;

			TotalSecondsFromStart += (float)this.TimeDiff.TotalSeconds;
		}
	}
}
