﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct2D1;
using SharpDX;
namespace TestingTest.Component {
	/// <summary>
	/// einzige Schnittstelle um Texte und Bilder auf dem Bildschirm zu zeichnen.
	/// </summary>
	public class Spritebatch {

		public static RenderTarget renderTarget;
		static SharpDX.DirectWrite.Factory fontFactory;
		public RenderTarget RenderTarget { get { return renderTarget; } }

		public Spritebatch(RenderTarget d2dRenderTarget) {
			renderTarget = d2dRenderTarget;
			fontFactory = new SharpDX.DirectWrite.Factory();
		}

		public void BeginScene() {
			renderTarget.BeginDraw();
		}

		public void EndScene() {
			renderTarget.EndDraw();
		}

		public void Clear(Color color) {
			renderTarget.Clear(color);
		}

		public void DrawText(string text, SpriteFont font, Vector2 position) {
			using (SharpDX.DirectWrite.TextFormat format = new SharpDX.DirectWrite.TextFormat(fontFactory, font.FontType, font.Weight, font.Style, font.Strech, font.Height)) {
				using (SolidColorBrush brush = new SolidColorBrush(renderTarget, font.Color)) {
					renderTarget.DrawText(text, format, new RectangleF(position.X, position.Y, 1000, 200), brush);
				}
			}
		}

		public void DrawText(string text, SpriteFont font, Vector2 position, float opacity) {
			using (SharpDX.DirectWrite.TextFormat format = new SharpDX.DirectWrite.TextFormat(fontFactory, font.FontType, font.Weight, font.Style, font.Strech, font.Height)) {
				using (SolidColorBrush brush = new SolidColorBrush(renderTarget, font.Color)) {
					brush.Opacity = opacity;
					renderTarget.DrawText(text, format, new RectangleF(position.X, position.Y, 1000, 200), brush);
				}
			}
		}

		public static Rectangle GetTextDimension(string text, SpriteFont font) {
			SharpDX.DirectWrite.TextFormat format = new SharpDX.DirectWrite.TextFormat(fontFactory, font.FontType, font.Weight, font.Style, font.Strech, font.Height);
			SharpDX.DirectWrite.TextLayout l = new SharpDX.DirectWrite.TextLayout(fontFactory, text, format, 1000, 2000);
			Rectangle a = new Rectangle(0, 0, (int)l.Metrics.Width, (int)l.Metrics.Height);
			l.Dispose();
			format.Dispose();
			return a;
		}

		public void Draw(Texture2D texture, Rectangle source, Rectangle target, Color color) {
			renderTarget.DrawBitmap(texture.Texture, new RectangleF(target.X, target.Y, target.Width, target.Height), 1f, BitmapInterpolationMode.Linear, new RectangleF(source.X, source.Y, source.Width, source.Height));
		}

		public void Draw(Texture2D texture, Rectangle target, Color color) {
			renderTarget.DrawBitmap(texture.Texture, new RectangleF(target.X, target.Y, target.Width, target.Height), 1f, BitmapInterpolationMode.Linear, new RectangleF(0, 0, texture.Texture.Size.Width, texture.Texture.Size.Height));
		}

		public void Draw(Texture2D texture, RectangleF source, RectangleF target, Color color) {
			renderTarget.DrawBitmap(texture.Texture, target, 1f, BitmapInterpolationMode.Linear, source);
		}

		public void Draw(Texture2D texture, RectangleF target, Color color) {
			renderTarget.DrawBitmap(texture.Texture, target, 1f, BitmapInterpolationMode.Linear, new RectangleF(0, 0, texture.Texture.Size.Width, texture.Texture.Size.Height));
		}

		public void Draw(Texture2D texture, Vector2 target, Color color) {
			renderTarget.DrawBitmap(texture.Texture, new RectangleF(target.X, target.Y, texture.Texture.Size.Width, texture.Texture.Size.Height), 1f, BitmapInterpolationMode.Linear, new RectangleF(0, 0, texture.Texture.Size.Width, texture.Texture.Size.Height));
		}

		internal void ClearScreen(Color color) {
			this.Clear(color);
		}

		/// <summary>
		/// Zeichnet einen Kreis
		/// </summary>
		/// <param name="p">Kreisdurchmesser</param>
		/// <param name="color">Farbe des Kreises</param>
		/// <param name="vector2">Center Point</param>
		internal void DrawCircle(int p, Color color, Vector2 vector2) {
			SolidColorBrush brush = new SolidColorBrush(renderTarget, new Color4(color.R, color.G, color.B, color.A));
			renderTarget.DrawEllipse(new Ellipse(vector2, p / 2, p / 2), brush);
			brush.Dispose();
		}

		public void DrawLine(Vector2 start, Vector2 end, float width, Color color) {
			SolidColorBrush brush = new SolidColorBrush(renderTarget, new Color4(color.R, color.G, color.B, color.A));
			renderTarget.DrawLine(start, end, brush, width);
			brush.Dispose();
		}

		internal void DrawString(string Text, Point point, SpriteFont Font) {
			this.DrawText(Text, Font, new Vector2(point.X, point.Y));
		}

		public void DrawSinus(Vector2 location, float width) {
			float weite = 50f; //amplitudenweite/-intervall
			float höhe = 60f;  //amplitudenhöhe
			for (float x = 0f; location.X + x * weite < location.X + width; x += 0.1f) {

				DrawText("A", SpriteFont.GetFont(10f), new Vector2(location.X + x * weite, location.Y + (float)Math.Sin(x) * höhe));
			}
		}

		public void Dispose() {
			fontFactory.Dispose();
		}
	}
}
