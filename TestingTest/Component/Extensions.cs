﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using TestingTest.Component;
using Color = SharpDX.Color;
using SharpDX;

namespace SideProjekt.Komponents {
	public static class Extensions {
		public static List<string> EnumToList<T>() {
			return Enum.GetValues(typeof(T)).Cast<T>().Select(x => x.ToString()).ToList();
		}

		public static T ToEnum<T>(this string s) {
			return (T)Enum.Parse(typeof(T), s);
		}

		//klappt nicht
		public static List<T> ToList<T>(this Object o) {
			return new List<T>() { (T)o };
		}

		/// <summary>
		/// // make red 50% lighter:
		///	Color.Red.Lerp( Color.White, 0.5 );
		///	// make red 75% darker:
		///	Color.Red.Lerp( Color.Black, 0.75 );
		///	// make white 10% bluer:
		///	Color.White.Lerp( Color.Blue, 0.1 );
		/// </summary>
		/// <param name="colour"></param>
		/// <param name="to">auf welche Farbe soll geändert werden</param>
		/// <param name="amount">um wieviel Prozent soll sich diese Farbe ändern</param>
		/// <returns></returns>
		public static Color Lerp(this Color colour, Color to, float amount) {
			// start colours as lerp-able floats
			float sr = colour.R, sg = colour.G, sb = colour.B;

			// end colours as lerp-able floats
			float er = to.R, eg = to.G, eb = to.B;

			// lerp the colours to get the difference
			byte r = (byte)sr.Lerp(er, amount),
				 g = (byte)sg.Lerp(eg, amount),
				 b = (byte)sb.Lerp(eb, amount);

			// return the new colour
			return new Color(r, g, b);
		}

		public static float Lerp(this float start, float end, float amount) {
			float difference = end - start;
			float adjusted = difference * amount;
			return start + adjusted;
		}

		public static Color Darker(this Color c, float amount) {
			return c.Lerp(Color.Black, amount);
		}

		public static Color Lighter(this Color c, float amount) {
			return c.Lerp(Color.White, amount);
		}

		public static Color SetAlpha(this Color c, int amount) {
			return new Color(amount, c.R, c.G, c.B);
		}

		public static Texture2D CreateTexture(int width, int height, Color color) {
			System.Drawing.Bitmap b = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					b.SetPixel(x, y, System.Drawing.Color.FromArgb(color.R, color.G, color.B));
				}
			}

			return new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, b));
		}

		public static Texture2D CreateTextureWidthBoarder(int width, int height, Color color, int boarderWidth, Color borderColor) {
			System.Drawing.Bitmap b = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (x <= boarderWidth || x >= width - boarderWidth - 1 || y <= boarderWidth || y >= height - boarderWidth - 1) {
						b.SetPixel(x, y, System.Drawing.Color.FromArgb(borderColor.R, borderColor.G, borderColor.B));
					} else {
						b.SetPixel(x, y, System.Drawing.Color.FromArgb(color.R, color.G, color.B));
					}
				}
			}

			return new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, b));
		}

		public static int Width(this string s, SpriteFont f) {
			return Spritebatch.GetTextDimension(s, f).Width;
		}

		public static int Height(this string s, SpriteFont f) {
			return Spritebatch.GetTextDimension(s, f).Height;
		}

		public static Color Mix(this Color color, Color backColor) {
			//color = Color.Red;
			//backColor = Color.Green;

			//int red = 0;
			//int green = 0;
			//int blue = 0;

			//red += Convert.ToInt32(Math.Pow(255 - color.R, 2));
			//green += Convert.ToInt32(Math.Pow(255 - color.G, 2));
			//blue += Convert.ToInt32(Math.Pow(255 - color.B, 2));

			//red += Convert.ToInt32(Math.Pow(255 - backColor.R, 2));
			//green += Convert.ToInt32(Math.Pow(255 - backColor.G, 2));
			//blue += Convert.ToInt32(Math.Pow(255 - backColor.B, 2));

			//return Color.FromArgb(Math.Min(255, (int)Math.Sqrt(red / 2)), Math.Min(255, (int)Math.Sqrt(green / 2)), Math.Min(255, (int)Math.Sqrt(blue / 2)));

			//int red = 1;
			//int green = 1;
			//int blue = 1;

			//red *= color.R;
			//green *= color.G;
			//blue *= color.B;

			//red *= backColor.R;
			//green *= backColor.G;
			//blue *= backColor.B;

			//return Color.FromArgb(Math.Min(255, red / 255), Math.Min(255, green / 255), Math.Min(255, blue / 255));

			//int red = 0;
			//int green = 0;
			//int blue = 0;

			//red += color.R;
			//green += color.G;
			//blue += color.B;

			//red += backColor.R;
			//green += backColor.G;
			//blue += backColor.B;

			//return Color.FromArgb(Math.Min(255, red), Math.Min(255, green), Math.Min(255, blue));

			float hue = color.GetHue() + backColor.GetHue();
			float sat = color.GetSaturation() + backColor.GetSaturation();
			float lum = color.GetBrightness() + backColor.GetBrightness();

			return HSLtoRGB(hue / 2, sat / 2, lum / 2);
		}

		private static Color HSLtoRGB(double h, double s, double l) {
			if (s == 0) {
				// achromatic color (gray scale)
				return new Color(
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						l * 255.0)))
					);
			} else {
				double q = (l < 0.5) ? (l * (1.0 + s)) : (l + s - (l * s));
				double p = (2.0 * l) - q;

				double Hk = h / 360.0;
				double[] T = new double[3];
				T[0] = Hk + (1.0 / 3.0);
				T[1] = Hk;
				T[2] = Hk - (1.0 / 3.0);

				for (int i = 0; i < 3; i++) {
					if (T[i] < 0) T[i] += 1.0;
					if (T[i] > 1) T[i] -= 1.0;

					if ((T[i] * 6) < 1) {
						T[i] = p + ((q - p) * 6.0 * T[i]);
					} else if ((T[i] * 2.0) < 1) {
						T[i] = q;
					} else if ((T[i] * 3.0) < 2) {
						T[i] = p + (q - p) * ((2.0 / 3.0) - T[i]) * 6.0;
					} else T[i] = p;
				}

				return new Color(
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[0] * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[1] * 255.0))),
					Convert.ToInt32(Double.Parse(String.Format("{0:0.00}",
						T[2] * 255.0)))
					);
			}
		}

		public static string ToBase64(this string s) {
			return System.Convert.ToBase64String(System.Text.ASCIIEncoding.UTF8.GetBytes(s));		
		}

		public static string ToStringFromBase64(this string s) {
			return System.Text.ASCIIEncoding.UTF8.GetString(System.Convert.FromBase64String(s));
		}

		public static void WriteBase64Line (this StreamWriter writer, string text) {
			writer.WriteLine(text.ToBase64());
		}
		public static void WriteBase64Line(this StreamWriter writer) {
			writer.WriteLine("");
		}
		public static string ReadBase64Line(this StreamReader reader) {
			return reader.ReadLine().ToStringFromBase64();
		}
	}
}
