﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SideProjekt.Komponents {
	/// <summary>
	/// Wie der Name schonsagt, stellt es alle Eingaben gebündelt zur Verfügung.
	/// </summary>
	public static class VirtualGamepad {
		static bool lastMouseIsClicked = false;
		static bool currentMouseIsClicked = false;
		static Point mouseClickPoint;
		static Point mousePosition;
		static bool lastRightMouseIsClicked = false;
		static bool currentRightMouseIsClicked = false;

		public static void MouseIsClicked(bool clicked, Point p) {
			currentMouseIsClicked = clicked;
			lastMouseIsClicked = clicked;
			mouseClickPoint = p;
			mousePosition = p;
		}

		public static void RightMouseIsClicked(bool clicked, Point p) {
			currentRightMouseIsClicked = clicked;
			lastRightMouseIsClicked = clicked;
			mouseClickPoint = p;
			mousePosition = p;
		}

		public static void MouseIsMoved(Point p) {
			mousePosition = p;
		}

		public static void Update()  {
			if(lastMouseIsClicked == currentMouseIsClicked)  {
				currentMouseIsClicked = false;
			}

			lastMouseIsClicked = currentMouseIsClicked;
		}

		public static bool IsMouseClicked () {
			return lastMouseIsClicked && currentMouseIsClicked;
		}

		public static bool IsRightMouseClicked() {
			return lastRightMouseIsClicked && currentRightMouseIsClicked;
		}

		public static Point MouseClickPoint() {
			return mouseClickPoint;
		}

		public static Point GetMousePoint() {
			return mousePosition;
		}
	}
}
