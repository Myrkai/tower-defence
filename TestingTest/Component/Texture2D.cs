﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct2D1;

namespace TestingTest.Component {
	/// <summary>
	/// Texture/Bild für die Darstellung auf dem Bildschirm
	/// </summary>
	public class Texture2D {
		public Bitmap Texture;

		public Texture2D(string Filename, RenderTarget render) {
			Texture = SharpDX.Game.BitmapLoadFromFile(render, Filename);
		}

		public Texture2D(Bitmap b) {
			Texture = b;
		}

		public int Width { get { return (int)Texture.Size.Width; } }
		public int Height { get { return (int)Texture.Size.Height; } }

		public void Dispose() {
			Texture.Dispose();
		}
	}
}
