﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SideProjekt.Komponents {
	//alles wird von dieser Klasse abgeleitet (wohl er nicht)
	/// <summary>
	/// Stell eine ID und individuelle eigenschaften zur Verfügung
	/// </summary>
	public class BeschriebeneKomponente {
		public string ID;
		public string Beschreibung;
		public Dictionary<string, object> Propertys;

		public BeschriebeneKomponente() {
			BaseKonstruktor("ohne ID", "ohne Beschreibung");
		}

		public BeschriebeneKomponente(string ID) {
			BaseKonstruktor(ID, "ohne Beschreibung");
		}

		public BeschriebeneKomponente(string ID, string Beschreibung) {
			BaseKonstruktor(ID, Beschreibung);
		}

		private void BaseKonstruktor(string ID, string Beschreibung) {
			this.ID = ID;
			this.Beschreibung = Beschreibung;
			Propertys = new Dictionary<string, object>();
		}
	}
}
