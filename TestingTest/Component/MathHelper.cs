﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using SharpDX;
using Rectangle = SharpDX.Rectangle;
using Matrix = System.Drawing.Drawing2D.Matrix;
using Point = System.Drawing.Point;

namespace SideProjekt.Komponents {
	public static class MathHelper {
		/// <summary>
		/// Parameters
		///
		///value
		///	Type: Single
		///	The value to clamp.
		///min
		///	Type: Single
		///	The minimum value. If value is less than min, min will be returned. 
		///max
		///	Type: Single
		///	The maximum value. If value is greater than max, max will be returned. 
		///
		///Return Value
		///The clamped value.
		///
		///	If value > max, max will be returned.
		///	If value < min, min will be returned.
		///	If min ≤ value ≥ max, value will be returned.
		///
		///
		/// </summary>
		/// <param name="value"></param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns></returns>
		public static int Clamp(int value, int min, int max) {
			if (value > max)
				return max;
			else if (value < min)
				return min;
			else
				return value;
		}

		/// <summary>
		/// Lässt einen Punkt rotieren.
		/// </summary>
		/// <param name="v">welcher Punkt soll rotiert werden</param>
		/// <param name="rotation">um wieviel Grad soll er rotiert werden</param>
		/// <param name="oV">ausgangspunkt der Rotation (stell dir einen Zirkel vor und die eisenspitze ist dieser punkt)</param>
		/// <returns></returns>
		public static Vector2 RotateAPoint(Vector2 v, double rotation, Vector2 oV) {
			Matrix matrix = new Matrix();
			matrix.RotateAt((float)rotation, new Point(Convert.ToInt32(oV.X), Convert.ToInt32(oV.Y)));
			Point[] a = new Point[1];
			a[0] = new Point(Convert.ToInt32(v.X), Convert.ToInt32(v.Y));
			matrix.TransformPoints(a);

			Vector2 L = new Vector2(a[0].X, a[0].Y);

			return L;
		}

		/// <summary>
		/// Rechnet den Winkel (Bogenwinkel). 
		/// Ausgehend davon das der Tower immer direkt nach obeb schießt und das dir Drehrichtung im Uhrzeigersinn verläuft (nach rechts)
		/// </summary>
		/// <param name="Position">Position des Zieles</param>
		/// <param name="CenterFromTower">Position des Tower. Mittelpunkt des Kreises</param>
		/// <returns></returns>
		public static double GetRotation (Vector2 Position, Vector2 CenterFromTower) {
			if(Position == Vector2.Zero && CenterFromTower == Vector2.Zero || Position.X == CenterFromTower.X && Position.Y < CenterFromTower.Y) {
				return 0;
			} //befindet sich direkt über dem tower

			double winkel = 0;
			//ziel ist rechts oben
			if(Position.X > CenterFromTower.X &&  Position.Y < CenterFromTower.Y) {
				float länge = (CenterFromTower.Y - Position.Y);
				if(CenterFromTower.Y - länge == Position.Y && CenterFromTower.X + länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X + länge > Position.X) { //winkel kleiner 45 grad
					winkel += 90 - Math.Atan((CenterFromTower.Y - Position.Y) / (Position.X - CenterFromTower.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += Math.Atan((Position.X - CenterFromTower.X) / (CenterFromTower.Y - Position.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			//ziel befindet sich direkt rechts vom tower
			if(CenterFromTower.Y == Position.Y && CenterFromTower.X < Position.X) {
				return 90;
			}

			//ziel befindet sich unten rechts
			if (Position.X > CenterFromTower.X && Position.Y > CenterFromTower.Y) {
				float länge = (Position.Y - CenterFromTower.Y);
				if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X + länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X + länge < Position.X) { //winkel kleiner 45 grad
					winkel += Math.Atan((Position.Y - CenterFromTower.Y) / (Position.X - CenterFromTower.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += 90 - Math.Atan((Position.X - CenterFromTower.X) / (Position.Y - CenterFromTower.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			//ziel befindet sich unten links
			if (Position.X < CenterFromTower.X && Position.Y > CenterFromTower.Y) {
				float länge = (Position.Y - CenterFromTower.Y);

				if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X - länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y + länge == Position.Y && CenterFromTower.X - länge < Position.X) { //winkel kleiner 45 grad
					winkel += 90 - Math.Atan((Position.Y - CenterFromTower.Y) / (CenterFromTower.X - Position.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += Math.Atan((CenterFromTower.X - Position.X) / (Position.Y - CenterFromTower.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else { 
				winkel += 90;
			}

			//ziel befindet sich oben links
			if (Position.X < CenterFromTower.X && Position.Y < CenterFromTower.Y) {
				float länge = (CenterFromTower.Y - Position.Y);

				if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X - länge == Position.X) {
					winkel += 45;
				} else if (CenterFromTower.Y - länge == Position.Y && CenterFromTower.X - länge < Position.X) { //winkel kleiner 45 grad
					winkel += Math.Atan((CenterFromTower.Y - Position.Y) / (CenterFromTower.X - Position.X)) * 180 / Math.PI;
				} else { //winkel größer 45 grad
					winkel += 90 - Math.Atan((CenterFromTower.X - Position.X) / (CenterFromTower.Y - Position.Y)) * 180 / Math.PI;
				}

				return winkel;
			} else {
				winkel += 90;
			}

			return winkel;
		}

		#region PointInCircle
		/// <summary>
		/// Bestimmt ob ein Punkt sich innerhalb eines Kreises befindet
		/// </summary>
		/// <param name="centerX">Mittelpunkt des Kreises</param>
		/// <param name="centerY">Mittelpunkt des Kreises</param>
		/// <param name="radius">Radius des Kreises</param>
		/// <param name="x">Prüfpunkt</param>
		/// <param name="y">Prüfpunkt</param>
		/// <returns></returns>
		public static bool IsPointInCircle(double centerX, double centerY, double radius, double x, double y) {
			return Vector2.Distance(new Vector2((float)centerX, (float)centerY), new Vector2((float)x, (float)y)) <= radius;
		}

		public static bool CircleIntersectsRectangle(double centerX, double centerY, double radius, Rectangle rectangle) {
			//1. Kreismittelpunkt innerhalb rectangle
			//2. Ecken des Rechteckes innerhalb des Kreises
			//3. Kreis überlappt eine seite des Rechteckes
			return
					rectangle.Intersects(new Rectangle((int)centerX, (int)centerX, 1, 1)) || //1. Satz
					MathHelper.IsPointInCircle(centerX, centerY, radius, rectangle.X, rectangle.Y) || //2. Satz
					MathHelper.IsPointInCircle(centerX, centerY, radius, rectangle.X + rectangle.Width, rectangle.Y) || //2. Satz
					MathHelper.IsPointInCircle(centerX, centerY, radius, rectangle.X, rectangle.Y + rectangle.Height) || //2. Satz
					MathHelper.IsPointInCircle(centerX, centerY, radius, rectangle.X + rectangle.Height, rectangle.Y + rectangle.Height) || //2. Satz
					(rectangle.X > centerX && rectangle.X + rectangle.Width < centerX && centerY < rectangle.Y && rectangle.Y - centerY > radius) || //3. Satz oben
					(rectangle.X > centerX && rectangle.X + rectangle.Width < centerX && centerY > rectangle.Y + rectangle.Height && centerY - rectangle.Y + rectangle.Height > radius) || //3. Satz unten
					(rectangle.Y > centerY && rectangle.Y + rectangle.Height < centerY && centerX < rectangle.X && rectangle.X - centerX > radius) || //3. Satz links
					(rectangle.Y > centerY && rectangle.Y + rectangle.Height < centerY && centerX > rectangle.X + rectangle.Width && centerX - rectangle.X + rectangle.Width > radius); //3. Satz rechts
		}
		#endregion
	}
}
