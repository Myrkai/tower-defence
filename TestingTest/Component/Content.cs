﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using SideProjekt.ToXNA;
using SideProjekt.TileEngine;
using SideProjekt.ToXNA.Animation;
using SideProjekt.ToXNA.Battle;
using TestingTest.Component;
using SharpDX;
using TestingTest.ToXNA;
using TestingTest.Interfaces;

namespace SideProjekt.Komponents {
	/// <summary>
	/// Zentrale Schnittstelle zum Laden und Speichern sämtlicher Klassen und Objekte.
	/// </summary>
	public class Content {
		/// <summary>
		/// Lädt sämtlichen Inhalt von der Festplatte und stellt die entsprechenden Klassen zur Verfügung.
		/// </summary>
		/// <typeparam name="T">Welche Klasse soll zurückgeben werden</typeparam>
		/// <param name="Pfad">Wo liegt die zu ladende Datei</param>
		/// <returns></returns>
		public static T Load<T>(string Pfad) where T : class {
			return (T)Load2(typeof(T), Pfad);
		}

		/// <summary>
		/// eiegntliche Implementiert des Ladens der Dateien
		/// </summary>
		/// <param name="t">Welche Klasse soll erzeugt werden</param>
		/// <param name="Pfad">Wo liegt die zu ladende Datei. Achtung der Pfad kann auch keine Dateiendung besitzen</param>
		/// <returns></returns>
		private static Object Load2(Type t, string Pfad) {
			if (t == typeof(Texture2D)) {
				string[] files = System.IO.Directory.GetFiles(System.IO.Path.GetFullPath("Content\\" + Pfad + ".text").Replace(System.IO.Path.GetFileName("Content\\" + Pfad + ".text"), ""));
				Bitmap a = (Bitmap)Image.FromFile(files.Single(file => file.StartsWith(System.IO.Path.GetFullPath("Content\\" + Pfad))));
				return new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, a));
			}

			if (t == typeof(Level)) {
				return TryLoadLevel(Pfad);
			}

			if (t == typeof(AnimatedSprite)) {
				string[] files = System.IO.Directory.GetFiles(System.IO.Path.GetFullPath("Content\\" + Pfad + ".text").Replace(System.IO.Path.GetFileName("Content\\" + Pfad + ".text"), ""));
				string iniFile = files.Where(x => System.IO.Path.GetFileName(x) == System.IO.Path.GetFileName(Pfad) + ".ini").First();

				Bitmap a = (Bitmap)Image.FromFile(files.Where(x => System.IO.Path.GetFileName(x) != System.IO.Path.GetFileName(Pfad) + ".ini").Single(file => file.StartsWith(System.IO.Path.GetFullPath("Content\\" + Pfad))));
				//Texture2D texture = new Texture2D(Game.BitmapToBitmap(Spritebatch.renderTarget, a));

				string[] information = { "3", "4", "0.2" };//default für .net
				using (StreamReader reader = new StreamReader(iniFile)) {
					string line = "";
					while (!reader.EndOfStream) {
						line = reader.ReadLine();
						if (!line.StartsWith("//") || line.Trim() != "") {
							information = line.Split(',');
						}
					}
				}

				return new AnimatedSprite(System.IO.Path.GetFileNameWithoutExtension(iniFile), a, Convert.ToInt32(information[0]), Convert.ToInt32(information[1]), Convert.ToSingle(information[2].Replace(".", ",")));
			}
			throw new Exception("Type ist nicht implementiert.");
		}

		private static Level TryLoadLevel(string pfad) {
			//stuff for level return
			Level l = new Level();
			bool isBullet = false, isTower = false, isMap = false, isMonster = false, isWaves = false, isElement = false;
			List<Bullet> bullets = new List<Bullet>();
			List<Monster> monsters = new List<Monster>();
			List<Element> elements = new List<Element>();

			//stuff for map loading
			int mapWidth = -1, mapHeight = -1, mapHeightIndex = 0;
			bool mapHeaderLoaded = false, mapDataLoaded = false;

			bool pathHeaderLoaded = false, pathDataLoaded = false;
			int pathNodeCount = -1;
			int pathNodeIndex = 0;

			TileMap map;
			SideProjekt.ToXNA.Path path = new SideProjekt.ToXNA.Path();

			Wave wave = null;

			string newPfad = @"Content\Data\" + pfad + ".lvl";
			using (StreamReader reader = new StreamReader(newPfad, Encoding.UTF8)) {
				while (!reader.EndOfStream) {
					string line = reader.ReadLine();
					if (!line.StartsWith("//") && line.Length > 1) {
						if (line.StartsWith("#")) {
							switch (line) {
								case "#Bullets": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isBullet = true; break;
								case "#Towers": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isTower = true; break;
								case "#Map": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isMap = true; break;
								case "#Monster": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isMonster = true; break;
								case "#Waves": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isWaves = true; break;
								case "#Elements": isBullet = isTower = isMonster = isMap = isWaves = isElement = false; isElement = true; break;
								default: isBullet = isTower = isMonster = isMap = isWaves = isElement = false; continue;
							}
							Calculator.CalcWeakerElementsFromStronger(ref elements);
						} else {
							if (isElement) {
								string[] elementStuff = line.Split(','); //todo save elements //todo tower and monster need elements and armor and atktype
								elements.Add(new Element(elementStuff[0], (ElementType)Enum.Parse(typeof(ElementType), elementStuff[1]), (ElementType)Enum.Parse(typeof(ElementType), elementStuff[2])));
							}

							if (isBullet) {
								string[] bulletStuff = line.Split(',');
								bullets.Add(new Bullet(bulletStuff[0], bulletStuff[1], bulletStuff[2], Convert.ToSingle(bulletStuff[3].Replace(".", ",")), 0, Convert.ToSingle(bulletStuff[4].Replace(".", ",")), Convert.ToSingle(bulletStuff[5].Replace(".", ",")), Convert.ToBoolean(bulletStuff[6])));
							}

							if (isTower) {
								string[] towerStuff = line.Split(',');
								Tower t = new Tower(towerStuff[1], towerStuff[2], towerStuff[0], Convert.ToInt32(towerStuff[3]), Convert.ToInt32(towerStuff[4]), bullets.Where(b => b.Name == towerStuff[5]).First().Clone(), Convert.ToSingle(towerStuff[6].Replace(".", ",")), null);
								l.AddTower(t);
								t.SetTowerCosts(Convert.ToInt32(towerStuff[7]), Convert.ToInt32(towerStuff[8]));
								t.SetTowerDamage(Convert.ToInt32(towerStuff[9]), Convert.ToInt32(towerStuff[11]), Convert.ToInt32(towerStuff[10]));
								t.SetTowerAmountForEffekt(Convert.ToSingle(towerStuff[12].Replace(".", ",")), towerStuff[13]=="1" ? true : false);
							}

							if (isMap) {
								if (!mapHeaderLoaded) {
									string[] mapStuff = line.Split(',');
									map = new TileMap(mapStuff[4]);
									l.myMap = map;
									l.myMap.SwitchMapAndReset(Convert.ToInt32(mapStuff[2]), Convert.ToInt32(mapStuff[3]), mapStuff[4]);
									mapWidth = Convert.ToInt32(mapStuff[0]);
									mapHeight = Convert.ToInt32(mapStuff[1]);
									mapHeaderLoaded = true;
									continue;
								}

								if (mapHeaderLoaded && !mapDataLoaded) {
									MapRow thisRow = new MapRow();
									string[] rowStuff = line.Split(' ');
									for (int x = 0; x < mapWidth; x++) {
										thisRow.Columns.Add(new MapCell(Convert.ToInt32(rowStuff[x]), new Vector2(x, mapHeightIndex)));
									}
									l.myMap.Rows.Add(thisRow);
									mapHeightIndex += 1;
									if (mapHeightIndex >= mapHeight) {
										mapDataLoaded = true;
									}
									continue;
								}

								if (mapHeaderLoaded && mapDataLoaded) {
									if (!pathHeaderLoaded) {
										line = line.Replace("PfadIndex: ", "");
										string[] pfadStuff = line.Split(',');
										path = new SideProjekt.ToXNA.Path();
										pathNodeCount = Convert.ToInt32(pfadStuff[1]);
										pathHeaderLoaded = true;
										continue;
									}

									if (pathHeaderLoaded && !pathDataLoaded) {
										string[] nodeStuff = line.Split(',');
										path.AddNewNode(new PathNode(Convert.ToInt32(nodeStuff[0]), Convert.ToInt32(nodeStuff[1]), l.myMap.TileHeight, l.myMap.TileWidth));
										pathNodeIndex += 1;
										if (pathNodeIndex >= pathNodeCount) {
											pathHeaderLoaded = false;
											pathDataLoaded = true;
										}
									}

									if (pathDataLoaded) {
										l.myMap.Paths.Add(path);
										pathDataLoaded = false;
										pathNodeIndex = 0;
										continue;
									}
								}
							}

							if (isMonster) {
								string[] monsterStuff = line.Split(',');
								Monster m = new Monster(monsterStuff[0], Convert.ToInt32(monsterStuff[1]), monsterStuff[2], monsterStuff[3], Convert.ToSingle(monsterStuff[4].Replace(".", ",")));
								m.SetBonus(Convert.ToInt32(monsterStuff[5]), Convert.ToInt32(monsterStuff[6]), Convert.ToInt32(monsterStuff[7]));
								monsters.Add(m);
							}

							if (isWaves) {
								if (line.StartsWith("ADD")) {
									string[] tupleStuff = line.Split(',');
									wave.MonsterToSend.Add(new Tuple<Monster, int, int>(monsters.Where(m => m.Name == tupleStuff[1]).First().Clone(wave, l.myMap.Paths[Convert.ToInt32(tupleStuff[3])]), Convert.ToInt32(tupleStuff[2]), Convert.ToInt32(tupleStuff[3])));
								} else {
									string[] waveStuff = line.Split(',');
									wave = new Wave(waveStuff[0], Convert.ToSingle(waveStuff[1].Replace(".", ",")));
									wave.SetWaveBonus(Convert.ToInt32(waveStuff[2]), Convert.ToInt32(waveStuff[3]), Convert.ToInt32(waveStuff[4]));
									l.Waves.Add(wave);
								}
							}
						}
					}
				}
			}

			return l;
		}

		public static void Save<T>(string Pfad, Object o) where T : class {
			Save2(typeof(T), o, Pfad);
		}

		private static void Save2(Type t, Object o, string Pfad) {
			if (t == typeof(Level)) {
				Level l = (Level)o;
				SaveLevel(Pfad, l);
				return;
			}
			throw new Exception("Type ist nicht implementiert.");
		}

		private static void SaveLevel(string pfad, Level level) {
			using (StreamWriter writer = new StreamWriter(@"Content\Data\" + pfad + ".lvl", false, Encoding.UTF8)) {
				writer.WriteLine(@"#ArmorType");
				foreach (string a in Extensions.EnumToList<ArmorType>()) {
					writer.WriteLine(a);
				}

				writer.WriteLine();
				writer.WriteLine(@"#AttackType");
				foreach (string a in Extensions.EnumToList<AttackType>()) {
					writer.WriteLine(a);
				}

				writer.WriteLine();
				writer.WriteLine(@"#ElementType");
				foreach (string a in Extensions.EnumToList<ElementType>()) {
					writer.WriteLine(a);
				}

				writer.WriteLine();
				writer.WriteLine(@"#Elements");

				writer.WriteLine();
				writer.WriteLine(@"#EffektType");
				List<IEffekt> Effekts = new List<IEffekt>();
				Effekts.Add(new PoisonEffekt(0, 0));
				Effekts.Add(new SlowEffekt(0, 0));
				Effekts.Add(new AoEEffekt(0, 0, 0));
				foreach (IEffekt e in Effekts) {
					writer.WriteLine(e.GetType().ToString().Substring(e.GetType().ToString().LastIndexOf(".") + 1));
				}
				writer.WriteLine("NULL");

				writer.WriteLine();
				writer.WriteLine(@"#Bullets");
				writer.WriteLine(@"//Name, EffektType, Grafik, Speed, AreaWidth, Time, hasDirektDamage");
				foreach (Bullet b in level.Towers.Select(x => x.BaseBullet).Distinct().ToList()) {
					writer.WriteLine(String.Concat(b.Name, ",", b.EffektType, ",", b.GraphicAsset, ",", b.FlightSpeed.ToString().Replace(",", "."), ",", b.AreaWidth.ToString().Replace(",", "."), ",", b.Time.ToString().Replace(",", "."), ",", b.HasDirektDamage.ToString()));
				}

				writer.WriteLine();
				writer.WriteLine(@"#Towers");
				writer.WriteLine(@"//Name, Vorschaubild, Grafik, HP, Radius, Schußname, Attack Speed, Gold Kosten, Energie Kosten, Damage, ThrowDamage, ThrowCount, AmountForEffekt, DamageIsAmount");
				foreach (Tower t in level.Towers) {
					writer.WriteLine(String.Concat(t.TowerName, ",", t.ThumbnailAsset, ",", t.GraphicAsset, ",", t.Hitpoints, ",", t.DmgCircleWidth, ",", t.BaseBullet.Name, ",", t.AttackSpeed.ToString().Replace(",", "."), ",", t.GoldCost, ",", t.EnergieCost, ",", t.Damage, ",", t.ThrowDamage, ",", t.ThrowCount, ",", t.MountForEffekt.ToString().Replace(",", "."), ",", t.DamageIsAmount ? "1" : "0"));
				}

				writer.WriteLine();
				writer.WriteLine(@"#Map");
				writer.WriteLine(@"//Kartenweite, Kartenhöhe, TileHöhe, TileBreite, TileSet");
				writer.WriteLine(String.Concat(level.myMap.MapWidth, ",", level.myMap.MapHeight, ",", level.myMap.TileHeight, ",", level.myMap.TileWidth, ",", level.myMap.TileSetAsset));
				writer.WriteLine(@"//Korrdinaten, beinhaltet die TileID");
				for (int y = 0; y < level.myMap.MapHeight; y++) {
					string line = "";
					for (int x = 0; x < level.myMap.MapWidth; x++) {
						line += string.Concat(level.myMap.Rows[y].Columns[x].TileID, " ");
					}
					writer.WriteLine(line);
				}

				writer.WriteLine(@"//Pfade");
				for (int i = 0; i < level.myMap.Paths.Count(); i++) {
					writer.WriteLine(string.Concat("PfadIndex: ", i, ",", level.myMap.Paths[i].Nodes.Count()));
					writer.WriteLine(@"//MapTileIndex X, MapTileIndex Y");
					foreach (PathNode n in level.myMap.Paths[i].Nodes) {
						writer.WriteLine(string.Concat(n.IndexX, ",", n.IndexY));
					}
				}

				writer.WriteLine();
				writer.WriteLine(@"#Monster");
				writer.WriteLine(@"//Name, Lebenspunkte, Vorschaubild, Grafik, Laufgewindigkeit, ScoreBonus, GoldBonus, EnergieBonus");
				List<Monster> monsters = level.Waves.SelectMany(x => x.MonsterToSend).Select(y => y.Item1).Distinct().ToList();
				List<string> monstersToWrite = new List<string>();
				foreach (Monster m in monsters) {
					monstersToWrite.Add(String.Concat(m.Name, ",", m.Hitpoints, ",", m.ThumbnailAsset, ",", m.GraphicAsset, ",", m.Movespeed.ToString().Replace(",", "."), ",", m.ScoreBonus, ",", m.GoldBonus, ",", m.EnergieBonus));
				}
				foreach (string s in monstersToWrite.Distinct()) {
					writer.WriteLine(s);
				}

				writer.WriteLine();
				writer.WriteLine(@"#Waves");
				foreach (Wave w in level.Waves) {
					writer.WriteLine(@"//Wavebezeichnung, Monster Send Speed, Gold Bonus, Energie Bonus, Leben Bonus");
					writer.WriteLine(String.Concat(w.Name, ",", w.monsterSendSpeed.ToString().Replace(",", "."), ",", w.GoldBonus, ",", w.EnergieBonus, ",", w.LivesBonus));
					writer.WriteLine(@"//ADD MonsterName, Anzahl, Pathindex");
					foreach (Tuple<Monster, int, int> t in w.MonsterToSend) {
						writer.WriteLine(String.Concat("ADD", ",", t.Item1.Name, ",", t.Item2, ",", t.Item3));
					}
					writer.WriteLine();
				}
			}
		}
	}
}
