﻿using System;
using SharpDX_Tutorial01;

namespace SharpDX {
	class Program {
		static void Main(string[] args) {
			using (Game game = new Game01("Test", 1024, 768)) {
				game.Run();
			}
		}
	}
}