﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Direct2D1;
using TestingTest;
using TestingTest.Component;
using SideProjekt.ToXNA.Screens;
using SideProjekt.Komponents;
using SideProjekt.ToXNA.Log;

namespace SharpDX_Tutorial01 {
	public class Game01 : Game {

		#region Variablen
		private ScreenManager screenManager { get; set; }
		public GameScreen gameScreen { get; private set; }
		public TitleScreen titleScreen { get; private set; }
		public NameInputScreen nameInputScreen { get; private set; }

		#endregion

		public Game01(string Title, int Width, int Height) : base(Title, Width, Height) {
			renderWindow.MouseClick += new System.Windows.Forms.MouseEventHandler(renderWindow_MouseClick);
			renderWindow.MouseMove += new System.Windows.Forms.MouseEventHandler(renderWindow_MouseMove);

			Log.SetOutput(new ConsoleLogger());
		}

		void renderWindow_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) {
			VirtualGamepad.MouseIsMoved(new System.Drawing.Point(e.X, e.Y));
		}

		void renderWindow_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e) {
			if (e.Button == System.Windows.Forms.MouseButtons.Left) {
				VirtualGamepad.MouseIsClicked(true, new System.Drawing.Point(e.X, e.Y));
			}

			if (e.Button == System.Windows.Forms.MouseButtons.Right) {
				VirtualGamepad.RightMouseIsClicked(true, new System.Drawing.Point(e.X, e.Y));
			}
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			screenManager.Update(gameTime);
			VirtualGamepad.Update();
		}

		public override void Draw(Spritebatch spriteBatch) {
			base.Draw(spriteBatch);

			spriteBatch.BeginScene();		
			//spriteBatch.Clear(Color.CornflowerBlue);
			screenManager.Draw(spriteBatch);
			spriteBatch.DrawText(FPS.ToString(), SpriteFont.GetFont(25f), new Vector2(10, 10));

			spriteBatch.EndScene();
		}

		public override void Initialize() {
			base.Initialize();
			screenManager = new ScreenManager();

			titleScreen = new TitleScreen(this, screenManager);
			titleScreen.Initialize();
			titleScreen.LoadContent();
			nameInputScreen = new NameInputScreen(this, screenManager);
			nameInputScreen.Initialize();
			nameInputScreen.LoadContent();
			gameScreen = new GameScreen(this, screenManager);
			gameScreen.Initialize();
			gameScreen.LoadContent();

			screenManager.Transfer(titleScreen);
		}

		public override void LoadContent() {
			base.LoadContent();
		}

		public override void UnloadContent() {
			base.UnloadContent();
		}
	}
}